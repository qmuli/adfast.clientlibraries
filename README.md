# Adfast.Client Libraries
# Copyright © 2016. Qmuli Ltd  
  
Contains:  
  
Adfast.Api.Client  
Client library class that wraps all Adfast API Calls  
  
Adfast.Api.Dto  
DTO/Models used by Adfast.Api.Client  
  
Adfast.Api.Demo  
This is a winforms app used to test the API manually.  
  
These libraries are maintained by Qmuli and updated as the API develops.

Versioning:  
  
See RELEASE.TXT in \Projects\Adfast.Api.Client for cumulative version of Client, Dto and Demo.