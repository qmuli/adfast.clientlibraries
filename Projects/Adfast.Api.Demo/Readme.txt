This client libary is used for testing the API

Setup:

The App.config file will need to have the API keys specified, please contact support@qmuli.com for more information.

Note: The asterisk's next to fields indicate the association with the same colour buttons, these do not represenr required fields.

See supporting documentation for more info.

The current live API is located at:
http://beta.adfast.co.uk/apidocumentation
[The new Adfast site is currently in beta, looking at the live database, however the API is live on this address]

The latest development online API documentation is located at: 
http://adfastdev.qmuli.com/apidocumentation
[This may not be the same version, but is the latest qmuli development]

