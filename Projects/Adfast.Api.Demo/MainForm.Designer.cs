﻿namespace Adfast.Api.Demo
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.IsPadnCheckbox = new System.Windows.Forms.CheckBox();
            this.InsertionPublicationSectionCodeTextBox = new System.Windows.Forms.TextBox();
            this.label166 = new System.Windows.Forms.Label();
            this.PublisherCodeTextBox = new System.Windows.Forms.TextBox();
            this.label163 = new System.Windows.Forms.Label();
            this.MailBoxDoNotSendEmailCheckBox = new System.Windows.Forms.CheckBox();
            this.label164 = new System.Windows.Forms.Label();
            this.label160 = new System.Windows.Forms.Label();
            this.label161 = new System.Windows.Forms.Label();
            this.label162 = new System.Windows.Forms.Label();
            this.label156 = new System.Windows.Forms.Label();
            this.label159 = new System.Windows.Forms.Label();
            this.label158 = new System.Windows.Forms.Label();
            this.PublicationSectionCodeTextBox = new System.Windows.Forms.TextBox();
            this.label157 = new System.Windows.Forms.Label();
            this.label155 = new System.Windows.Forms.Label();
            this.WidthTextBox = new System.Windows.Forms.TextBox();
            this.label154 = new System.Windows.Forms.Label();
            this.label153 = new System.Windows.Forms.Label();
            this.SplitDpsDeliveryRightHandPageUrnTextBox = new System.Windows.Forms.TextBox();
            this.label151 = new System.Windows.Forms.Label();
            this.SplitDpsDeliveryLeftHandPageUrnTextBox = new System.Windows.Forms.TextBox();
            this.label144 = new System.Windows.Forms.Label();
            this.ExternalDeliveryRefTextBox = new System.Windows.Forms.TextBox();
            this.label145 = new System.Windows.Forms.Label();
            this.SplitDPSFileCheckBox = new System.Windows.Forms.CheckBox();
            this.label140 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this.QmuliStandardWorkflowIdTextBox = new System.Windows.Forms.TextBox();
            this.label139 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.MailBoxUrgentDeadlineCheckBox = new System.Windows.Forms.CheckBox();
            this.label108 = new System.Windows.Forms.Label();
            this.label146 = new System.Windows.Forms.Label();
            this.label143 = new System.Windows.Forms.Label();
            this.CropFileCheckBox = new System.Windows.Forms.CheckBox();
            this.label138 = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.label137 = new System.Windows.Forms.Label();
            this.DeadlineDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label133 = new System.Windows.Forms.Label();
            this.SendDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label128 = new System.Windows.Forms.Label();
            this.SendAutoCreatedAdfastDeliveryReminderCheckBox = new System.Windows.Forms.CheckBox();
            this.label124 = new System.Windows.Forms.Label();
            this.AutoCreateAdfastDeliveryCheckBox = new System.Windows.Forms.CheckBox();
            this.label125 = new System.Windows.Forms.Label();
            this.CreateSavedDelivery = new System.Windows.Forms.Button();
            this.DeliverWithoutChecks = new System.Windows.Forms.CheckBox();
            this.label89 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.IgnoreSizeCheck = new System.Windows.Forms.CheckBox();
            this.IgnoreSizeCheckx = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.AccountNumberTextBox = new System.Windows.Forms.TextBox();
            this.label111 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.MultipleInsertionsSectionId = new System.Windows.Forms.TextBox();
            this.label100 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.InsertionDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label81 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.IgnoreErrorsAndDeliverOriginalFile = new System.Windows.Forms.CheckBox();
            this.label87 = new System.Windows.Forms.Label();
            this.AutoUpsampleFix = new System.Windows.Forms.CheckBox();
            this.label86 = new System.Windows.Forms.Label();
            this.AutoFontFix = new System.Windows.Forms.CheckBox();
            this.label85 = new System.Windows.Forms.Label();
            this.AutoDeliverOptimisedFile = new System.Windows.Forms.CheckBox();
            this.label84 = new System.Windows.Forms.Label();
            this.AutoDeliverFixedFile = new System.Windows.Forms.CheckBox();
            this.label83 = new System.Windows.Forms.Label();
            this.AutoDeliverCheckBox = new System.Windows.Forms.CheckBox();
            this.label82 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.ContactCompanyTextBox = new System.Windows.Forms.TextBox();
            this.label74 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.ContactEmailTextBox = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.ContactPhoneTextBox = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.ContactNameTextBox = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.MultiInsertionDatePicker = new System.Windows.Forms.DateTimePicker();
            this.DontOptimiseRadioButton = new System.Windows.Forms.RadioButton();
            this.OptimiseRadioButton = new System.Windows.Forms.RadioButton();
            this.label53 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.InsertionDateListBox = new System.Windows.Forms.ListBox();
            this.button4 = new System.Windows.Forms.Button();
            this.CreateMailBoxDeliveryButton = new System.Windows.Forms.Button();
            this.DeliveryToFTPUsernameTextBox = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.DeliveryToFtpPortTextBox = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.FilePathTextBox = new System.Windows.Forms.TextBox();
            this.SelectFileButton = new System.Windows.Forms.Button();
            this.DeliveryRuleDropDownList = new System.Windows.Forms.ComboBox();
            this.DeliveryToFtpPasswordTextBox = new System.Windows.Forms.TextBox();
            this.DeliveryToFtpPathTextBox = new System.Windows.Forms.TextBox();
            this.DeliveryToFtpHostTextBox = new System.Windows.Forms.TextBox();
            this.DeliverToEmailAddressTextBox = new System.Windows.Forms.TextBox();
            this.DeliveryToTitleTextBox = new System.Windows.Forms.TextBox();
            this.BrandTextBox = new System.Windows.Forms.TextBox();
            this.AdvertiserTextBox = new System.Windows.Forms.TextBox();
            this.UrnTextBox = new System.Windows.Forms.TextBox();
            this.CommentsTextBox = new System.Windows.Forms.TextBox();
            this.HeightTextBox = new System.Windows.Forms.TextBox();
            this.PublicationSectionSizeIdTextBox = new System.Windows.Forms.TextBox();
            this.PublicationSectionIdTextBox = new System.Windows.Forms.TextBox();
            this.SubmissionTypeDropDownList = new System.Windows.Forms.ComboBox();
            this.MonoCheckBox = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SubmissionTypeLabel = new System.Windows.Forms.Label();
            this.UploadFileButton = new System.Windows.Forms.Button();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label141 = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.label152 = new System.Windows.Forms.Label();
            this.label149 = new System.Windows.Forms.Label();
            this.label150 = new System.Windows.Forms.Label();
            this.label147 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label165 = new System.Windows.Forms.Label();
            this.label167 = new System.Windows.Forms.Label();
            this.label168 = new System.Windows.Forms.Label();
            this.label169 = new System.Windows.Forms.Label();
            this.label170 = new System.Windows.Forms.Label();
            this.UpdateDeliveryDetailsButton = new System.Windows.Forms.Button();
            this.IdTestBox = new System.Windows.Forms.TextBox();
            this.IdLabel = new System.Windows.Forms.Label();
            this.GetDeliveryStatusButton = new System.Windows.Forms.Button();
            this.GetDeliveryDetailsButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SetDeliveryReceivedButton = new System.Windows.Forms.Button();
            this.GetPublisherDownloadList = new System.Windows.Forms.Button();
            this.PublisherDeliveryDownload = new System.Windows.Forms.Button();
            this.PageNumber = new System.Windows.Forms.NumericUpDown();
            this.label110 = new System.Windows.Forms.Label();
            this.AuthorizedByTextBox = new System.Windows.Forms.TextBox();
            this.GetDeliveryListButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.UnArchiveDeliveryButton = new System.Windows.Forms.Button();
            this.ArchiveDeliveryButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.InitiateDeliveryButton = new System.Windows.Forms.Button();
            this.label75 = new System.Windows.Forms.Label();
            this.GetApiVersionButton = new System.Windows.Forms.Button();
            this.OutputTextBox = new System.Windows.Forms.TextBox();
            this.CloseButton = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.MailBoxGetDetailsButton = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.label117 = new System.Windows.Forms.Label();
            this.MailBoxUrnTextBox = new System.Windows.Forms.TextBox();
            this.label116 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.MailBoxIdTextBox = new System.Windows.Forms.TextBox();
            this.label115 = new System.Windows.Forms.Label();
            this.label130 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PageNumber)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.IsPadnCheckbox);
            this.groupBox3.Controls.Add(this.InsertionPublicationSectionCodeTextBox);
            this.groupBox3.Controls.Add(this.label166);
            this.groupBox3.Controls.Add(this.PublisherCodeTextBox);
            this.groupBox3.Controls.Add(this.label163);
            this.groupBox3.Controls.Add(this.MailBoxDoNotSendEmailCheckBox);
            this.groupBox3.Controls.Add(this.label164);
            this.groupBox3.Controls.Add(this.label160);
            this.groupBox3.Controls.Add(this.label161);
            this.groupBox3.Controls.Add(this.label162);
            this.groupBox3.Controls.Add(this.label156);
            this.groupBox3.Controls.Add(this.label159);
            this.groupBox3.Controls.Add(this.label158);
            this.groupBox3.Controls.Add(this.PublicationSectionCodeTextBox);
            this.groupBox3.Controls.Add(this.label157);
            this.groupBox3.Controls.Add(this.label155);
            this.groupBox3.Controls.Add(this.WidthTextBox);
            this.groupBox3.Controls.Add(this.label154);
            this.groupBox3.Controls.Add(this.label153);
            this.groupBox3.Controls.Add(this.SplitDpsDeliveryRightHandPageUrnTextBox);
            this.groupBox3.Controls.Add(this.label151);
            this.groupBox3.Controls.Add(this.SplitDpsDeliveryLeftHandPageUrnTextBox);
            this.groupBox3.Controls.Add(this.label144);
            this.groupBox3.Controls.Add(this.ExternalDeliveryRefTextBox);
            this.groupBox3.Controls.Add(this.label145);
            this.groupBox3.Controls.Add(this.SplitDPSFileCheckBox);
            this.groupBox3.Controls.Add(this.label140);
            this.groupBox3.Controls.Add(this.label132);
            this.groupBox3.Controls.Add(this.QmuliStandardWorkflowIdTextBox);
            this.groupBox3.Controls.Add(this.label139);
            this.groupBox3.Controls.Add(this.label109);
            this.groupBox3.Controls.Add(this.MailBoxUrgentDeadlineCheckBox);
            this.groupBox3.Controls.Add(this.label108);
            this.groupBox3.Controls.Add(this.label146);
            this.groupBox3.Controls.Add(this.label143);
            this.groupBox3.Controls.Add(this.CropFileCheckBox);
            this.groupBox3.Controls.Add(this.label138);
            this.groupBox3.Controls.Add(this.label142);
            this.groupBox3.Controls.Add(this.label101);
            this.groupBox3.Controls.Add(this.label135);
            this.groupBox3.Controls.Add(this.label136);
            this.groupBox3.Controls.Add(this.label137);
            this.groupBox3.Controls.Add(this.DeadlineDatePicker);
            this.groupBox3.Controls.Add(this.label133);
            this.groupBox3.Controls.Add(this.SendDatePicker);
            this.groupBox3.Controls.Add(this.label128);
            this.groupBox3.Controls.Add(this.SendAutoCreatedAdfastDeliveryReminderCheckBox);
            this.groupBox3.Controls.Add(this.label124);
            this.groupBox3.Controls.Add(this.AutoCreateAdfastDeliveryCheckBox);
            this.groupBox3.Controls.Add(this.label125);
            this.groupBox3.Controls.Add(this.CreateSavedDelivery);
            this.groupBox3.Controls.Add(this.DeliverWithoutChecks);
            this.groupBox3.Controls.Add(this.label89);
            this.groupBox3.Controls.Add(this.label96);
            this.groupBox3.Controls.Add(this.IgnoreSizeCheck);
            this.groupBox3.Controls.Add(this.IgnoreSizeCheckx);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.label95);
            this.groupBox3.Controls.Add(this.label123);
            this.groupBox3.Controls.Add(this.label122);
            this.groupBox3.Controls.Add(this.label121);
            this.groupBox3.Controls.Add(this.label120);
            this.groupBox3.Controls.Add(this.label119);
            this.groupBox3.Controls.Add(this.label118);
            this.groupBox3.Controls.Add(this.label114);
            this.groupBox3.Controls.Add(this.AccountNumberTextBox);
            this.groupBox3.Controls.Add(this.label111);
            this.groupBox3.Controls.Add(this.label106);
            this.groupBox3.Controls.Add(this.label105);
            this.groupBox3.Controls.Add(this.label104);
            this.groupBox3.Controls.Add(this.label103);
            this.groupBox3.Controls.Add(this.MultipleInsertionsSectionId);
            this.groupBox3.Controls.Add(this.label100);
            this.groupBox3.Controls.Add(this.label99);
            this.groupBox3.Controls.Add(this.InsertionDatePicker);
            this.groupBox3.Controls.Add(this.label81);
            this.groupBox3.Controls.Add(this.label98);
            this.groupBox3.Controls.Add(this.label50);
            this.groupBox3.Controls.Add(this.label97);
            this.groupBox3.Controls.Add(this.IgnoreErrorsAndDeliverOriginalFile);
            this.groupBox3.Controls.Add(this.label87);
            this.groupBox3.Controls.Add(this.AutoUpsampleFix);
            this.groupBox3.Controls.Add(this.label86);
            this.groupBox3.Controls.Add(this.AutoFontFix);
            this.groupBox3.Controls.Add(this.label85);
            this.groupBox3.Controls.Add(this.AutoDeliverOptimisedFile);
            this.groupBox3.Controls.Add(this.label84);
            this.groupBox3.Controls.Add(this.AutoDeliverFixedFile);
            this.groupBox3.Controls.Add(this.label83);
            this.groupBox3.Controls.Add(this.AutoDeliverCheckBox);
            this.groupBox3.Controls.Add(this.label82);
            this.groupBox3.Controls.Add(this.label45);
            this.groupBox3.Controls.Add(this.label61);
            this.groupBox3.Controls.Add(this.label79);
            this.groupBox3.Controls.Add(this.label80);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.label77);
            this.groupBox3.Controls.Add(this.label76);
            this.groupBox3.Controls.Add(this.label72);
            this.groupBox3.Controls.Add(this.label73);
            this.groupBox3.Controls.Add(this.ContactCompanyTextBox);
            this.groupBox3.Controls.Add(this.label74);
            this.groupBox3.Controls.Add(this.label56);
            this.groupBox3.Controls.Add(this.label71);
            this.groupBox3.Controls.Add(this.label70);
            this.groupBox3.Controls.Add(this.label69);
            this.groupBox3.Controls.Add(this.label68);
            this.groupBox3.Controls.Add(this.label67);
            this.groupBox3.Controls.Add(this.label66);
            this.groupBox3.Controls.Add(this.label65);
            this.groupBox3.Controls.Add(this.label64);
            this.groupBox3.Controls.Add(this.label63);
            this.groupBox3.Controls.Add(this.label62);
            this.groupBox3.Controls.Add(this.label60);
            this.groupBox3.Controls.Add(this.label59);
            this.groupBox3.Controls.Add(this.label58);
            this.groupBox3.Controls.Add(this.label57);
            this.groupBox3.Controls.Add(this.label54);
            this.groupBox3.Controls.Add(this.ContactEmailTextBox);
            this.groupBox3.Controls.Add(this.label55);
            this.groupBox3.Controls.Add(this.label51);
            this.groupBox3.Controls.Add(this.ContactPhoneTextBox);
            this.groupBox3.Controls.Add(this.label52);
            this.groupBox3.Controls.Add(this.label40);
            this.groupBox3.Controls.Add(this.ContactNameTextBox);
            this.groupBox3.Controls.Add(this.label41);
            this.groupBox3.Controls.Add(this.label39);
            this.groupBox3.Controls.Add(this.label42);
            this.groupBox3.Controls.Add(this.MultiInsertionDatePicker);
            this.groupBox3.Controls.Add(this.DontOptimiseRadioButton);
            this.groupBox3.Controls.Add(this.OptimiseRadioButton);
            this.groupBox3.Controls.Add(this.label53);
            this.groupBox3.Controls.Add(this.button5);
            this.groupBox3.Controls.Add(this.InsertionDateListBox);
            this.groupBox3.Controls.Add(this.button4);
            this.groupBox3.Controls.Add(this.CreateMailBoxDeliveryButton);
            this.groupBox3.Controls.Add(this.DeliveryToFTPUsernameTextBox);
            this.groupBox3.Controls.Add(this.label48);
            this.groupBox3.Controls.Add(this.label49);
            this.groupBox3.Controls.Add(this.label47);
            this.groupBox3.Controls.Add(this.label46);
            this.groupBox3.Controls.Add(this.label44);
            this.groupBox3.Controls.Add(this.label43);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.DeliveryToFtpPortTextBox);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.FilePathTextBox);
            this.groupBox3.Controls.Add(this.SelectFileButton);
            this.groupBox3.Controls.Add(this.DeliveryRuleDropDownList);
            this.groupBox3.Controls.Add(this.DeliveryToFtpPasswordTextBox);
            this.groupBox3.Controls.Add(this.DeliveryToFtpPathTextBox);
            this.groupBox3.Controls.Add(this.DeliveryToFtpHostTextBox);
            this.groupBox3.Controls.Add(this.DeliverToEmailAddressTextBox);
            this.groupBox3.Controls.Add(this.DeliveryToTitleTextBox);
            this.groupBox3.Controls.Add(this.BrandTextBox);
            this.groupBox3.Controls.Add(this.AdvertiserTextBox);
            this.groupBox3.Controls.Add(this.UrnTextBox);
            this.groupBox3.Controls.Add(this.CommentsTextBox);
            this.groupBox3.Controls.Add(this.HeightTextBox);
            this.groupBox3.Controls.Add(this.PublicationSectionSizeIdTextBox);
            this.groupBox3.Controls.Add(this.PublicationSectionIdTextBox);
            this.groupBox3.Controls.Add(this.SubmissionTypeDropDownList);
            this.groupBox3.Controls.Add(this.MonoCheckBox);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.SubmissionTypeLabel);
            this.groupBox3.Controls.Add(this.UploadFileButton);
            this.groupBox3.Controls.Add(this.label38);
            this.groupBox3.Controls.Add(this.label37);
            this.groupBox3.Controls.Add(this.label36);
            this.groupBox3.Controls.Add(this.label35);
            this.groupBox3.Controls.Add(this.label34);
            this.groupBox3.Controls.Add(this.label33);
            this.groupBox3.Controls.Add(this.label32);
            this.groupBox3.Controls.Add(this.label31);
            this.groupBox3.Controls.Add(this.label30);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.label94);
            this.groupBox3.Controls.Add(this.label93);
            this.groupBox3.Controls.Add(this.label92);
            this.groupBox3.Controls.Add(this.label91);
            this.groupBox3.Controls.Add(this.label90);
            this.groupBox3.Controls.Add(this.label88);
            this.groupBox3.Controls.Add(this.label78);
            this.groupBox3.Controls.Add(this.label102);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label129);
            this.groupBox3.Controls.Add(this.label127);
            this.groupBox3.Controls.Add(this.label126);
            this.groupBox3.Controls.Add(this.label112);
            this.groupBox3.Controls.Add(this.label141);
            this.groupBox3.Controls.Add(this.label134);
            this.groupBox3.Controls.Add(this.label148);
            this.groupBox3.Controls.Add(this.label113);
            this.groupBox3.Controls.Add(this.label152);
            this.groupBox3.Controls.Add(this.label149);
            this.groupBox3.Controls.Add(this.label150);
            this.groupBox3.Controls.Add(this.label147);
            this.groupBox3.Controls.Add(this.label107);
            this.groupBox3.Controls.Add(this.label165);
            this.groupBox3.Controls.Add(this.label167);
            this.groupBox3.Controls.Add(this.label168);
            this.groupBox3.Controls.Add(this.label169);
            this.groupBox3.Controls.Add(this.label170);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(20, 23);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(20, 19, 20, 19);
            this.groupBox3.Size = new System.Drawing.Size(1362, 1529);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Upload / Update";
            // 
            // IsPadnCheckbox
            // 
            this.IsPadnCheckbox.AutoSize = true;
            this.IsPadnCheckbox.Location = new System.Drawing.Point(896, 223);
            this.IsPadnCheckbox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.IsPadnCheckbox.Name = "IsPadnCheckbox";
            this.IsPadnCheckbox.Size = new System.Drawing.Size(141, 30);
            this.IsPadnCheckbox.TabIndex = 268;
            this.IsPadnCheckbox.Text = "PADN job";
            this.IsPadnCheckbox.UseVisualStyleBackColor = true;
            // 
            // InsertionPublicationSectionCodeTextBox
            // 
            this.InsertionPublicationSectionCodeTextBox.Location = new System.Drawing.Point(1180, 1058);
            this.InsertionPublicationSectionCodeTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.InsertionPublicationSectionCodeTextBox.Name = "InsertionPublicationSectionCodeTextBox";
            this.InsertionPublicationSectionCodeTextBox.Size = new System.Drawing.Size(76, 32);
            this.InsertionPublicationSectionCodeTextBox.TabIndex = 267;
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label166.ForeColor = System.Drawing.Color.Blue;
            this.label166.Location = new System.Drawing.Point(614, 323);
            this.label166.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(42, 53);
            this.label166.TabIndex = 265;
            this.label166.Text = "*";
            // 
            // PublisherCodeTextBox
            // 
            this.PublisherCodeTextBox.Location = new System.Drawing.Point(282, 319);
            this.PublisherCodeTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.PublisherCodeTextBox.Name = "PublisherCodeTextBox";
            this.PublisherCodeTextBox.Size = new System.Drawing.Size(316, 32);
            this.PublisherCodeTextBox.TabIndex = 264;
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Location = new System.Drawing.Point(118, 325);
            this.label163.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(161, 26);
            this.label163.TabIndex = 263;
            this.label163.Text = "PublisherCode:";
            // 
            // MailBoxDoNotSendEmailCheckBox
            // 
            this.MailBoxDoNotSendEmailCheckBox.AutoSize = true;
            this.MailBoxDoNotSendEmailCheckBox.Location = new System.Drawing.Point(1230, 1387);
            this.MailBoxDoNotSendEmailCheckBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.MailBoxDoNotSendEmailCheckBox.Name = "MailBoxDoNotSendEmailCheckBox";
            this.MailBoxDoNotSendEmailCheckBox.Size = new System.Drawing.Size(28, 27);
            this.MailBoxDoNotSendEmailCheckBox.TabIndex = 262;
            this.MailBoxDoNotSendEmailCheckBox.UseVisualStyleBackColor = true;
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Location = new System.Drawing.Point(840, 1387);
            this.label164.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(414, 26);
            this.label164.TabIndex = 261;
            this.label164.Text = "Email sent externally (No MailBox Email):";
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label160.ForeColor = System.Drawing.Color.Magenta;
            this.label160.Location = new System.Drawing.Point(672, 365);
            this.label160.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(42, 53);
            this.label160.TabIndex = 258;
            this.label160.Text = "*";
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label161.ForeColor = System.Drawing.Color.ForestGreen;
            this.label161.Location = new System.Drawing.Point(642, 365);
            this.label161.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(42, 53);
            this.label161.TabIndex = 259;
            this.label161.Text = "*";
            // 
            // label162
            // 
            this.label162.AutoSize = true;
            this.label162.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label162.ForeColor = System.Drawing.Color.Blue;
            this.label162.Location = new System.Drawing.Point(614, 365);
            this.label162.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(42, 53);
            this.label162.TabIndex = 257;
            this.label162.Text = "*";
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label156.ForeColor = System.Drawing.Color.Magenta;
            this.label156.Location = new System.Drawing.Point(672, 410);
            this.label156.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(42, 53);
            this.label156.TabIndex = 252;
            this.label156.Text = "*";
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label159.ForeColor = System.Drawing.Color.ForestGreen;
            this.label159.Location = new System.Drawing.Point(642, 410);
            this.label159.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(42, 53);
            this.label159.TabIndex = 256;
            this.label159.Text = "*";
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Location = new System.Drawing.Point(28, 367);
            this.label158.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(250, 26);
            this.label158.TabIndex = 255;
            this.label158.Text = "PublicationSectionCode:";
            // 
            // PublicationSectionCodeTextBox
            // 
            this.PublicationSectionCodeTextBox.Location = new System.Drawing.Point(282, 363);
            this.PublicationSectionCodeTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.PublicationSectionCodeTextBox.Name = "PublicationSectionCodeTextBox";
            this.PublicationSectionCodeTextBox.Size = new System.Drawing.Size(316, 32);
            this.PublicationSectionCodeTextBox.TabIndex = 254;
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Location = new System.Drawing.Point(30, 415);
            this.label157.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(248, 26);
            this.label157.TabIndex = 253;
            this.label157.Text = "Width (instead if SizeId):";
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label155.ForeColor = System.Drawing.Color.Blue;
            this.label155.Location = new System.Drawing.Point(614, 410);
            this.label155.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(42, 53);
            this.label155.TabIndex = 251;
            this.label155.Text = "*";
            // 
            // WidthTextBox
            // 
            this.WidthTextBox.Location = new System.Drawing.Point(282, 408);
            this.WidthTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.WidthTextBox.Name = "WidthTextBox";
            this.WidthTextBox.Size = new System.Drawing.Size(316, 32);
            this.WidthTextBox.TabIndex = 250;
            this.WidthTextBox.Text = "0";
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.ForeColor = System.Drawing.Color.Blue;
            this.label154.Location = new System.Drawing.Point(276, 288);
            this.label154.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(469, 26);
            this.label154.TabIndex = 249;
            this.label154.Text = "Publication Section Code + Height/Width below";
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.ForeColor = System.Drawing.Color.Blue;
            this.label153.Location = new System.Drawing.Point(276, 260);
            this.label153.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(357, 26);
            this.label153.TabIndex = 248;
            this.label153.Text = "Use the 2 IDs above OR Publisher, ";
            // 
            // SplitDpsDeliveryRightHandPageUrnTextBox
            // 
            this.SplitDpsDeliveryRightHandPageUrnTextBox.Location = new System.Drawing.Point(1104, 121);
            this.SplitDpsDeliveryRightHandPageUrnTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.SplitDpsDeliveryRightHandPageUrnTextBox.Name = "SplitDpsDeliveryRightHandPageUrnTextBox";
            this.SplitDpsDeliveryRightHandPageUrnTextBox.Size = new System.Drawing.Size(142, 32);
            this.SplitDpsDeliveryRightHandPageUrnTextBox.TabIndex = 241;
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Location = new System.Drawing.Point(1042, 127);
            this.label151.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(65, 26);
            this.label151.TabIndex = 240;
            this.label151.Text = "RHP:";
            // 
            // SplitDpsDeliveryLeftHandPageUrnTextBox
            // 
            this.SplitDpsDeliveryLeftHandPageUrnTextBox.Location = new System.Drawing.Point(896, 121);
            this.SplitDpsDeliveryLeftHandPageUrnTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.SplitDpsDeliveryLeftHandPageUrnTextBox.Name = "SplitDpsDeliveryLeftHandPageUrnTextBox";
            this.SplitDpsDeliveryLeftHandPageUrnTextBox.Size = new System.Drawing.Size(140, 32);
            this.SplitDpsDeliveryLeftHandPageUrnTextBox.TabIndex = 239;
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Location = new System.Drawing.Point(736, 127);
            this.label144.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(167, 26);
            this.label144.TabIndex = 238;
            this.label144.Text = "DPS URN LHP:";
            // 
            // ExternalDeliveryRefTextBox
            // 
            this.ExternalDeliveryRefTextBox.Location = new System.Drawing.Point(1008, 40);
            this.ExternalDeliveryRefTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.ExternalDeliveryRefTextBox.Name = "ExternalDeliveryRefTextBox";
            this.ExternalDeliveryRefTextBox.Size = new System.Drawing.Size(232, 32);
            this.ExternalDeliveryRefTextBox.TabIndex = 233;
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Location = new System.Drawing.Point(776, 48);
            this.label145.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(235, 26);
            this.label145.TabIndex = 232;
            this.label145.Text = "External Delivery. Ref.:";
            // 
            // SplitDPSFileCheckBox
            // 
            this.SplitDPSFileCheckBox.AutoSize = true;
            this.SplitDPSFileCheckBox.Location = new System.Drawing.Point(896, 88);
            this.SplitDPSFileCheckBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.SplitDPSFileCheckBox.Name = "SplitDPSFileCheckBox";
            this.SplitDPSFileCheckBox.Size = new System.Drawing.Size(224, 30);
            this.SplitDPSFileCheckBox.TabIndex = 230;
            this.SplitDPSFileCheckBox.Text = "Split DPS Delivery";
            this.SplitDPSFileCheckBox.UseVisualStyleBackColor = true;
            this.SplitDPSFileCheckBox.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label140.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label140.Location = new System.Drawing.Point(670, 1483);
            this.label140.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(42, 53);
            this.label140.TabIndex = 229;
            this.label140.Text = "*";
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.BackColor = System.Drawing.Color.Transparent;
            this.label132.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label132.ForeColor = System.Drawing.Color.ForestGreen;
            this.label132.Location = new System.Drawing.Point(642, 1483);
            this.label132.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(42, 53);
            this.label132.TabIndex = 228;
            this.label132.Text = "*";
            // 
            // QmuliStandardWorkflowIdTextBox
            // 
            this.QmuliStandardWorkflowIdTextBox.Location = new System.Drawing.Point(282, 1477);
            this.QmuliStandardWorkflowIdTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.QmuliStandardWorkflowIdTextBox.Name = "QmuliStandardWorkflowIdTextBox";
            this.QmuliStandardWorkflowIdTextBox.Size = new System.Drawing.Size(316, 32);
            this.QmuliStandardWorkflowIdTextBox.TabIndex = 226;
            this.QmuliStandardWorkflowIdTextBox.Text = "0";
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label139.ForeColor = System.Drawing.Color.Blue;
            this.label139.Location = new System.Drawing.Point(614, 1483);
            this.label139.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(42, 53);
            this.label139.TabIndex = 227;
            this.label139.Text = "*";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label109.ForeColor = System.Drawing.Color.Teal;
            this.label109.Location = new System.Drawing.Point(1286, 1340);
            this.label109.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(42, 53);
            this.label109.TabIndex = 224;
            this.label109.Text = "*";
            // 
            // MailBoxUrgentDeadlineCheckBox
            // 
            this.MailBoxUrgentDeadlineCheckBox.AutoSize = true;
            this.MailBoxUrgentDeadlineCheckBox.Location = new System.Drawing.Point(1230, 1344);
            this.MailBoxUrgentDeadlineCheckBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.MailBoxUrgentDeadlineCheckBox.Name = "MailBoxUrgentDeadlineCheckBox";
            this.MailBoxUrgentDeadlineCheckBox.Size = new System.Drawing.Size(28, 27);
            this.MailBoxUrgentDeadlineCheckBox.TabIndex = 223;
            this.MailBoxUrgentDeadlineCheckBox.UseVisualStyleBackColor = true;
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(1052, 1346);
            this.label108.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(175, 26);
            this.label108.TabIndex = 222;
            this.label108.Text = "Urgent Deadline:";
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label146.ForeColor = System.Drawing.Color.Magenta;
            this.label146.Location = new System.Drawing.Point(672, 221);
            this.label146.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(42, 53);
            this.label146.TabIndex = 220;
            this.label146.Text = "*";
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label143.ForeColor = System.Drawing.Color.Blue;
            this.label143.Location = new System.Drawing.Point(310, 1433);
            this.label143.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(42, 53);
            this.label143.TabIndex = 216;
            this.label143.Text = "*";
            // 
            // CropFileCheckBox
            // 
            this.CropFileCheckBox.AutoSize = true;
            this.CropFileCheckBox.Location = new System.Drawing.Point(282, 1438);
            this.CropFileCheckBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.CropFileCheckBox.Name = "CropFileCheckBox";
            this.CropFileCheckBox.Size = new System.Drawing.Size(28, 27);
            this.CropFileCheckBox.TabIndex = 215;
            this.CropFileCheckBox.UseVisualStyleBackColor = true;
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Location = new System.Drawing.Point(162, 1438);
            this.label138.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(106, 26);
            this.label138.TabIndex = 214;
            this.label138.Text = "Crop File:";
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label142.ForeColor = System.Drawing.Color.Teal;
            this.label142.Location = new System.Drawing.Point(1290, 608);
            this.label142.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(42, 53);
            this.label142.TabIndex = 213;
            this.label142.Text = "*";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.ForeColor = System.Drawing.Color.Magenta;
            this.label101.Location = new System.Drawing.Point(1262, 608);
            this.label101.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(42, 53);
            this.label101.TabIndex = 170;
            this.label101.Text = "*";
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label135.ForeColor = System.Drawing.Color.Teal;
            this.label135.Location = new System.Drawing.Point(1290, 908);
            this.label135.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(42, 53);
            this.label135.TabIndex = 210;
            this.label135.Text = "*";
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label136.ForeColor = System.Drawing.Color.Teal;
            this.label136.Location = new System.Drawing.Point(1288, 996);
            this.label136.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(42, 53);
            this.label136.TabIndex = 209;
            this.label136.Text = "*";
            this.label136.Visible = false;
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label137.ForeColor = System.Drawing.Color.Teal;
            this.label137.Location = new System.Drawing.Point(1288, 960);
            this.label137.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(42, 53);
            this.label137.TabIndex = 208;
            this.label137.Text = "*";
            // 
            // DeadlineDatePicker
            // 
            this.DeadlineDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DeadlineDatePicker.Location = new System.Drawing.Point(1058, 1154);
            this.DeadlineDatePicker.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.DeadlineDatePicker.Name = "DeadlineDatePicker";
            this.DeadlineDatePicker.Size = new System.Drawing.Size(200, 32);
            this.DeadlineDatePicker.TabIndex = 203;
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Location = new System.Drawing.Point(902, 1162);
            this.label133.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(156, 26);
            this.label133.TabIndex = 202;
            this.label133.Text = "Deadline Date:";
            // 
            // SendDatePicker
            // 
            this.SendDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.SendDatePicker.Location = new System.Drawing.Point(942, 908);
            this.SendDatePicker.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.SendDatePicker.Name = "SendDatePicker";
            this.SendDatePicker.Size = new System.Drawing.Size(316, 32);
            this.SendDatePicker.TabIndex = 200;
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(820, 913);
            this.label128.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(121, 26);
            this.label128.TabIndex = 199;
            this.label128.Text = "Send Date:";
            // 
            // SendAutoCreatedAdfastDeliveryReminderCheckBox
            // 
            this.SendAutoCreatedAdfastDeliveryReminderCheckBox.AutoSize = true;
            this.SendAutoCreatedAdfastDeliveryReminderCheckBox.Location = new System.Drawing.Point(1232, 1000);
            this.SendAutoCreatedAdfastDeliveryReminderCheckBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.SendAutoCreatedAdfastDeliveryReminderCheckBox.Name = "SendAutoCreatedAdfastDeliveryReminderCheckBox";
            this.SendAutoCreatedAdfastDeliveryReminderCheckBox.Size = new System.Drawing.Size(28, 27);
            this.SendAutoCreatedAdfastDeliveryReminderCheckBox.TabIndex = 196;
            this.SendAutoCreatedAdfastDeliveryReminderCheckBox.UseVisualStyleBackColor = true;
            this.SendAutoCreatedAdfastDeliveryReminderCheckBox.Visible = false;
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(788, 1000);
            this.label124.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(457, 26);
            this.label124.TabIndex = 195;
            this.label124.Text = "Send Auto Created Adfast Delivery Reminder:";
            this.label124.Visible = false;
            // 
            // AutoCreateAdfastDeliveryCheckBox
            // 
            this.AutoCreateAdfastDeliveryCheckBox.AutoSize = true;
            this.AutoCreateAdfastDeliveryCheckBox.Location = new System.Drawing.Point(1232, 963);
            this.AutoCreateAdfastDeliveryCheckBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.AutoCreateAdfastDeliveryCheckBox.Name = "AutoCreateAdfastDeliveryCheckBox";
            this.AutoCreateAdfastDeliveryCheckBox.Size = new System.Drawing.Size(28, 27);
            this.AutoCreateAdfastDeliveryCheckBox.TabIndex = 194;
            this.AutoCreateAdfastDeliveryCheckBox.UseVisualStyleBackColor = true;
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(774, 963);
            this.label125.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(481, 26);
            this.label125.TabIndex = 193;
            this.label125.Text = "Auto Create Adfast Delivery (No MailBox Email):";
            // 
            // CreateSavedDelivery
            // 
            this.CreateSavedDelivery.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateSavedDelivery.ForeColor = System.Drawing.Color.ForestGreen;
            this.CreateSavedDelivery.Location = new System.Drawing.Point(800, 687);
            this.CreateSavedDelivery.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.CreateSavedDelivery.Name = "CreateSavedDelivery";
            this.CreateSavedDelivery.Size = new System.Drawing.Size(464, 77);
            this.CreateSavedDelivery.TabIndex = 70;
            this.CreateSavedDelivery.Text = "Create Delivery - No File Attached (Saved)";
            this.CreateSavedDelivery.UseVisualStyleBackColor = true;
            this.CreateSavedDelivery.Click += new System.EventHandler(this.CreateSavedDelivery_Click);
            // 
            // DeliverWithoutChecks
            // 
            this.DeliverWithoutChecks.AutoSize = true;
            this.DeliverWithoutChecks.Location = new System.Drawing.Point(1180, 652);
            this.DeliverWithoutChecks.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.DeliverWithoutChecks.Name = "DeliverWithoutChecks";
            this.DeliverWithoutChecks.Size = new System.Drawing.Size(28, 27);
            this.DeliverWithoutChecks.TabIndex = 153;
            this.DeliverWithoutChecks.UseVisualStyleBackColor = true;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(940, 652);
            this.label89.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(233, 26);
            this.label89.TabIndex = 152;
            this.label89.Text = "DeliverWithoutChecks:";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.ForeColor = System.Drawing.Color.ForestGreen;
            this.label96.Location = new System.Drawing.Point(1206, 650);
            this.label96.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(42, 53);
            this.label96.TabIndex = 161;
            this.label96.Text = "*";
            // 
            // IgnoreSizeCheck
            // 
            this.IgnoreSizeCheck.AutoSize = true;
            this.IgnoreSizeCheck.Location = new System.Drawing.Point(1180, 612);
            this.IgnoreSizeCheck.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.IgnoreSizeCheck.Name = "IgnoreSizeCheck";
            this.IgnoreSizeCheck.Size = new System.Drawing.Size(28, 27);
            this.IgnoreSizeCheck.TabIndex = 151;
            this.IgnoreSizeCheck.UseVisualStyleBackColor = true;
            // 
            // IgnoreSizeCheckx
            // 
            this.IgnoreSizeCheckx.AutoSize = true;
            this.IgnoreSizeCheckx.Location = new System.Drawing.Point(988, 613);
            this.IgnoreSizeCheckx.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.IgnoreSizeCheckx.Name = "IgnoreSizeCheckx";
            this.IgnoreSizeCheckx.Size = new System.Drawing.Size(184, 26);
            this.IgnoreSizeCheckx.TabIndex = 150;
            this.IgnoreSizeCheckx.Text = "IgnoreSizeCheck:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Blue;
            this.label26.Location = new System.Drawing.Point(1234, 608);
            this.label26.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(42, 53);
            this.label26.TabIndex = 97;
            this.label26.Text = "*";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label95.ForeColor = System.Drawing.Color.ForestGreen;
            this.label95.Location = new System.Drawing.Point(1206, 608);
            this.label95.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(42, 53);
            this.label95.TabIndex = 160;
            this.label95.Text = "*";
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label123.ForeColor = System.Drawing.Color.Teal;
            this.label123.Location = new System.Drawing.Point(734, 1087);
            this.label123.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(42, 53);
            this.label123.TabIndex = 192;
            this.label123.Text = "*";
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label122.ForeColor = System.Drawing.Color.Teal;
            this.label122.Location = new System.Drawing.Point(728, 783);
            this.label122.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(42, 53);
            this.label122.TabIndex = 191;
            this.label122.Text = "*";
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label121.ForeColor = System.Drawing.Color.Teal;
            this.label121.Location = new System.Drawing.Point(730, 727);
            this.label121.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(42, 53);
            this.label121.TabIndex = 190;
            this.label121.Text = "*";
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label120.ForeColor = System.Drawing.Color.Teal;
            this.label120.Location = new System.Drawing.Point(730, 673);
            this.label120.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(42, 53);
            this.label120.TabIndex = 189;
            this.label120.Text = "*";
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label119.ForeColor = System.Drawing.Color.Teal;
            this.label119.Location = new System.Drawing.Point(728, 625);
            this.label119.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(42, 53);
            this.label119.TabIndex = 188;
            this.label119.Text = "*";
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label118.ForeColor = System.Drawing.Color.Magenta;
            this.label118.Location = new System.Drawing.Point(1270, 1458);
            this.label118.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(42, 53);
            this.label118.TabIndex = 187;
            this.label118.Text = "*";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label114.ForeColor = System.Drawing.Color.Blue;
            this.label114.Location = new System.Drawing.Point(1270, 271);
            this.label114.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(42, 53);
            this.label114.TabIndex = 186;
            this.label114.Text = "*";
            // 
            // AccountNumberTextBox
            // 
            this.AccountNumberTextBox.Location = new System.Drawing.Point(942, 858);
            this.AccountNumberTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.AccountNumberTextBox.Name = "AccountNumberTextBox";
            this.AccountNumberTextBox.Size = new System.Drawing.Size(316, 32);
            this.AccountNumberTextBox.TabIndex = 183;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(798, 863);
            this.label111.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(145, 26);
            this.label111.TabIndex = 182;
            this.label111.Text = "Acc. Number:";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label106.ForeColor = System.Drawing.Color.Magenta;
            this.label106.Location = new System.Drawing.Point(698, 623);
            this.label106.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(42, 53);
            this.label106.TabIndex = 175;
            this.label106.Text = "*";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label105.ForeColor = System.Drawing.Color.Magenta;
            this.label105.Location = new System.Drawing.Point(698, 673);
            this.label105.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(42, 53);
            this.label105.TabIndex = 174;
            this.label105.Text = "*";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label104.ForeColor = System.Drawing.Color.Magenta;
            this.label104.Location = new System.Drawing.Point(700, 781);
            this.label104.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(42, 53);
            this.label104.TabIndex = 173;
            this.label104.Text = "*";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label103.ForeColor = System.Drawing.Color.Magenta;
            this.label103.Location = new System.Drawing.Point(700, 727);
            this.label103.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(42, 53);
            this.label103.TabIndex = 172;
            this.label103.Text = "*";
            // 
            // MultipleInsertionsSectionId
            // 
            this.MultipleInsertionsSectionId.Location = new System.Drawing.Point(994, 1058);
            this.MultipleInsertionsSectionId.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.MultipleInsertionsSectionId.Name = "MultipleInsertionsSectionId";
            this.MultipleInsertionsSectionId.Size = new System.Drawing.Size(72, 32);
            this.MultipleInsertionsSectionId.TabIndex = 124;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label100.ForeColor = System.Drawing.Color.Magenta;
            this.label100.Location = new System.Drawing.Point(702, 1085);
            this.label100.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(42, 53);
            this.label100.TabIndex = 169;
            this.label100.Text = "*";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.ForeColor = System.Drawing.Color.Magenta;
            this.label99.Location = new System.Drawing.Point(672, 481);
            this.label99.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(42, 53);
            this.label99.TabIndex = 168;
            this.label99.Text = "*";
            // 
            // InsertionDatePicker
            // 
            this.InsertionDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.InsertionDatePicker.Location = new System.Drawing.Point(282, 567);
            this.InsertionDatePicker.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.InsertionDatePicker.Name = "InsertionDatePicker";
            this.InsertionDatePicker.Size = new System.Drawing.Size(316, 32);
            this.InsertionDatePicker.TabIndex = 27;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.ForeColor = System.Drawing.Color.ForestGreen;
            this.label81.Location = new System.Drawing.Point(342, 523);
            this.label81.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(42, 53);
            this.label81.TabIndex = 137;
            this.label81.Text = "*";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.ForeColor = System.Drawing.Color.Blue;
            this.label98.Location = new System.Drawing.Point(1234, 569);
            this.label98.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(42, 53);
            this.label98.TabIndex = 165;
            this.label98.Text = "*";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Blue;
            this.label50.Location = new System.Drawing.Point(1236, 492);
            this.label50.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(42, 53);
            this.label50.TabIndex = 164;
            this.label50.Text = "*";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.ForeColor = System.Drawing.Color.ForestGreen;
            this.label97.Location = new System.Drawing.Point(1274, 708);
            this.label97.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(42, 53);
            this.label97.TabIndex = 162;
            this.label97.Text = "*";
            // 
            // IgnoreErrorsAndDeliverOriginalFile
            // 
            this.IgnoreErrorsAndDeliverOriginalFile.AutoSize = true;
            this.IgnoreErrorsAndDeliverOriginalFile.Location = new System.Drawing.Point(1180, 571);
            this.IgnoreErrorsAndDeliverOriginalFile.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.IgnoreErrorsAndDeliverOriginalFile.Name = "IgnoreErrorsAndDeliverOriginalFile";
            this.IgnoreErrorsAndDeliverOriginalFile.Size = new System.Drawing.Size(28, 27);
            this.IgnoreErrorsAndDeliverOriginalFile.TabIndex = 149;
            this.IgnoreErrorsAndDeliverOriginalFile.UseVisualStyleBackColor = true;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(836, 573);
            this.label87.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(349, 26);
            this.label87.TabIndex = 148;
            this.label87.Text = "IgnoreErrorsAndDeliverOriginalFile";
            // 
            // AutoUpsampleFix
            // 
            this.AutoUpsampleFix.AutoSize = true;
            this.AutoUpsampleFix.Location = new System.Drawing.Point(1180, 533);
            this.AutoUpsampleFix.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.AutoUpsampleFix.Name = "AutoUpsampleFix";
            this.AutoUpsampleFix.Size = new System.Drawing.Size(28, 27);
            this.AutoUpsampleFix.TabIndex = 147;
            this.AutoUpsampleFix.UseVisualStyleBackColor = true;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(988, 535);
            this.label86.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(191, 26);
            this.label86.TabIndex = 146;
            this.label86.Text = "AutoUpsampleFix:";
            // 
            // AutoFontFix
            // 
            this.AutoFontFix.AutoSize = true;
            this.AutoFontFix.Location = new System.Drawing.Point(1180, 494);
            this.AutoFontFix.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.AutoFontFix.Name = "AutoFontFix";
            this.AutoFontFix.Size = new System.Drawing.Size(28, 27);
            this.AutoFontFix.TabIndex = 145;
            this.AutoFontFix.UseVisualStyleBackColor = true;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(1038, 496);
            this.label85.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(135, 26);
            this.label85.TabIndex = 144;
            this.label85.Text = "AutoFontFix:";
            // 
            // AutoDeliverOptimisedFile
            // 
            this.AutoDeliverOptimisedFile.AutoSize = true;
            this.AutoDeliverOptimisedFile.Location = new System.Drawing.Point(1180, 456);
            this.AutoDeliverOptimisedFile.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.AutoDeliverOptimisedFile.Name = "AutoDeliverOptimisedFile";
            this.AutoDeliverOptimisedFile.Size = new System.Drawing.Size(28, 27);
            this.AutoDeliverOptimisedFile.TabIndex = 143;
            this.AutoDeliverOptimisedFile.UseVisualStyleBackColor = true;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(918, 458);
            this.label84.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(265, 26);
            this.label84.TabIndex = 142;
            this.label84.Text = "AutoDeliverOptimisedFile:";
            // 
            // AutoDeliverFixedFile
            // 
            this.AutoDeliverFixedFile.AutoSize = true;
            this.AutoDeliverFixedFile.Location = new System.Drawing.Point(1180, 419);
            this.AutoDeliverFixedFile.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.AutoDeliverFixedFile.Name = "AutoDeliverFixedFile";
            this.AutoDeliverFixedFile.Size = new System.Drawing.Size(28, 27);
            this.AutoDeliverFixedFile.TabIndex = 141;
            this.AutoDeliverFixedFile.UseVisualStyleBackColor = true;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(958, 419);
            this.label83.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(219, 26);
            this.label83.TabIndex = 140;
            this.label83.Text = "AutoDeliverFixedFile:";
            // 
            // AutoDeliverCheckBox
            // 
            this.AutoDeliverCheckBox.AutoSize = true;
            this.AutoDeliverCheckBox.Location = new System.Drawing.Point(1180, 383);
            this.AutoDeliverCheckBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.AutoDeliverCheckBox.Name = "AutoDeliverCheckBox";
            this.AutoDeliverCheckBox.Size = new System.Drawing.Size(28, 27);
            this.AutoDeliverCheckBox.TabIndex = 139;
            this.AutoDeliverCheckBox.UseVisualStyleBackColor = true;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(1042, 385);
            this.label82.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(131, 26);
            this.label82.TabIndex = 138;
            this.label82.Text = "AutoDeliver:";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.ForestGreen;
            this.label45.Location = new System.Drawing.Point(642, 823);
            this.label45.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(42, 53);
            this.label45.TabIndex = 75;
            this.label45.Text = "*";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label61.Location = new System.Drawing.Point(670, 781);
            this.label61.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(42, 53);
            this.label61.TabIndex = 113;
            this.label61.Text = "*";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.ForeColor = System.Drawing.Color.ForestGreen;
            this.label79.Location = new System.Drawing.Point(642, 781);
            this.label79.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(42, 53);
            this.label79.TabIndex = 135;
            this.label79.Text = "*";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.ForeColor = System.Drawing.Color.Blue;
            this.label80.Location = new System.Drawing.Point(614, 781);
            this.label80.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(42, 53);
            this.label80.TabIndex = 136;
            this.label80.Text = "*";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.ForestGreen;
            this.label19.Location = new System.Drawing.Point(642, 481);
            this.label19.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(42, 53);
            this.label19.TabIndex = 133;
            this.label19.Text = "*";
            // 
            // label77
            // 
            this.label77.Location = new System.Drawing.Point(860, 1106);
            this.label77.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(198, 35);
            this.label77.TabIndex = 131;
            this.label77.Text = "Insertion Date:";
            this.label77.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label76
            // 
            this.label76.Location = new System.Drawing.Point(762, 1058);
            this.label76.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(236, 35);
            this.label76.TabIndex = 130;
            this.label76.Text = "Publication Section Id:";
            this.label76.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label72.Location = new System.Drawing.Point(670, 929);
            this.label72.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(42, 53);
            this.label72.TabIndex = 129;
            this.label72.Text = "*";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(96, 935);
            this.label73.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(187, 26);
            this.label73.TabIndex = 127;
            this.label73.Text = "ContactCompany:";
            // 
            // ContactCompanyTextBox
            // 
            this.ContactCompanyTextBox.Location = new System.Drawing.Point(282, 929);
            this.ContactCompanyTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.ContactCompanyTextBox.Name = "ContactCompanyTextBox";
            this.ContactCompanyTextBox.Size = new System.Drawing.Size(316, 32);
            this.ContactCompanyTextBox.TabIndex = 126;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.Color.Blue;
            this.label74.Location = new System.Drawing.Point(614, 929);
            this.label74.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(42, 53);
            this.label74.TabIndex = 128;
            this.label74.Text = "*";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label56.Location = new System.Drawing.Point(670, 98);
            this.label56.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(42, 53);
            this.label56.TabIndex = 125;
            this.label56.Text = "*";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label71.Location = new System.Drawing.Point(670, 1033);
            this.label71.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(42, 53);
            this.label71.TabIndex = 123;
            this.label71.Text = "*";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label70.Location = new System.Drawing.Point(670, 979);
            this.label70.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(42, 53);
            this.label70.TabIndex = 122;
            this.label70.Text = "*";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label69.Location = new System.Drawing.Point(670, 879);
            this.label69.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(42, 53);
            this.label69.TabIndex = 121;
            this.label69.Text = "*";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label68.Location = new System.Drawing.Point(670, 1388);
            this.label68.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(42, 53);
            this.label68.TabIndex = 120;
            this.label68.Text = "*";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label67.Location = new System.Drawing.Point(670, 1340);
            this.label67.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(42, 53);
            this.label67.TabIndex = 119;
            this.label67.Text = "*";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label66.Location = new System.Drawing.Point(670, 1288);
            this.label66.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(42, 53);
            this.label66.TabIndex = 118;
            this.label66.Text = "*";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label65.Location = new System.Drawing.Point(670, 1233);
            this.label65.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(42, 53);
            this.label65.TabIndex = 117;
            this.label65.Text = "*";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label64.Location = new System.Drawing.Point(670, 1183);
            this.label64.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(42, 53);
            this.label64.TabIndex = 116;
            this.label64.Text = "*";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label63.Location = new System.Drawing.Point(670, 1133);
            this.label63.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(42, 53);
            this.label63.TabIndex = 115;
            this.label63.Text = "*";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label62.Location = new System.Drawing.Point(670, 1085);
            this.label62.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(42, 53);
            this.label62.TabIndex = 114;
            this.label62.Text = "*";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label60.Location = new System.Drawing.Point(670, 725);
            this.label60.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(42, 53);
            this.label60.TabIndex = 112;
            this.label60.Text = "*";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label59.Location = new System.Drawing.Point(670, 673);
            this.label59.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(42, 53);
            this.label59.TabIndex = 111;
            this.label59.Text = "*";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label58.Location = new System.Drawing.Point(670, 623);
            this.label58.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(42, 53);
            this.label58.TabIndex = 110;
            this.label58.Text = "*";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label57.Location = new System.Drawing.Point(670, 567);
            this.label57.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(42, 53);
            this.label57.TabIndex = 109;
            this.label57.Text = "*";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(130, 1037);
            this.label54.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(149, 26);
            this.label54.TabIndex = 106;
            this.label54.Text = "ContactEmail:";
            // 
            // ContactEmailTextBox
            // 
            this.ContactEmailTextBox.Location = new System.Drawing.Point(282, 1031);
            this.ContactEmailTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.ContactEmailTextBox.Name = "ContactEmailTextBox";
            this.ContactEmailTextBox.Size = new System.Drawing.Size(316, 32);
            this.ContactEmailTextBox.TabIndex = 105;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Blue;
            this.label55.Location = new System.Drawing.Point(614, 1031);
            this.label55.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(42, 53);
            this.label55.TabIndex = 107;
            this.label55.Text = "*";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(120, 985);
            this.label51.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(156, 26);
            this.label51.TabIndex = 103;
            this.label51.Text = "ContactPhone:";
            // 
            // ContactPhoneTextBox
            // 
            this.ContactPhoneTextBox.Location = new System.Drawing.Point(282, 979);
            this.ContactPhoneTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.ContactPhoneTextBox.Name = "ContactPhoneTextBox";
            this.ContactPhoneTextBox.Size = new System.Drawing.Size(316, 32);
            this.ContactPhoneTextBox.TabIndex = 102;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.Blue;
            this.label52.Location = new System.Drawing.Point(614, 979);
            this.label52.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(42, 53);
            this.label52.TabIndex = 104;
            this.label52.Text = "*";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(130, 885);
            this.label40.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(152, 26);
            this.label40.TabIndex = 100;
            this.label40.Text = "ContactName:";
            // 
            // ContactNameTextBox
            // 
            this.ContactNameTextBox.Location = new System.Drawing.Point(282, 879);
            this.ContactNameTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.ContactNameTextBox.Name = "ContactNameTextBox";
            this.ContactNameTextBox.Size = new System.Drawing.Size(316, 32);
            this.ContactNameTextBox.TabIndex = 99;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Blue;
            this.label41.Location = new System.Drawing.Point(614, 879);
            this.label41.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(42, 53);
            this.label41.TabIndex = 101;
            this.label41.Text = "*";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.ForestGreen;
            this.label39.Location = new System.Drawing.Point(642, 623);
            this.label39.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(42, 53);
            this.label39.TabIndex = 98;
            this.label39.Text = "*";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.BackColor = System.Drawing.Color.Transparent;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.ForestGreen;
            this.label42.Location = new System.Drawing.Point(642, 169);
            this.label42.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(42, 53);
            this.label42.TabIndex = 72;
            this.label42.Text = "*";
            // 
            // MultiInsertionDatePicker
            // 
            this.MultiInsertionDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.MultiInsertionDatePicker.Location = new System.Drawing.Point(1058, 1106);
            this.MultiInsertionDatePicker.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.MultiInsertionDatePicker.Name = "MultiInsertionDatePicker";
            this.MultiInsertionDatePicker.Size = new System.Drawing.Size(198, 32);
            this.MultiInsertionDatePicker.TabIndex = 94;
            // 
            // DontOptimiseRadioButton
            // 
            this.DontOptimiseRadioButton.AutoSize = true;
            this.DontOptimiseRadioButton.Checked = true;
            this.DontOptimiseRadioButton.Location = new System.Drawing.Point(424, 827);
            this.DontOptimiseRadioButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.DontOptimiseRadioButton.Name = "DontOptimiseRadioButton";
            this.DontOptimiseRadioButton.Size = new System.Drawing.Size(186, 30);
            this.DontOptimiseRadioButton.TabIndex = 93;
            this.DontOptimiseRadioButton.TabStop = true;
            this.DontOptimiseRadioButton.Text = "Don\'t Optimise";
            this.DontOptimiseRadioButton.UseVisualStyleBackColor = true;
            // 
            // OptimiseRadioButton
            // 
            this.OptimiseRadioButton.AutoSize = true;
            this.OptimiseRadioButton.Location = new System.Drawing.Point(282, 827);
            this.OptimiseRadioButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.OptimiseRadioButton.Name = "OptimiseRadioButton";
            this.OptimiseRadioButton.Size = new System.Drawing.Size(130, 30);
            this.OptimiseRadioButton.TabIndex = 92;
            this.OptimiseRadioButton.Text = "Optimise";
            this.OptimiseRadioButton.UseVisualStyleBackColor = true;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(64, 831);
            this.label53.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(210, 26);
            this.label53.TabIndex = 91;
            this.label53.Text = "Colour Optimisation:";
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(1080, 1196);
            this.button5.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(90, 38);
            this.button5.TabIndex = 90;
            this.button5.Text = "Clear";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // InsertionDateListBox
            // 
            this.InsertionDateListBox.FormattingEnabled = true;
            this.InsertionDateListBox.ItemHeight = 26;
            this.InsertionDateListBox.Location = new System.Drawing.Point(942, 1240);
            this.InsertionDateListBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.InsertionDateListBox.Name = "InsertionDateListBox";
            this.InsertionDateListBox.Size = new System.Drawing.Size(316, 56);
            this.InsertionDateListBox.TabIndex = 89;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(1180, 1196);
            this.button4.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(82, 38);
            this.button4.TabIndex = 86;
            this.button4.Text = "Add";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // CreateMailBoxDeliveryButton
            // 
            this.CreateMailBoxDeliveryButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateMailBoxDeliveryButton.ForeColor = System.Drawing.Color.Magenta;
            this.CreateMailBoxDeliveryButton.Location = new System.Drawing.Point(798, 1433);
            this.CreateMailBoxDeliveryButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.CreateMailBoxDeliveryButton.Name = "CreateMailBoxDeliveryButton";
            this.CreateMailBoxDeliveryButton.Size = new System.Drawing.Size(466, 77);
            this.CreateMailBoxDeliveryButton.TabIndex = 81;
            this.CreateMailBoxDeliveryButton.Text = "Create MailBox Delivery";
            this.CreateMailBoxDeliveryButton.UseVisualStyleBackColor = true;
            this.CreateMailBoxDeliveryButton.Click += new System.EventHandler(this.button3_Click);
            // 
            // DeliveryToFTPUsernameTextBox
            // 
            this.DeliveryToFTPUsernameTextBox.Location = new System.Drawing.Point(282, 1233);
            this.DeliveryToFTPUsernameTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.DeliveryToFTPUsernameTextBox.Name = "DeliveryToFTPUsernameTextBox";
            this.DeliveryToFTPUsernameTextBox.Size = new System.Drawing.Size(316, 32);
            this.DeliveryToFTPUsernameTextBox.TabIndex = 79;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(34, 1238);
            this.label48.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(253, 26);
            this.label48.TabIndex = 78;
            this.label48.Text = "DeliveryToFtpUsername:";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Blue;
            this.label49.Location = new System.Drawing.Point(614, 1233);
            this.label49.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(42, 53);
            this.label49.TabIndex = 80;
            this.label49.Text = "*";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.ForestGreen;
            this.label47.Location = new System.Drawing.Point(642, 219);
            this.label47.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(42, 53);
            this.label47.TabIndex = 77;
            this.label47.Text = "*";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.ForestGreen;
            this.label46.Location = new System.Drawing.Point(642, 567);
            this.label46.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(42, 53);
            this.label46.TabIndex = 76;
            this.label46.Text = "*";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.ForestGreen;
            this.label44.Location = new System.Drawing.Point(642, 725);
            this.label44.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(42, 53);
            this.label44.TabIndex = 74;
            this.label44.Text = "*";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.ForestGreen;
            this.label43.Location = new System.Drawing.Point(642, 673);
            this.label43.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(42, 53);
            this.label43.TabIndex = 73;
            this.label43.Text = "*";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(74, 1394);
            this.label21.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(192, 26);
            this.label21.TabIndex = 49;
            this.label21.Text = "DeliveryToFtpPort:";
            // 
            // DeliveryToFtpPortTextBox
            // 
            this.DeliveryToFtpPortTextBox.Location = new System.Drawing.Point(282, 1388);
            this.DeliveryToFtpPortTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.DeliveryToFtpPortTextBox.Name = "DeliveryToFtpPortTextBox";
            this.DeliveryToFtpPortTextBox.Size = new System.Drawing.Size(316, 32);
            this.DeliveryToFtpPortTextBox.TabIndex = 48;
            this.DeliveryToFtpPortTextBox.Text = "21";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(776, 179);
            this.label15.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(120, 26);
            this.label15.TabIndex = 45;
            this.label15.Text = "Select File:";
            // 
            // FilePathTextBox
            // 
            this.FilePathTextBox.Location = new System.Drawing.Point(896, 171);
            this.FilePathTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.FilePathTextBox.Name = "FilePathTextBox";
            this.FilePathTextBox.Size = new System.Drawing.Size(278, 32);
            this.FilePathTextBox.TabIndex = 44;
            // 
            // SelectFileButton
            // 
            this.SelectFileButton.Location = new System.Drawing.Point(1184, 167);
            this.SelectFileButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.SelectFileButton.Name = "SelectFileButton";
            this.SelectFileButton.Size = new System.Drawing.Size(70, 42);
            this.SelectFileButton.TabIndex = 43;
            this.SelectFileButton.Text = "...";
            this.SelectFileButton.UseVisualStyleBackColor = true;
            this.SelectFileButton.Click += new System.EventHandler(this.SelectFileButton_Click);
            // 
            // DeliveryRuleDropDownList
            // 
            this.DeliveryRuleDropDownList.FormattingEnabled = true;
            this.DeliveryRuleDropDownList.Location = new System.Drawing.Point(282, 90);
            this.DeliveryRuleDropDownList.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.DeliveryRuleDropDownList.Name = "DeliveryRuleDropDownList";
            this.DeliveryRuleDropDownList.Size = new System.Drawing.Size(316, 34);
            this.DeliveryRuleDropDownList.TabIndex = 42;
            // 
            // DeliveryToFtpPasswordTextBox
            // 
            this.DeliveryToFtpPasswordTextBox.Location = new System.Drawing.Point(282, 1288);
            this.DeliveryToFtpPasswordTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.DeliveryToFtpPasswordTextBox.Name = "DeliveryToFtpPasswordTextBox";
            this.DeliveryToFtpPasswordTextBox.Size = new System.Drawing.Size(316, 32);
            this.DeliveryToFtpPasswordTextBox.TabIndex = 41;
            // 
            // DeliveryToFtpPathTextBox
            // 
            this.DeliveryToFtpPathTextBox.Location = new System.Drawing.Point(282, 1338);
            this.DeliveryToFtpPathTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.DeliveryToFtpPathTextBox.Name = "DeliveryToFtpPathTextBox";
            this.DeliveryToFtpPathTextBox.Size = new System.Drawing.Size(316, 32);
            this.DeliveryToFtpPathTextBox.TabIndex = 40;
            // 
            // DeliveryToFtpHostTextBox
            // 
            this.DeliveryToFtpHostTextBox.Location = new System.Drawing.Point(282, 1183);
            this.DeliveryToFtpHostTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.DeliveryToFtpHostTextBox.Name = "DeliveryToFtpHostTextBox";
            this.DeliveryToFtpHostTextBox.Size = new System.Drawing.Size(316, 32);
            this.DeliveryToFtpHostTextBox.TabIndex = 39;
            // 
            // DeliverToEmailAddressTextBox
            // 
            this.DeliverToEmailAddressTextBox.Location = new System.Drawing.Point(282, 1083);
            this.DeliverToEmailAddressTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.DeliverToEmailAddressTextBox.Name = "DeliverToEmailAddressTextBox";
            this.DeliverToEmailAddressTextBox.Size = new System.Drawing.Size(316, 32);
            this.DeliverToEmailAddressTextBox.TabIndex = 38;
            // 
            // DeliveryToTitleTextBox
            // 
            this.DeliveryToTitleTextBox.Location = new System.Drawing.Point(282, 1133);
            this.DeliveryToTitleTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.DeliveryToTitleTextBox.Name = "DeliveryToTitleTextBox";
            this.DeliveryToTitleTextBox.Size = new System.Drawing.Size(316, 32);
            this.DeliveryToTitleTextBox.TabIndex = 37;
            // 
            // BrandTextBox
            // 
            this.BrandTextBox.Location = new System.Drawing.Point(282, 775);
            this.BrandTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.BrandTextBox.Name = "BrandTextBox";
            this.BrandTextBox.Size = new System.Drawing.Size(316, 32);
            this.BrandTextBox.TabIndex = 36;
            // 
            // AdvertiserTextBox
            // 
            this.AdvertiserTextBox.Location = new System.Drawing.Point(282, 719);
            this.AdvertiserTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.AdvertiserTextBox.Name = "AdvertiserTextBox";
            this.AdvertiserTextBox.Size = new System.Drawing.Size(316, 32);
            this.AdvertiserTextBox.TabIndex = 35;
            // 
            // UrnTextBox
            // 
            this.UrnTextBox.Location = new System.Drawing.Point(282, 669);
            this.UrnTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.UrnTextBox.Name = "UrnTextBox";
            this.UrnTextBox.Size = new System.Drawing.Size(316, 32);
            this.UrnTextBox.TabIndex = 34;
            // 
            // CommentsTextBox
            // 
            this.CommentsTextBox.Location = new System.Drawing.Point(282, 617);
            this.CommentsTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.CommentsTextBox.Name = "CommentsTextBox";
            this.CommentsTextBox.Size = new System.Drawing.Size(316, 32);
            this.CommentsTextBox.TabIndex = 33;
            // 
            // HeightTextBox
            // 
            this.HeightTextBox.Location = new System.Drawing.Point(282, 475);
            this.HeightTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.HeightTextBox.Name = "HeightTextBox";
            this.HeightTextBox.Size = new System.Drawing.Size(316, 32);
            this.HeightTextBox.TabIndex = 32;
            this.HeightTextBox.Text = "0";
            // 
            // PublicationSectionSizeIdTextBox
            // 
            this.PublicationSectionSizeIdTextBox.Location = new System.Drawing.Point(282, 213);
            this.PublicationSectionSizeIdTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.PublicationSectionSizeIdTextBox.Name = "PublicationSectionSizeIdTextBox";
            this.PublicationSectionSizeIdTextBox.Size = new System.Drawing.Size(316, 32);
            this.PublicationSectionSizeIdTextBox.TabIndex = 30;
            this.PublicationSectionSizeIdTextBox.Text = "0";
            // 
            // PublicationSectionIdTextBox
            // 
            this.PublicationSectionIdTextBox.Location = new System.Drawing.Point(282, 163);
            this.PublicationSectionIdTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.PublicationSectionIdTextBox.Name = "PublicationSectionIdTextBox";
            this.PublicationSectionIdTextBox.Size = new System.Drawing.Size(316, 32);
            this.PublicationSectionIdTextBox.TabIndex = 29;
            this.PublicationSectionIdTextBox.Text = "0";
            // 
            // SubmissionTypeDropDownList
            // 
            this.SubmissionTypeDropDownList.FormattingEnabled = true;
            this.SubmissionTypeDropDownList.Location = new System.Drawing.Point(282, 38);
            this.SubmissionTypeDropDownList.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.SubmissionTypeDropDownList.Name = "SubmissionTypeDropDownList";
            this.SubmissionTypeDropDownList.Size = new System.Drawing.Size(316, 34);
            this.SubmissionTypeDropDownList.TabIndex = 26;
            // 
            // MonoCheckBox
            // 
            this.MonoCheckBox.AutoSize = true;
            this.MonoCheckBox.Location = new System.Drawing.Point(282, 529);
            this.MonoCheckBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.MonoCheckBox.Name = "MonoCheckBox";
            this.MonoCheckBox.Size = new System.Drawing.Size(28, 27);
            this.MonoCheckBox.TabIndex = 25;
            this.MonoCheckBox.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(74, 1344);
            this.label18.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(197, 26);
            this.label18.TabIndex = 24;
            this.label18.Text = "DeliveryToFtpPath:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(82, 1188);
            this.label17.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(197, 26);
            this.label17.TabIndex = 23;
            this.label17.Text = "DeliveryToFtpHost:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(34, 1294);
            this.label16.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(248, 26);
            this.label16.TabIndex = 22;
            this.label16.Text = "DeliveryToFtpPassword:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(196, 781);
            this.label14.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(76, 26);
            this.label14.TabIndex = 20;
            this.label14.Text = "Brand:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(152, 623);
            this.label13.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(125, 26);
            this.label13.TabIndex = 19;
            this.label13.Text = "Comments:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(58, 169);
            this.label12.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(216, 26);
            this.label12.TabIndex = 18;
            this.label12.Text = "PublicationSectionId:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(18, 219);
            this.label11.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(259, 26);
            this.label11.TabIndex = 17;
            this.label11.Text = "PublicationSectionSizeId:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(38, 481);
            this.label9.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(244, 26);
            this.label9.TabIndex = 15;
            this.label9.Text = "Height (If Column Size):";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(196, 529);
            this.label8.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 26);
            this.label8.TabIndex = 14;
            this.label8.Text = "Mono:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(126, 571);
            this.label7.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(147, 26);
            this.label7.TabIndex = 13;
            this.label7.Text = "InsertionDate:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(204, 675);
            this.label6.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 26);
            this.label6.TabIndex = 12;
            this.label6.Text = "URN:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(158, 725);
            this.label5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 26);
            this.label5.TabIndex = 11;
            this.label5.Text = "Advertiser:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(112, 1138);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(161, 26);
            this.label4.TabIndex = 10;
            this.label4.Text = "DeliveryToTitle:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(34, 1088);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(246, 26);
            this.label3.TabIndex = 9;
            this.label3.Text = "DeliverToEmailAddress:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(130, 96);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 26);
            this.label1.TabIndex = 7;
            this.label1.Text = "DeliveryRule:";
            // 
            // SubmissionTypeLabel
            // 
            this.SubmissionTypeLabel.AutoSize = true;
            this.SubmissionTypeLabel.Location = new System.Drawing.Point(96, 50);
            this.SubmissionTypeLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.SubmissionTypeLabel.Name = "SubmissionTypeLabel";
            this.SubmissionTypeLabel.Size = new System.Drawing.Size(179, 26);
            this.SubmissionTypeLabel.TabIndex = 6;
            this.SubmissionTypeLabel.Text = "SubmissionType:";
            // 
            // UploadFileButton
            // 
            this.UploadFileButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UploadFileButton.ForeColor = System.Drawing.Color.Blue;
            this.UploadFileButton.Location = new System.Drawing.Point(800, 265);
            this.UploadFileButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.UploadFileButton.Name = "UploadFileButton";
            this.UploadFileButton.Size = new System.Drawing.Size(466, 62);
            this.UploadFileButton.TabIndex = 5;
            this.UploadFileButton.Text = "Upload";
            this.UploadFileButton.UseVisualStyleBackColor = true;
            this.UploadFileButton.Click += new System.EventHandler(this.UploadFileButton_Click);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Blue;
            this.label38.Location = new System.Drawing.Point(614, 1388);
            this.label38.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(42, 53);
            this.label38.TabIndex = 67;
            this.label38.Text = "*";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Blue;
            this.label37.Location = new System.Drawing.Point(614, 1340);
            this.label37.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(42, 53);
            this.label37.TabIndex = 66;
            this.label37.Text = "*";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Blue;
            this.label36.Location = new System.Drawing.Point(614, 1288);
            this.label36.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(42, 53);
            this.label36.TabIndex = 65;
            this.label36.Text = "*";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Blue;
            this.label35.Location = new System.Drawing.Point(614, 1183);
            this.label35.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(42, 53);
            this.label35.TabIndex = 64;
            this.label35.Text = "*";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Blue;
            this.label34.Location = new System.Drawing.Point(614, 1135);
            this.label34.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(42, 53);
            this.label34.TabIndex = 63;
            this.label34.Text = "*";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Blue;
            this.label33.Location = new System.Drawing.Point(614, 1085);
            this.label33.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(42, 53);
            this.label33.TabIndex = 62;
            this.label33.Text = "*";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Blue;
            this.label32.Location = new System.Drawing.Point(614, 823);
            this.label32.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(42, 53);
            this.label32.TabIndex = 61;
            this.label32.Text = "*";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Blue;
            this.label31.Location = new System.Drawing.Point(614, 725);
            this.label31.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(42, 53);
            this.label31.TabIndex = 60;
            this.label31.Text = "*";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Blue;
            this.label30.Location = new System.Drawing.Point(614, 673);
            this.label30.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(42, 53);
            this.label30.TabIndex = 59;
            this.label30.Text = "*";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Blue;
            this.label29.Location = new System.Drawing.Point(614, 623);
            this.label29.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(42, 53);
            this.label29.TabIndex = 58;
            this.label29.Text = "*";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Blue;
            this.label28.Location = new System.Drawing.Point(614, 567);
            this.label28.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(42, 53);
            this.label28.TabIndex = 57;
            this.label28.Text = "*";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Blue;
            this.label27.Location = new System.Drawing.Point(310, 523);
            this.label27.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(42, 53);
            this.label27.TabIndex = 56;
            this.label27.Text = "*";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Blue;
            this.label25.Location = new System.Drawing.Point(614, 481);
            this.label25.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(42, 53);
            this.label25.TabIndex = 54;
            this.label25.Text = "*";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Blue;
            this.label24.Location = new System.Drawing.Point(614, 219);
            this.label24.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(42, 53);
            this.label24.TabIndex = 53;
            this.label24.Text = "*";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Blue;
            this.label23.Location = new System.Drawing.Point(614, 169);
            this.label23.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(42, 53);
            this.label23.TabIndex = 52;
            this.label23.Text = "*";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Blue;
            this.label22.Location = new System.Drawing.Point(614, 96);
            this.label22.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(42, 53);
            this.label22.TabIndex = 51;
            this.label22.Text = "*";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Blue;
            this.label20.Location = new System.Drawing.Point(614, 40);
            this.label20.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(42, 53);
            this.label20.TabIndex = 50;
            this.label20.Text = "*";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.ForeColor = System.Drawing.Color.ForestGreen;
            this.label94.Location = new System.Drawing.Point(1204, 567);
            this.label94.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(42, 53);
            this.label94.TabIndex = 159;
            this.label94.Text = "*";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.ForeColor = System.Drawing.Color.ForestGreen;
            this.label93.Location = new System.Drawing.Point(1204, 527);
            this.label93.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(42, 53);
            this.label93.TabIndex = 158;
            this.label93.Text = "*";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.ForeColor = System.Drawing.Color.ForestGreen;
            this.label92.Location = new System.Drawing.Point(1204, 490);
            this.label92.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(42, 53);
            this.label92.TabIndex = 157;
            this.label92.Text = "*";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.ForeColor = System.Drawing.Color.ForestGreen;
            this.label91.Location = new System.Drawing.Point(1204, 450);
            this.label91.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(42, 53);
            this.label91.TabIndex = 156;
            this.label91.Text = "*";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.ForeColor = System.Drawing.Color.ForestGreen;
            this.label90.Location = new System.Drawing.Point(1204, 413);
            this.label90.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(42, 53);
            this.label90.TabIndex = 155;
            this.label90.Text = "*";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.ForeColor = System.Drawing.Color.ForestGreen;
            this.label88.Location = new System.Drawing.Point(1204, 379);
            this.label88.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(42, 53);
            this.label88.TabIndex = 154;
            this.label88.Text = "*";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.ForeColor = System.Drawing.Color.Magenta;
            this.label78.Location = new System.Drawing.Point(376, 523);
            this.label78.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(42, 53);
            this.label78.TabIndex = 167;
            this.label78.Text = "*";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.ForeColor = System.Drawing.Color.Magenta;
            this.label102.Location = new System.Drawing.Point(1170, 1019);
            this.label102.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(42, 53);
            this.label102.TabIndex = 171;
            this.label102.Text = "*";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(1052, 1019);
            this.label10.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(236, 35);
            this.label10.TabIndex = 166;
            this.label10.Text = "Insertions";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label129.ForeColor = System.Drawing.Color.Magenta;
            this.label129.Location = new System.Drawing.Point(1256, 908);
            this.label129.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(42, 53);
            this.label129.TabIndex = 201;
            this.label129.Text = "*";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label127.ForeColor = System.Drawing.Color.Magenta;
            this.label127.Location = new System.Drawing.Point(1256, 988);
            this.label127.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(42, 53);
            this.label127.TabIndex = 198;
            this.label127.Text = "*";
            this.label127.Visible = false;
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label126.ForeColor = System.Drawing.Color.Magenta;
            this.label126.Location = new System.Drawing.Point(1256, 960);
            this.label126.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(42, 53);
            this.label126.TabIndex = 197;
            this.label126.Text = "*";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label112.ForeColor = System.Drawing.Color.Magenta;
            this.label112.Location = new System.Drawing.Point(1258, 863);
            this.label112.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(42, 53);
            this.label112.TabIndex = 184;
            this.label112.Text = "*";
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label141.ForeColor = System.Drawing.Color.Teal;
            this.label141.Location = new System.Drawing.Point(414, 523);
            this.label141.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(42, 53);
            this.label141.TabIndex = 212;
            this.label141.Text = "*";
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Location = new System.Drawing.Point(12, 1483);
            this.label134.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(272, 26);
            this.label134.TabIndex = 225;
            this.label134.Text = "QmuliStandardWorkflowId:";
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label148.ForeColor = System.Drawing.Color.ForestGreen;
            this.label148.Location = new System.Drawing.Point(1238, 42);
            this.label148.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(42, 53);
            this.label148.TabIndex = 234;
            this.label148.Text = "*";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label113.ForeColor = System.Drawing.Color.Blue;
            this.label113.Location = new System.Drawing.Point(1270, 173);
            this.label113.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(42, 53);
            this.label113.TabIndex = 185;
            this.label113.Text = "*";
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label152.ForeColor = System.Drawing.Color.Blue;
            this.label152.Location = new System.Drawing.Point(1270, 123);
            this.label152.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(42, 53);
            this.label152.TabIndex = 242;
            this.label152.Text = "*";
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label149.ForeColor = System.Drawing.Color.Blue;
            this.label149.Location = new System.Drawing.Point(1270, 83);
            this.label149.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(42, 53);
            this.label149.TabIndex = 237;
            this.label149.Text = "*";
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label150.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label150.Location = new System.Drawing.Point(1300, 42);
            this.label150.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(42, 53);
            this.label150.TabIndex = 247;
            this.label150.Text = "*";
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label147.ForeColor = System.Drawing.Color.Blue;
            this.label147.Location = new System.Drawing.Point(1272, 42);
            this.label147.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(42, 53);
            this.label147.TabIndex = 235;
            this.label147.Text = "*";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label107.ForeColor = System.Drawing.Color.Magenta;
            this.label107.Location = new System.Drawing.Point(1252, 1340);
            this.label107.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(42, 53);
            this.label107.TabIndex = 221;
            this.label107.Text = "*";
            // 
            // label165
            // 
            this.label165.AutoSize = true;
            this.label165.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label165.ForeColor = System.Drawing.Color.Magenta;
            this.label165.Location = new System.Drawing.Point(1252, 1385);
            this.label165.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(42, 53);
            this.label165.TabIndex = 260;
            this.label165.Text = "*";
            // 
            // label167
            // 
            this.label167.Location = new System.Drawing.Point(1070, 1062);
            this.label167.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(112, 35);
            this.label167.TabIndex = 266;
            this.label167.Text = "OR code:";
            this.label167.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label168.ForeColor = System.Drawing.Color.Magenta;
            this.label168.Location = new System.Drawing.Point(1094, 219);
            this.label168.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(42, 53);
            this.label168.TabIndex = 271;
            this.label168.Text = "*";
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label169.ForeColor = System.Drawing.Color.ForestGreen;
            this.label169.Location = new System.Drawing.Point(1064, 219);
            this.label169.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(42, 53);
            this.label169.TabIndex = 270;
            this.label169.Text = "*";
            // 
            // label170
            // 
            this.label170.AutoSize = true;
            this.label170.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label170.ForeColor = System.Drawing.Color.Blue;
            this.label170.Location = new System.Drawing.Point(1036, 219);
            this.label170.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(42, 53);
            this.label170.TabIndex = 269;
            this.label170.Text = "*";
            // 
            // UpdateDeliveryDetailsButton
            // 
            this.UpdateDeliveryDetailsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UpdateDeliveryDetailsButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.UpdateDeliveryDetailsButton.Location = new System.Drawing.Point(516, 40);
            this.UpdateDeliveryDetailsButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.UpdateDeliveryDetailsButton.Name = "UpdateDeliveryDetailsButton";
            this.UpdateDeliveryDetailsButton.Size = new System.Drawing.Size(402, 42);
            this.UpdateDeliveryDetailsButton.TabIndex = 96;
            this.UpdateDeliveryDetailsButton.Text = "Update Delivery Details";
            this.UpdateDeliveryDetailsButton.UseVisualStyleBackColor = true;
            this.UpdateDeliveryDetailsButton.Click += new System.EventHandler(this.UpdateDeliveryDetailsButton_Click);
            // 
            // IdTestBox
            // 
            this.IdTestBox.Location = new System.Drawing.Point(296, 44);
            this.IdTestBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.IdTestBox.Name = "IdTestBox";
            this.IdTestBox.Size = new System.Drawing.Size(204, 31);
            this.IdTestBox.TabIndex = 0;
            // 
            // IdLabel
            // 
            this.IdLabel.AutoSize = true;
            this.IdLabel.Location = new System.Drawing.Point(76, 50);
            this.IdLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.IdLabel.Name = "IdLabel";
            this.IdLabel.Size = new System.Drawing.Size(209, 25);
            this.IdLabel.TabIndex = 1;
            this.IdLabel.Text = "Id (tracking number):";
            // 
            // GetDeliveryStatusButton
            // 
            this.GetDeliveryStatusButton.Location = new System.Drawing.Point(516, 88);
            this.GetDeliveryStatusButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.GetDeliveryStatusButton.Name = "GetDeliveryStatusButton";
            this.GetDeliveryStatusButton.Size = new System.Drawing.Size(402, 44);
            this.GetDeliveryStatusButton.TabIndex = 3;
            this.GetDeliveryStatusButton.Text = "Get Delivery Status";
            this.GetDeliveryStatusButton.UseVisualStyleBackColor = true;
            this.GetDeliveryStatusButton.Click += new System.EventHandler(this.GetDeliveryStatusButton_Click);
            // 
            // GetDeliveryDetailsButton
            // 
            this.GetDeliveryDetailsButton.Location = new System.Drawing.Point(516, 137);
            this.GetDeliveryDetailsButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.GetDeliveryDetailsButton.Name = "GetDeliveryDetailsButton";
            this.GetDeliveryDetailsButton.Size = new System.Drawing.Size(402, 44);
            this.GetDeliveryDetailsButton.TabIndex = 4;
            this.GetDeliveryDetailsButton.Text = "Get Delivery Details";
            this.GetDeliveryDetailsButton.UseVisualStyleBackColor = true;
            this.GetDeliveryDetailsButton.Click += new System.EventHandler(this.GetDeliveryDetailsButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.SetDeliveryReceivedButton);
            this.groupBox1.Controls.Add(this.GetPublisherDownloadList);
            this.groupBox1.Controls.Add(this.PublisherDeliveryDownload);
            this.groupBox1.Controls.Add(this.PageNumber);
            this.groupBox1.Controls.Add(this.label110);
            this.groupBox1.Controls.Add(this.AuthorizedByTextBox);
            this.groupBox1.Controls.Add(this.GetDeliveryListButton);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.UnArchiveDeliveryButton);
            this.groupBox1.Controls.Add(this.ArchiveDeliveryButton);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.GetDeliveryDetailsButton);
            this.groupBox1.Controls.Add(this.IdTestBox);
            this.groupBox1.Controls.Add(this.GetDeliveryStatusButton);
            this.groupBox1.Controls.Add(this.InitiateDeliveryButton);
            this.groupBox1.Controls.Add(this.IdLabel);
            this.groupBox1.Controls.Add(this.UpdateDeliveryDetailsButton);
            this.groupBox1.Controls.Add(this.label75);
            this.groupBox1.Location = new System.Drawing.Point(1394, 25);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox1.Size = new System.Drawing.Size(952, 398);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "API Calls by Adfast Delivery ID (Tracking Number)";
            // 
            // SetDeliveryReceivedButton
            // 
            this.SetDeliveryReceivedButton.Location = new System.Drawing.Point(516, 235);
            this.SetDeliveryReceivedButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.SetDeliveryReceivedButton.Name = "SetDeliveryReceivedButton";
            this.SetDeliveryReceivedButton.Size = new System.Drawing.Size(194, 44);
            this.SetDeliveryReceivedButton.TabIndex = 103;
            this.SetDeliveryReceivedButton.Text = "Set Received";
            this.SetDeliveryReceivedButton.UseVisualStyleBackColor = true;
            this.SetDeliveryReceivedButton.Click += new System.EventHandler(this.SetDeliveryReceivedButton_Click);
            // 
            // GetPublisherDownloadList
            // 
            this.GetPublisherDownloadList.Location = new System.Drawing.Point(516, 346);
            this.GetPublisherDownloadList.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.GetPublisherDownloadList.Name = "GetPublisherDownloadList";
            this.GetPublisherDownloadList.Size = new System.Drawing.Size(402, 44);
            this.GetPublisherDownloadList.TabIndex = 102;
            this.GetPublisherDownloadList.Text = "Get Publisher Downloads List";
            this.GetPublisherDownloadList.UseVisualStyleBackColor = true;
            this.GetPublisherDownloadList.Click += new System.EventHandler(this.GetPublisherDownloadList_Click);
            // 
            // PublisherDeliveryDownload
            // 
            this.PublisherDeliveryDownload.Location = new System.Drawing.Point(32, 346);
            this.PublisherDeliveryDownload.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.PublisherDeliveryDownload.Name = "PublisherDeliveryDownload";
            this.PublisherDeliveryDownload.Size = new System.Drawing.Size(236, 40);
            this.PublisherDeliveryDownload.TabIndex = 101;
            this.PublisherDeliveryDownload.Text = "Publisher Download";
            this.PublisherDeliveryDownload.UseVisualStyleBackColor = true;
            this.PublisherDeliveryDownload.Click += new System.EventHandler(this.PublisherDeliveryDownload_Click);
            // 
            // PageNumber
            // 
            this.PageNumber.Location = new System.Drawing.Point(402, 296);
            this.PageNumber.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.PageNumber.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.PageNumber.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.PageNumber.Name = "PageNumber";
            this.PageNumber.Size = new System.Drawing.Size(102, 31);
            this.PageNumber.TabIndex = 100;
            this.PageNumber.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(330, 300);
            this.label110.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(68, 25);
            this.label110.TabIndex = 99;
            this.label110.Text = "Page:";
            // 
            // AuthorizedByTextBox
            // 
            this.AuthorizedByTextBox.Location = new System.Drawing.Point(170, 194);
            this.AuthorizedByTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.AuthorizedByTextBox.Name = "AuthorizedByTextBox";
            this.AuthorizedByTextBox.Size = new System.Drawing.Size(74, 31);
            this.AuthorizedByTextBox.TabIndex = 11;
            // 
            // GetDeliveryListButton
            // 
            this.GetDeliveryListButton.Location = new System.Drawing.Point(516, 290);
            this.GetDeliveryListButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.GetDeliveryListButton.Name = "GetDeliveryListButton";
            this.GetDeliveryListButton.Size = new System.Drawing.Size(402, 44);
            this.GetDeliveryListButton.TabIndex = 98;
            this.GetDeliveryListButton.Text = "Get Delivery List";
            this.GetDeliveryListButton.UseVisualStyleBackColor = true;
            this.GetDeliveryListButton.Click += new System.EventHandler(this.GetDeliveryListButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 200);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(152, 25);
            this.label2.TabIndex = 17;
            this.label2.Text = "Authorized By:";
            // 
            // UnArchiveDeliveryButton
            // 
            this.UnArchiveDeliveryButton.Location = new System.Drawing.Point(718, 187);
            this.UnArchiveDeliveryButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.UnArchiveDeliveryButton.Name = "UnArchiveDeliveryButton";
            this.UnArchiveDeliveryButton.Size = new System.Drawing.Size(200, 44);
            this.UnArchiveDeliveryButton.TabIndex = 10;
            this.UnArchiveDeliveryButton.Text = "Un-Archive";
            this.UnArchiveDeliveryButton.UseVisualStyleBackColor = true;
            this.UnArchiveDeliveryButton.Click += new System.EventHandler(this.UnArchiveDeliveryButton_Click);
            // 
            // ArchiveDeliveryButton
            // 
            this.ArchiveDeliveryButton.Location = new System.Drawing.Point(516, 187);
            this.ArchiveDeliveryButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.ArchiveDeliveryButton.Name = "ArchiveDeliveryButton";
            this.ArchiveDeliveryButton.Size = new System.Drawing.Size(194, 44);
            this.ArchiveDeliveryButton.TabIndex = 9;
            this.ArchiveDeliveryButton.Text = "Archive";
            this.ArchiveDeliveryButton.UseVisualStyleBackColor = true;
            this.ArchiveDeliveryButton.Click += new System.EventHandler(this.ArchiveDeliveryButton_Click);
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.Color.Red;
            this.button1.Location = new System.Drawing.Point(718, 235);
            this.button1.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(204, 44);
            this.button1.TabIndex = 7;
            this.button1.Text = "Delete Delivery";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // InitiateDeliveryButton
            // 
            this.InitiateDeliveryButton.Location = new System.Drawing.Point(260, 190);
            this.InitiateDeliveryButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.InitiateDeliveryButton.Name = "InitiateDeliveryButton";
            this.InitiateDeliveryButton.Size = new System.Drawing.Size(182, 44);
            this.InitiateDeliveryButton.TabIndex = 6;
            this.InitiateDeliveryButton.Text = "Initiate Delivery";
            this.InitiateDeliveryButton.UseVisualStyleBackColor = true;
            this.InitiateDeliveryButton.Click += new System.EventHandler(this.InitiateDeliveryButton_Click);
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.BackColor = System.Drawing.Color.Transparent;
            this.label75.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.ForeColor = System.Drawing.Color.ForestGreen;
            this.label75.Location = new System.Drawing.Point(436, 196);
            this.label75.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(42, 53);
            this.label75.TabIndex = 97;
            this.label75.Text = "*";
            // 
            // GetApiVersionButton
            // 
            this.GetApiVersionButton.Location = new System.Drawing.Point(32, 37);
            this.GetApiVersionButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.GetApiVersionButton.Name = "GetApiVersionButton";
            this.GetApiVersionButton.Size = new System.Drawing.Size(188, 44);
            this.GetApiVersionButton.TabIndex = 5;
            this.GetApiVersionButton.Text = "Get API Version";
            this.GetApiVersionButton.UseVisualStyleBackColor = true;
            this.GetApiVersionButton.Click += new System.EventHandler(this.GetApiVersionButton_Click);
            // 
            // OutputTextBox
            // 
            this.OutputTextBox.Location = new System.Drawing.Point(12, 31);
            this.OutputTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.OutputTextBox.Multiline = true;
            this.OutputTextBox.Name = "OutputTextBox";
            this.OutputTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.OutputTextBox.Size = new System.Drawing.Size(922, 731);
            this.OutputTextBox.TabIndex = 5;
            this.OutputTextBox.WordWrap = false;
            // 
            // CloseButton
            // 
            this.CloseButton.Location = new System.Drawing.Point(718, 37);
            this.CloseButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(208, 44);
            this.CloseButton.TabIndex = 4;
            this.CloseButton.Text = "Close";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.OutputTextBox);
            this.groupBox2.Location = new System.Drawing.Point(1394, 665);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox2.Size = new System.Drawing.Size(952, 777);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Output";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "PDF Document|*.pdf|Jpeg/jpg Image|*.jpg";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.MailBoxGetDetailsButton);
            this.groupBox4.Controls.Add(this.button6);
            this.groupBox4.Controls.Add(this.label117);
            this.groupBox4.Controls.Add(this.MailBoxUrnTextBox);
            this.groupBox4.Controls.Add(this.label116);
            this.groupBox4.Controls.Add(this.button2);
            this.groupBox4.Controls.Add(this.MailBoxIdTextBox);
            this.groupBox4.Controls.Add(this.label115);
            this.groupBox4.Controls.Add(this.label130);
            this.groupBox4.Controls.Add(this.label131);
            this.groupBox4.Location = new System.Drawing.Point(1394, 435);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox4.Size = new System.Drawing.Size(952, 215);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "API Calls By MailBox Delivery ID or URN";
            // 
            // MailBoxGetDetailsButton
            // 
            this.MailBoxGetDetailsButton.Location = new System.Drawing.Point(512, 87);
            this.MailBoxGetDetailsButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.MailBoxGetDetailsButton.Name = "MailBoxGetDetailsButton";
            this.MailBoxGetDetailsButton.Size = new System.Drawing.Size(406, 44);
            this.MailBoxGetDetailsButton.TabIndex = 188;
            this.MailBoxGetDetailsButton.Text = "Get MailBox Delivery Details";
            this.MailBoxGetDetailsButton.UseVisualStyleBackColor = true;
            this.MailBoxGetDetailsButton.Click += new System.EventHandler(this.MailBoxGetDetailsButton_Click);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.Color.Teal;
            this.button6.Location = new System.Drawing.Point(512, 33);
            this.button6.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(406, 42);
            this.button6.TabIndex = 98;
            this.button6.Text = "Update MailBox Delivery Details";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click_1);
            // 
            // label117
            // 
            this.label117.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label117.Location = new System.Drawing.Point(250, 83);
            this.label117.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(106, 35);
            this.label117.TabIndex = 187;
            this.label117.Text = "- OR -";
            this.label117.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // MailBoxUrnTextBox
            // 
            this.MailBoxUrnTextBox.Location = new System.Drawing.Point(192, 121);
            this.MailBoxUrnTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.MailBoxUrnTextBox.Name = "MailBoxUrnTextBox";
            this.MailBoxUrnTextBox.Size = new System.Drawing.Size(204, 31);
            this.MailBoxUrnTextBox.TabIndex = 100;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(112, 127);
            this.label116.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(63, 25);
            this.label116.TabIndex = 101;
            this.label116.Text = "URN:";
            // 
            // button2
            // 
            this.button2.ForeColor = System.Drawing.Color.Red;
            this.button2.Location = new System.Drawing.Point(512, 142);
            this.button2.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(406, 44);
            this.button2.TabIndex = 98;
            this.button2.Text = "Cancel MailBox Delivery";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // MailBoxIdTextBox
            // 
            this.MailBoxIdTextBox.Location = new System.Drawing.Point(192, 42);
            this.MailBoxIdTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.MailBoxIdTextBox.Name = "MailBoxIdTextBox";
            this.MailBoxIdTextBox.Size = new System.Drawing.Size(204, 31);
            this.MailBoxIdTextBox.TabIndex = 98;
            this.MailBoxIdTextBox.Text = "0";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(62, 50);
            this.label115.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(118, 25);
            this.label115.TabIndex = 99;
            this.label115.Text = "MailBox Id:";
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label130.ForeColor = System.Drawing.Color.Teal;
            this.label130.Location = new System.Drawing.Point(392, 44);
            this.label130.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(42, 53);
            this.label130.TabIndex = 189;
            this.label130.Text = "*";
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label131.ForeColor = System.Drawing.Color.Teal;
            this.label131.Location = new System.Drawing.Point(392, 121);
            this.label131.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(42, 53);
            this.label131.TabIndex = 190;
            this.label131.Text = "*";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.GetApiVersionButton);
            this.groupBox5.Controls.Add(this.CloseButton);
            this.groupBox5.Location = new System.Drawing.Point(1394, 1448);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox5.Size = new System.Drawing.Size(952, 104);
            this.groupBox5.TabIndex = 8;
            this.groupBox5.TabStop = false;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(260, 125);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(182, 39);
            this.button3.TabIndex = 104;
            this.button3.Text = "Set As PADN";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.Button3_Click_1);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2370, 1575);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Adfast Api Demo Application";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PageNumber)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button UploadFileButton;
        private System.Windows.Forms.TextBox IdTestBox;
        private System.Windows.Forms.Label IdLabel;
        private System.Windows.Forms.Button GetDeliveryStatusButton;
        private System.Windows.Forms.Button GetDeliveryDetailsButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox OutputTextBox;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Label SubmissionTypeLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox DeliveryRuleDropDownList;
        private System.Windows.Forms.TextBox DeliveryToFtpPasswordTextBox;
        private System.Windows.Forms.TextBox DeliveryToFtpPathTextBox;
        private System.Windows.Forms.TextBox DeliveryToFtpHostTextBox;
        private System.Windows.Forms.TextBox DeliverToEmailAddressTextBox;
        private System.Windows.Forms.TextBox DeliveryToTitleTextBox;
        private System.Windows.Forms.TextBox BrandTextBox;
        private System.Windows.Forms.TextBox AdvertiserTextBox;
        private System.Windows.Forms.TextBox UrnTextBox;
        private System.Windows.Forms.TextBox CommentsTextBox;
        private System.Windows.Forms.TextBox HeightTextBox;
        private System.Windows.Forms.TextBox PublicationSectionSizeIdTextBox;
        private System.Windows.Forms.TextBox PublicationSectionIdTextBox;
        private System.Windows.Forms.DateTimePicker InsertionDatePicker;
        private System.Windows.Forms.ComboBox SubmissionTypeDropDownList;
        private System.Windows.Forms.CheckBox MonoCheckBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button GetApiVersionButton;
        private System.Windows.Forms.Button InitiateDeliveryButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox FilePathTextBox;
        private System.Windows.Forms.Button SelectFileButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox DeliveryToFtpPortTextBox;
        private System.Windows.Forms.Button ArchiveDeliveryButton;
        private System.Windows.Forms.Button UnArchiveDeliveryButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox AuthorizedByTextBox;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Button CreateSavedDelivery;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.ListBox InsertionDateListBox;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button CreateMailBoxDeliveryButton;
        private System.Windows.Forms.TextBox DeliveryToFTPUsernameTextBox;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.RadioButton DontOptimiseRadioButton;
        private System.Windows.Forms.RadioButton OptimiseRadioButton;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.DateTimePicker MultiInsertionDatePicker;
        private System.Windows.Forms.Button UpdateDeliveryDetailsButton;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox ContactEmailTextBox;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox ContactPhoneTextBox;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox ContactNameTextBox;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox MultipleInsertionsSectionId;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.TextBox ContactCompanyTextBox;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.CheckBox AutoDeliverFixedFile;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.CheckBox AutoDeliverCheckBox;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.CheckBox AutoFontFix;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.CheckBox AutoDeliverOptimisedFile;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.CheckBox DeliverWithoutChecks;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.CheckBox IgnoreSizeCheck;
        private System.Windows.Forms.Label IgnoreSizeCheckx;
        private System.Windows.Forms.CheckBox IgnoreErrorsAndDeliverOriginalFile;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.CheckBox AutoUpsampleFix;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.TextBox AccountNumberTextBox;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.TextBox MailBoxUrnTextBox;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox MailBoxIdTextBox;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Button MailBoxGetDetailsButton;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.CheckBox SendAutoCreatedAdfastDeliveryReminderCheckBox;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.CheckBox AutoCreateAdfastDeliveryCheckBox;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.DateTimePicker SendDatePicker;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.DateTimePicker DeadlineDatePicker;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.CheckBox CropFileCheckBox;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.CheckBox MailBoxUrgentDeadlineCheckBox;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.NumericUpDown PageNumber;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Button GetDeliveryListButton;
        private System.Windows.Forms.Button PublisherDeliveryDownload;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button GetPublisherDownloadList;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.TextBox QmuliStandardWorkflowIdTextBox;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.CheckBox SplitDPSFileCheckBox;
        private System.Windows.Forms.TextBox ExternalDeliveryRefTextBox;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.TextBox SplitDpsDeliveryRightHandPageUrnTextBox;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.TextBox SplitDpsDeliveryLeftHandPageUrnTextBox;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.TextBox WidthTextBox;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.Label label160;
        private System.Windows.Forms.Label label161;
        private System.Windows.Forms.Label label162;
        private System.Windows.Forms.Label label159;
        private System.Windows.Forms.Label label158;
        private System.Windows.Forms.TextBox PublicationSectionCodeTextBox;
        private System.Windows.Forms.CheckBox MailBoxDoNotSendEmailCheckBox;
        private System.Windows.Forms.Label label164;
        private System.Windows.Forms.Label label165;
        private System.Windows.Forms.Label label166;
        private System.Windows.Forms.TextBox PublisherCodeTextBox;
        private System.Windows.Forms.Label label163;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox InsertionPublicationSectionCodeTextBox;
        private System.Windows.Forms.Label label167;
        private System.Windows.Forms.Button SetDeliveryReceivedButton;
        private System.Windows.Forms.CheckBox IsPadnCheckbox;
        private System.Windows.Forms.Label label168;
        private System.Windows.Forms.Label label169;
        private System.Windows.Forms.Label label170;
        private System.Windows.Forms.Button button3;
    }
}

