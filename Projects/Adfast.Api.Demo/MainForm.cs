﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Windows.Forms;
using Adfast.Api.Dto;
using Adfast.Api.Dto.V1;

#pragma warning disable IDE1006 // Naming Styles

namespace Adfast.Api.Demo
{
    public partial class MainForm : Form
    {
        private List<MailBoxInsertion> _insertions = new List<MailBoxInsertion>();

        public MainForm()
        {
            InitializeComponent();
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private async void GetDeliveryStatusButton_Click(object sender, EventArgs e)
        {
            ClearOutput();

            if (Int32.TryParse(IdTestBox.Text, out int id))
            {
                try
                {
                    AddOutput("Calling GetDeliveryStatus...");

                    var status = await Client.V1.GetDeliveryStatus(id);

                    AddOutput("");
                    AddOutput($"DeliveryStatusDescription: {status.DeliveryStatusDescription}");
                    AddOutput($"FileToDeliverDescription: {status.FileToDeliverDescription}");
                    AddOutput($"DeliveryInitiated: {status.DeliveryInitiated}");
                    AddOutput($"DeliveryInitiatedDate: {status.DeliveryInitiatedDate}");
                    AddOutput($"DeliverySent: {status.DeliverySent}");
                    AddOutput($"DeliverySentDate: {status.DeliverySentDate}");
                    AddOutput($"DeliveryReceived: {status.DeliveryReceived}");
                    AddOutput($"DeliveryReceivedDate: {status.DeliveryReceivedDate}");
                    AddOutput($"AutoDelivery: {status.AutoDelivery}");
                    AddOutput($"AutoDeliveryError: {status.AutoDeliveryError}");
                    AddOutput($"HasFile: {status.HasFile}");
                    AddOutput($"AutoDeliveryErrorMessage: {status.AutoDeliveryErrorMessage}");
                    AddOutput($"AuthorizedBy: {status.AuthorizedBy}");
                    AddOutput($"AuthorizedDate {status.AuthorizedDate}");
                    AddOutput($"UploadProcessed: {status.UploadProcessed}");
                    AddOutput($"Upload Status: {status.UploadStatus}");
                    AddOutput($"UploadStatusDescription: {status.UploadStatusDescription}");
                    AddOutput($"UploadFlightCheckReportUrl: {status.UploadFlightCheckReportUrl}");
                    AddOutput($"FixProcessed: {status.FixProcessed}");
                    AddOutput($"FixStatus: {status.FixStatus}");
                    AddOutput($"FixStatusDescription: {status.FixStatusDescription}");
                    AddOutput($"FixFlightCheckReportUrl: {status.FixFlightCheckReportUrl}");
                    AddOutput($"ColorCorrectionProcessed: {status.ColorCorrectionProcessed}");
                    AddOutput($"ColorCorrectionStatus: {status.ColorCorrectionStatus}");
                    AddOutput($"ColorCorrectionStatusDescription: {status.ColorCorrectionStatusDescription}");
                    AddOutput($"UploadedPdfUrl: {status.UploadedPdfUrl}");
                    AddOutput($"UploadFlightCheckReportUrl: {status.UploadFlightCheckReportUrl}");
                    AddOutput($"UploadConvertedSoftProofUrl: {status.UploadConvertedSoftProofUrl}");
                    AddOutput($"FixFlightCheckReportUrl: {status.FixFlightCheckReportUrl}");
                    AddOutput($"FixConvertedSoftProofUrl: {status.FixConvertedSoftProofUrl}");
                    AddOutput($"FixPdfUrl: {status.FixPdfUrl}");
                    AddOutput($"ColorCorrectedFlightCheckReportUrl: {status.ColorCorrectedFlightCheckReportUrl}");
                    AddOutput($"ColorCorrectedSoftProofUrl: {status.ColorCorrectedSoftProofUrl}");
                    AddOutput($"ColorCorrectedPdfUrl: {status.ColorCorrectedPdfUrl}");
                    AddOutput($"DeliveredPdfUrl: {status.DeliveredPdfUrl}");
                    AddOutput($"CanDeliverOriginalFile: {status.CanDeliverOriginalFile}");
                    AddOutput($"CanDeliverFixedFile: {status.CanDeliverFixedFile}");
                    AddOutput($"CanDeliverOptimisedFile: {status.CanDeliverOptimisedFile}");
                }
                catch (Exception exception)
                {
                    OutputTextBox.Text = exception.Message;
                }
            }
            else
            {
                MessageBox.Show(this, "Please enter a valid Id");
            }
        }

        private void ClearOutput()
        {
            OutputTextBox.Clear();
        }

        private void AddOutput(object value)
        {
            OutputTextBox.Text = OutputTextBox.Text + Environment.NewLine + value;
        }

        private async void GetDeliveryDetailsButton_Click(object sender, EventArgs e)
        {
            ClearOutput();

            if (Int32.TryParse(IdTestBox.Text, out int id))
            {
                try
                {
                    AddOutput("Calling GetDeliveryDetails...");

                    var details = await Client.V1.GetDeliveryDetails(id);

                    AddOutput("Delivery / Job Info");
                    AddOutput("");
                    AddOutput($"Id: {details.Id}");
                    AddOutput($"PublicationSectionId: {details.PublicationSectionId}");
                    AddOutput($"Filename: {details.Filename}");
                    AddOutput($"Advertiser: {details.Advertiser}");
                    AddOutput($"Brand: {details.Brand}");
                    AddOutput($"Publication: {details.Publication}");
                    AddOutput($"Width: {details.Width}");
                    AddOutput($"Height: {details.Height}");
                    AddOutput($"InsertionDate: {details.InsertionDate}");
                    AddOutput($"Urn: {details.Urn}");
                    AddOutput($"MailBoxDelivery: {details.MailBoxDelivery}");
                    AddOutput($"ThumbnailUrl: {details.ThumbnailUrl}");
                    AddOutput($"Mono : {details.Mono}");
                    AddOutput($"UploadedDate: {details.UploadedDate}");
                    AddOutput($"FileSource: {details.FileSource}");
                    AddOutput($"FileSourceDescription: {details.FileSourceDescription}");
                    AddOutput($"ContactName: {details.ContactName}");
                    AddOutput($"ContactPhone: {details.ContactPhone}");
                    AddOutput($"ContactCompany: {details.ContactCompany}");
                    AddOutput($"ContactEmail: {details.ContactEmail}");
                    AddOutput($"Comments: {details.Comments}");
                    AddOutput($"DeliveryCosts: {details.DeliveryCosts}");
                    AddOutput($"CreatedDate: {details.CreatedDate}");
                    AddOutput($"LastUpdatedDate: {details.LastUpdatedDate}");
                    AddOutput($"StatusResults: {details.StatusResults}");
                    AddOutput($"DPS Split (not deliverable, has LHP/RHP Ids): {details.IsSplitDpsDelivery}");
                    AddOutput($"DpsDeliveryLeftHandPageDeliveryId: {details.DpsDeliveryLeftHandPageDeliveryId}");
                    AddOutput($"DpsDeliveryRightHandPageDeliveryId: {details.DpsDeliveryRightHandPageDeliveryId}");
                    AddOutput($"ExternalDeliveryReference: {details.ExternalDeliveryReference}");
                    AddOutput("");
                    AddOutput("Delivery / Processing Status & File paths");
                    AddOutput("");
                    AddOutput($"DeliveryStatusDescription: {details.StatusResults.DeliveryStatusDescription}");
                    AddOutput($"FileToDeliverDescription: {details.StatusResults.FileToDeliverDescription}");
                    AddOutput($"DeliveryInitiated: {details.StatusResults.DeliveryInitiated}");
                    AddOutput($"DeliveryInitiatedDate: {details.StatusResults.DeliveryInitiatedDate}");
                    AddOutput($"DeliverySent: {details.StatusResults.DeliverySent}");
                    AddOutput($"DeliverySentDate: {details.StatusResults.DeliverySentDate}");
                    AddOutput($"DeliveryReceived: {details.StatusResults.DeliveryReceived}");
                    AddOutput($"DeliveryReceivedDate: {details.StatusResults.DeliveryReceivedDate}");
                    AddOutput($"AutoDelivery: {details.StatusResults.AutoDelivery}");
                    AddOutput($"AutoDeliveryError: {details.StatusResults.AutoDeliveryError}");
                    AddOutput($"HasFile: {details.StatusResults.HasFile}");
                    AddOutput($"AutoDeliveryErrorMessage: {details.StatusResults.AutoDeliveryErrorMessage}");
                    AddOutput($"AuthorizedBy: {details.StatusResults.AuthorizedBy}");
                    AddOutput($"AuthorizedDate {details.StatusResults.AuthorizedDate}");
                    AddOutput($"UploadProcessed: {details.StatusResults.UploadProcessed}");
                    AddOutput($"Upload Status: {details.StatusResults.UploadStatus}");
                    AddOutput($"UploadStatusDescription: {details.StatusResults.UploadStatusDescription}");
                    AddOutput($"UploadFlightCheckReportUrl: {details.StatusResults.UploadFlightCheckReportUrl}");
                    AddOutput($"FixProcessed: {details.StatusResults.FixProcessed}");
                    AddOutput($"FixStatus: {details.StatusResults.FixStatus}");
                    AddOutput($"FixStatusDescription: {details.StatusResults.FixStatusDescription}");
                    AddOutput($"FixFlightCheckReportUrl: {details.StatusResults.FixFlightCheckReportUrl}");
                    AddOutput($"ColorCorrectionProcessed: {details.StatusResults.ColorCorrectionProcessed}");
                    AddOutput($"ColorCorrectionStatus: {details.StatusResults.ColorCorrectionStatus}");
                    AddOutput($"ColorCorrectionStatusDescription: {details.StatusResults.ColorCorrectionStatusDescription}");
                    AddOutput($"UploadedPdfUrl: {details.StatusResults.UploadedPdfUrl}");
                    AddOutput($"UploadFlightCheckReportUrl: {details.StatusResults.UploadFlightCheckReportUrl}");
                    AddOutput($"UploadConvertedSoftProofUrl: {details.StatusResults.UploadConvertedSoftProofUrl}");
                    AddOutput($"FixFlightCheckReportUrl: {details.StatusResults.FixFlightCheckReportUrl}");
                    AddOutput($"FixConvertedSoftProofUrl: {details.StatusResults.FixConvertedSoftProofUrl}");
                    AddOutput($"FixPdfUrl: {details.StatusResults.FixPdfUrl}");
                    AddOutput($"ColorCorrectedFlightCheckReportUrl: {details.StatusResults.ColorCorrectedFlightCheckReportUrl}");
                    AddOutput($"ColorCorrectedSoftProofUrl: {details.StatusResults.ColorCorrectedSoftProofUrl}");
                    AddOutput($"ColorCorrectedPdfUrl: {details.StatusResults.ColorCorrectedPdfUrl}");
                    AddOutput($"DeliveredPdfUrl: {details.StatusResults.DeliveredPdfUrl}");
                    AddOutput($"CanDeliverOriginalFile: {details.StatusResults.CanDeliverOriginalFile}");
                    AddOutput($"CanDeliverFixedFile: {details.StatusResults.CanDeliverFixedFile}");
                    AddOutput($"CanDeliverOptimisedFile: {details.StatusResults.CanDeliverOptimisedFile}");

                }
                catch (Exception exception)
                {
                    OutputTextBox.Text = exception.Message;
                }
            }
            else
            {
                MessageBox.Show(this, "Please enter a valid Id");
            }
        }

        private async void GetApiVersionButton_Click(object sender, EventArgs e)
        {
            ClearOutput();

            try
            {
                AddOutput("Calling GetApiVersion...");

                var version = await Client.V1.GetApiVersion();

                AddOutput("");
                AddOutput($"API Version: {version}");
            }
            catch (Exception exception)
            {
                OutputTextBox.Text = exception.Message;
            }
        }

        private async void InitiateDeliveryButton_Click(object sender, EventArgs e)
        {
            ClearOutput();

            if (Int32.TryParse(IdTestBox.Text, out int id))
            {
                try
                {
                    AddOutput("Calling Initiate Delivery...");

                    var result = await Client.V1.InitiateDelivery(id, AuthorizedByTextBox.Text);

                    AddOutput("");

                    AddOutput(result ? "Delivery Initiated" : "Delivery Not Initiated");
                }
                catch (Exception exception)
                {
                    OutputTextBox.Text = exception.Message;
                }
            }
            else
            {
                MessageBox.Show(this, "Please enter a valid Id");
            }
        }


        private async void button1_Click(object sender, EventArgs e)

        {
            ClearOutput();

            if (Int32.TryParse(IdTestBox.Text, out int id))
            {
                try
                {
                    AddOutput("Calling Delete Delivery...");

                    var result = await Client.V1.DeleteDelivery(id);

                    AddOutput("");

                    AddOutput(result ? "Delivery Deleted" : "Delivery Not Deleted");
                }
                catch (Exception exception)
                {
                    OutputTextBox.Text = exception.Message;
                }
            }
            else
            {
                MessageBox.Show(this, "Please enter a valid Id");
            }
        }

        private void SelectFileButton_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                FilePathTextBox.Text = openFileDialog1.FileName;
            }
        }

        private async void UploadFileButton_Click(object sender, EventArgs e)
        {
            ClearOutput();

            try
            {
                AddOutput("Parsing Upload Values ...");
                AddOutput("");

                Enum.TryParse(SubmissionTypeDropDownList.SelectedValue.ToString(), out UploadSubmissionType submissionType);

                Enum.TryParse(DeliveryRuleDropDownList.SelectedValue.ToString(), out DeliveryRule deliveryRule);

                var fileUploadParameters = new FileUploadParameters
                {
                    SubmissionType = submissionType,
                    DeliveryRule = deliveryRule,
                    PublicationSectionId = Int32.Parse(PublicationSectionIdTextBox.Text),
                    PublicationSectionSizeId = Int32.Parse(PublicationSectionSizeIdTextBox.Text),
                    PublisherCode = PublisherCodeTextBox.Text,
                    PublicationSectionCode = PublicationSectionCodeTextBox.Text,
                    Width = Decimal.Parse(WidthTextBox.Text),
                    Height = Decimal.Parse(HeightTextBox.Text),
                    Mono = MonoCheckBox.Checked,
                    InsertionDate = InsertionDatePicker.Value,
                    Comments = CommentsTextBox.Text,
                    Urn = UrnTextBox.Text,
                    Advertiser = AdvertiserTextBox.Text,
                    Brand = BrandTextBox.Text,
                    DeliveryToTitle = DeliveryToTitleTextBox.Text,
                    DeliveryToEmailAddress = DeliverToEmailAddressTextBox.Text,
                    DeliveryToFtpHost = DeliveryToFtpHostTextBox.Text,
                    DeliveryToFtpPath = DeliveryToFtpPathTextBox.Text,
                    DeliveryToFtpPort = Int32.Parse(DeliveryToFtpPortTextBox.Text),
                    DeliveryToFtpPassword = DeliveryToFtpPasswordTextBox.Text,
                    Optimize = OptimiseRadioButton.Checked,
                    ContactCompany = ContactCompanyTextBox.Text,
                    ContactName = ContactNameTextBox.Text,
                    ContactPhone = ContactPhoneTextBox.Text,
                    ContactEmail = ContactEmailTextBox.Text,
                    AutoFontFix = AutoFontFix.Checked,
                    AutoUpsampleFix = AutoUpsampleFix.Checked,
                    IgnoreSizeCheck = IgnoreSizeCheck.Checked,
                    CropFile = CropFileCheckBox.Checked,
                    QmuliStandardWorkflowId = Int32.Parse(QmuliStandardWorkflowIdTextBox.Text),
                    SplitDpsDelivery = SplitDPSFileCheckBox.Checked,
                    SplitDpsDeliveryLeftHandPageUrn = SplitDpsDeliveryLeftHandPageUrnTextBox.Text,
                    SplitDpsDeliveryRightHandPageUrn = SplitDpsDeliveryRightHandPageUrnTextBox.Text,
                    ExternalDeliveryReference = ExternalDeliveryRefTextBox.Text,
                    IsPadn = IsPadnCheckbox.Checked
                };

                AddOutput("Calling UploadFile ...");

                var result = await Client.V1.UploadFileAsync(System.IO.Path.GetFileName(FilePathTextBox.Text), System.IO.File.ReadAllBytes(FilePathTextBox.Text), fileUploadParameters);

                AddOutput("");

                AddOutput(result > 0 ? $"Delivery Uploaded ID = ({result})" : "Upload Failed");

                if (result > 0)
                {
                    IdTestBox.Text = result.ToString();
                }
            }
            catch (Exception exception)
            {
                AddOutput("");
                AddOutput("Error:");
                OutputTextBox.Text = exception.Message;
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            SubmissionTypeDropDownList.DataSource = Enum.GetValues(typeof(UploadSubmissionType));
            DeliveryRuleDropDownList.DataSource = Enum.GetValues(typeof(DeliveryRule));
            SubmissionTypeDropDownList.SelectedIndex = 0;
            DeliveryRuleDropDownList.SelectedIndex = 0;
        }

        private async void ArchiveDeliveryButton_Click(object sender, EventArgs e)
        {
            ClearOutput();

            if (Int32.TryParse(IdTestBox.Text, out int id))
            {
                try
                {
                    AddOutput("Calling ArchiveDelivery...");

                    var result = await Client.V1.ArchiveDelivery(id);

                    AddOutput("");

                    AddOutput(result ? "Delivery Archived" : "Delivery Not Archived");
                }
                catch (Exception exception)
                {
                    OutputTextBox.Text = exception.Message;
                }
            }
            else
            {
                MessageBox.Show(this, "Please enter a valid Id");
            }
        }

        private async void UnArchiveDeliveryButton_Click(object sender, EventArgs e)
        {
            ClearOutput();

            if (Int32.TryParse(IdTestBox.Text, out int id))
            {
                try
                {
                    AddOutput("Calling UnArchiveDelivery...");

                    var result = await Client.V1.UnArchiveDelivery(id);

                    AddOutput("");

                    AddOutput(result ? "Delivery Un-Archived" : "Delivery Not Un-Archived");
                }
                catch (Exception exception)
                {
                    OutputTextBox.Text = exception.Message;
                }
            }
            else
            {
                MessageBox.Show(this, "Please enter a valid Id");
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            _insertions = new List<MailBoxInsertion>();
            InsertionDateListBox.Items.Clear();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            _insertions.Add(new MailBoxInsertion(Int32.Parse(MultipleInsertionsSectionId.Text), MultiInsertionDatePicker.Value, DeadlineDatePicker.Value, InsertionPublicationSectionCodeTextBox.Text));

            InsertionDateListBox.Items.Clear();
            foreach (var insertion in _insertions)
            {
                InsertionDateListBox.Items.Add($"{(insertion.PublicationSectionId > 0 ? insertion.PublicationSectionId.ToString() : insertion.PublicationSectionCode)}, {insertion.InsertionDate.Date:d}, {insertion.DeadlineDate.Date:d}");
            }
        }

        private async void button3_Click(object sender, EventArgs e)
        {
            ClearOutput();

            try
            {
                var deliveryCreateParameters = new MailBoxDeliveryCreateParameters
                {
                    PublicationSectionSizeId = Int32.Parse(PublicationSectionSizeIdTextBox.Text),
                    PublicationSectionCode = PublicationSectionCodeTextBox.Text,
                    Width = Decimal.Parse(WidthTextBox.Text),
                    Urn = UrnTextBox.Text,
                    AccountNumber = AccountNumberTextBox.Text,
                    Advertiser = AdvertiserTextBox.Text,
                    Brand = BrandTextBox.Text,
                    Email = DeliverToEmailAddressTextBox.Text,
                    Height = Decimal.Parse(HeightTextBox.Text),
                    IsMono = MonoCheckBox.Checked,
                    Comments = CommentsTextBox.Text,
                    IgnoreSizeCheck = IgnoreSizeCheck.Checked,
                    SendDate = SendDatePicker.Value,
                    DeadlineDate = DeadlineDatePicker.Value,
                    AutoCreateAdfastDelivery = AutoCreateAdfastDeliveryCheckBox.Checked,
                    SendAutoCreatedAdfastDeliveryReminder = SendAutoCreatedAdfastDeliveryReminderCheckBox.Checked,
                    UrgentDeadline = MailBoxUrgentDeadlineCheckBox.Checked,
                    EmailSentExternally = MailBoxDoNotSendEmailCheckBox.Checked,
                    IsPadn = IsPadnCheckbox.Checked
                };

                deliveryCreateParameters.Insertions.AddRange(_insertions);

                AddOutput("Calling CreateMailBoxDelivery...");

                var result = await Client.V1.CreateMailBoxDelivery(deliveryCreateParameters);

                AddOutput("");

                AddOutput(result > 0 ? $"MailBox Delivery Created ID = ({result})" : "MailBoxDelivery Not Created");

                if (result > 0)
                {
                    MailBoxIdTextBox.Text = result.ToString();
                }
            }
            catch (Exception exception)
            {
                OutputTextBox.Text = exception.Message;
            }
        }

        private async void UpdateDeliveryDetailsButton_Click(object sender, EventArgs e)
        {
           
            ClearOutput();

            if (Int32.TryParse(IdTestBox.Text, out int id))
            {
                try
                {
                    Enum.TryParse(DeliveryRuleDropDownList.SelectedValue.ToString(), out DeliveryRule deliveryRule);

                    var deliveryDetails = new DeliveryUpdateParameters
                    {
                        DeliveryRule = deliveryRule,
                        Id = id,
                        InsertionDate = InsertionDatePicker.Value,
                        Comments = CommentsTextBox.Text,
                        Urn = UrnTextBox.Text,
                        Advertiser = AdvertiserTextBox.Text,
                        Brand = BrandTextBox.Text,
                        ContactCompany = ContactCompanyTextBox.Text,
                        ContactName = ContactNameTextBox.Text,
                        ContactPhone = ContactPhoneTextBox.Text,
                        ContactEmail = ContactEmailTextBox.Text,
                        DeliveryToTitle = DeliveryToTitleTextBox.Text,
                        DeliveryToEmailAddress = DeliverToEmailAddressTextBox.Text,
                        DeliveryToFtpHost = DeliveryToFtpPathTextBox.Text,
                        DeliveryToFtpPath = DeliveryToFtpPathTextBox.Text,
                        DeliveryToFtpPort = Int32.Parse(DeliveryToFtpPortTextBox.Text),
                        DeliveryToFtpUsername = DeliveryToFTPUsernameTextBox.Text,
                        DeliveryToFtpPassword = DeliveryToFtpPasswordTextBox.Text,
                        QmuliStandardWorkflowId = Int32.Parse(QmuliStandardWorkflowIdTextBox.Text),
                        ExternalDeliveryReference = ExternalDeliveryRefTextBox.Text
                    };

                    AddOutput("Calling UpdateDelivery...");

                    var result = await Client.V1.UpdateDelivery(deliveryDetails);

                    AddOutput("");

                    AddOutput(result ? "Delivery Updated" : "Delivery Not Updated");
                }
                catch (Exception exception)
                {
                    OutputTextBox.Text = exception.Message;
                }
            }
            else
            {
                MessageBox.Show(this, "Please enter a valid Id");
            }

        }

        private async void CreateSavedDelivery_Click(object sender, EventArgs e)
        {
            ClearOutput();

            try
            {
                var deliveryCreateParameters = new DeliveryCreateParameters
                {
                    PublicationSectionId = Int32.Parse(PublicationSectionIdTextBox.Text),
                    PublicationSectionSizeId = Int32.Parse(PublicationSectionSizeIdTextBox.Text),
                    PublicationSectionCode = PublicationSectionCodeTextBox.Text,
                    Width = Decimal.Parse(WidthTextBox.Text),
                    Height = Decimal.Parse(HeightTextBox.Text),
                    InsertionDate = InsertionDatePicker.Value,
                    Comments = CommentsTextBox.Text,
                    Urn = UrnTextBox.Text,
                    Advertiser = AdvertiserTextBox.Text,
                    Brand = BrandTextBox.Text,
                    DeliveryAuthorizedBy = AuthorizedByTextBox.Text,
                    Mono = MonoCheckBox.Checked,
                    Optimize = OptimiseRadioButton.Checked,
                    AutoDeliver = AutoDeliverCheckBox.Checked,
                    AutoDeliverFixedFile = AutoDeliverFixedFile.Checked,
                    AutoDeliverOptimisedFile = AutoDeliverOptimisedFile.Checked,
                    AutoFontFix = AutoFontFix.Checked,
                    AutoUpsampleFix = AutoUpsampleFix.Checked,
                    IgnoreErrorsAndDeliverOriginalFile = IgnoreErrorsAndDeliverOriginalFile.Checked,
                    IgnoreSizeCheck = IgnoreSizeCheck.Checked,
                    DeliverWithoutChecks = DeliverWithoutChecks.Checked,
                    QmuliStandardWorkflowId = Int32.Parse(QmuliStandardWorkflowIdTextBox.Text),
                    IsPadn = IsPadnCheckbox.Checked
                };

                AddOutput("Calling CreateDelivery...");

                var result = await Client.V1.CreateDelivery(deliveryCreateParameters);

                AddOutput("");

                AddOutput(result ? "Delivery Created" : "Delivery Not Created");
            }
            catch (Exception exception)
            {
                OutputTextBox.Text = exception.Message;
            }
        }

        private async void button2_Click(object sender, EventArgs e)
        {
            ClearOutput();

            if (!Int32.TryParse(MailBoxIdTextBox.Text, out int id))
            {
                id = 0;
            }

            if (id <= 0 && String.IsNullOrWhiteSpace(MailBoxUrnTextBox.Text))
            {
                MessageBox.Show(this, "A MailBox Id or URN to update, must be specified");
                return;
            }

            var urn = MailBoxUrnTextBox.Text;

            try
            {
                AddOutput(id > 0 ? $"Calling CancelMailBoxDeliveryById ({id})" : $"Calling CancelMailBoxDeliveryByUrn ({urn})");

                var result = id > 0 ? await Client.V1.CancelMailBoxDeliveryById(id) : await Client.V1.CancelMailBoxDeliveryByUrn(urn);

                AddOutput("");

                AddOutput(result ? "Delivery Cancelled" : "Delivery Not Cancelled");
            }
            catch (Exception exception)
            {
                OutputTextBox.Text = exception.Message;
            }
        }

        private async void button6_Click_1(object sender, EventArgs e)
        {
            ClearOutput();

            if (!Int32.TryParse(MailBoxIdTextBox.Text, out int id))
            {
                id = 0;
            }

            if (id <= 0 && String.IsNullOrWhiteSpace(MailBoxUrnTextBox.Text))
            {
                MessageBox.Show(this, "A MailBox Id or URN to update, must be specified");
                return;
            }

            var urn = MailBoxUrnTextBox.Text;

            try
            {
                Enum.TryParse(DeliveryRuleDropDownList.SelectedValue.ToString(), out DeliveryRule deliveryRule);

                var deliveryDetails = new MailBoxDeliveryUpdateParameters
                {
                    PublicationSectionSizeId = Int32.Parse(PublicationSectionSizeIdTextBox.Text),
                    Urn = UrnTextBox.Text,
                    Advertiser = AdvertiserTextBox.Text,
                    Brand = BrandTextBox.Text,
                    Email = DeliverToEmailAddressTextBox.Text,
                    Height = Decimal.Parse(HeightTextBox.Text),
                    IsMono = MonoCheckBox.Checked,
                    Comments = CommentsTextBox.Text,
                    IgnoreSizeCheck = IgnoreSizeCheck.Checked,
                    SendDate = SendDatePicker.Value,
                    DeadlineDate = DeadlineDatePicker.Value,
                    AutoCreateAdfastDelivery = AutoCreateAdfastDeliveryCheckBox.Checked,
                    SendAutoCreatedAdfastDeliveryReminder = SendAutoCreatedAdfastDeliveryReminderCheckBox.Checked,
                    UrgentDeadline = MailBoxUrgentDeadlineCheckBox.Checked
                };

                if (id > 0)
                {
                    deliveryDetails.Id = id;
                }
                else
                {
                    deliveryDetails.UpdateByUrn = urn;
                }

                AddOutput(id > 0 ? $"Calling UpdateMailBoxDelivery (id = {id})" : $"Calling UpdateMailBoxDelivery (UpdateByUrn = {urn})");

                var result = await Client.V1.UpdateMailBoxDelivery(deliveryDetails);

                AddOutput("");

                AddOutput(result ? "Delivery Updated" : "Delivery Not Updated");
            }
            catch (Exception exception)
            {
                OutputTextBox.Text = exception.Message;
            }
        }

        private async void MailBoxGetDetailsButton_Click(object sender, EventArgs e)
        {
            ClearOutput();

            if (!Int32.TryParse(MailBoxIdTextBox.Text, out int id))
            {
                id = 0;
            }

            if (id <= 0 && String.IsNullOrWhiteSpace(MailBoxUrnTextBox.Text))
            {
                MessageBox.Show(this, "A MailBox Id or URN to update, must be specified");
                return;
            }

            var urn = MailBoxUrnTextBox.Text;

            try
            {
                AddOutput(id > 0 ? $"Calling GetMailBoxDeliveryDetailsById ({id})" : $"Calling GetMailBoxDeliveryDetailsByUrn ({urn})");

                var details = id > 0 ? await Client.V1.GetMailBoxDeliveryDetailsById(id) : await Client.V1.GetMailBoxDeliveryDetailsByUrn(urn);

                AddOutput("MailBox Delivery Data");
                AddOutput("");
                AddOutput($"Id: {details.Id}");
                AddOutput($"Guid: {details.Guid}");
                AddOutput($"PublicationSectionSizeId: {details.PublicationSectionSizeId}");
                AddOutput($"Urn: {details.Urn}");
                AddOutput($"AccountNumber: {details.AccountNumber}");
                AddOutput($"Advertiser: {details.Advertiser}");
                AddOutput($"Brand: {details.Brand}");
                AddOutput($"Email: {details.Email}");
                AddOutput($"Height: {details.Height}");
                AddOutput($"Mono : {details.IsMono}");
                AddOutput($"Comments: {details.Comments}");
                AddOutput($"IgnoreSizeCheck : {details.IgnoreSizeCheck}");
                AddOutput($"SendDate: {details.SendDate}");
                AddOutput($"AutoCreateAdfastDelivery: {details.AutoCreateAdfastDelivery}");
                AddOutput($"SendAutoCreatedAdfastDeliveryReminder: {details.SendAutoCreatedAdfastDeliveryReminder}");
                AddOutput($"Sent: {details.Sent}");
                AddOutput($"Cancelled: {details.Cancelled}");
                AddOutput($"SendDate: {details.SendDate}");
                AddOutput($"CreatedDate: {details.CreatedDate}");
                AddOutput($"LastUpdatedDate: {details.LastUpdatedDate}");
                AddOutput($"EmailLinkCallBackStatus: {details.EmailLinkCallBackStatus}");
                AddOutput($"Urgent Deadline: {details.UrgentDeadline}");
                AddOutput($"Email Sent Externally: {details.EmailSentExternally}");
                AddOutput($"MailBox Upload Page Url: {details.UploadPageUrl}");
                AddOutput($"AdfastDeliveryId: {details.AdfastDeliveryId}");
                AddOutput($"UploadDate: {details.UploadDate}");
                AddOutput($"CallbackProcessed: {details.CallbackProcessed}");
                AddOutput($"CallbackFailed: {details.CallbackFailed}");
                AddOutput($"CallbackRetryCount: {details.CallbackRetryCount}");
                AddOutput($"CallbackError: {details.CallbackError}");
                AddOutput("");
                AddOutput("Insertions");
                AddOutput("");
                foreach (var insertion in details.Insertions)
                {
                    AddOutput($"{insertion.PublicationSectionId} - {insertion.InsertionDate}");
                }
            }
            catch (Exception exception)
            {
                OutputTextBox.Text = exception.Message;
            }
        }

        private async void GetDeliveryListButton_Click(object sender, EventArgs e)
        {
            ClearOutput();

            try
            {
                AddOutput($"Calling GetDeliveryList Page: {PageNumber.Value}");

                var deliveryListParameters = new DeliveryListParameters
                {
                    Page = (int)PageNumber.Value,
                    PageSize = 20,
                    OrderBy = "LastUpdatedDate",
                    OrderDirection = "desc"
                };

                var deliveries = await Client.V1.GetDeliveryList(deliveryListParameters);

                AddOutput("");
                AddOutput($"Page {deliveryListParameters.Page} showing {deliveryListParameters.PageSize} of {deliveries.TotalCount} total items");

                foreach (var details in deliveries.List)
                {
                    AddOutput("Delivery / Job Info");
                    AddOutput("");
                    AddOutput($"Id: {details.Id}");
                    AddOutput($"PublicationSectionId: {details.PublicationSectionId}");
                    AddOutput($"Filename: {details.Filename}");
                    AddOutput($"Advertiser: {details.Advertiser}");
                    AddOutput($"Brand: {details.Brand}");
                    AddOutput($"Publication: {details.Publication}");
                    AddOutput($"Width: {details.Width}");
                    AddOutput($"Height: {details.Height}");
                    AddOutput($"InsertionDate: {details.InsertionDate}");
                    AddOutput($"Urn: {details.Urn}");
                    AddOutput($"MailBoxDelivery: {details.MailBoxDelivery}");
                    AddOutput($"ThumbnailUrl: {details.ThumbnailUrl}");
                    AddOutput($"Mono : {details.Mono}");
                    AddOutput($"UploadedDate: {details.UploadedDate}");
                    AddOutput($"FileSource: {details.FileSource}");
                    AddOutput($"FileSourceDescription: {details.FileSourceDescription}");
                    AddOutput($"ContactName: {details.ContactName}");
                    AddOutput($"ContactPhone: {details.ContactPhone}");
                    AddOutput($"ContactCompany: {details.ContactCompany}");
                    AddOutput($"ContactEmail: {details.ContactEmail}");
                    AddOutput($"Comments: {details.Comments}");
                    AddOutput($"DeliveryCosts: {details.DeliveryCosts}");
                    AddOutput($"CreatedDate: {details.CreatedDate}");
                    AddOutput($"LastUpdatedDate: {details.LastUpdatedDate}");
                    AddOutput($"StatusResults: {details.StatusResults}");
                    AddOutput("");
                    AddOutput("Delivery / Processing Status & File paths");
                    AddOutput("");
                    AddOutput($"DeliveryStatusDescription: {details.StatusResults.DeliveryStatusDescription}");
                    AddOutput($"FileToDeliverDescription: {details.StatusResults.FileToDeliverDescription}");
                    AddOutput($"DeliveryInitiated: {details.StatusResults.DeliveryInitiated}");
                    AddOutput($"DeliveryInitiatedDate: {details.StatusResults.DeliveryInitiatedDate}");
                    AddOutput($"DeliverySent: {details.StatusResults.DeliverySent}");
                    AddOutput($"DeliverySentDate: {details.StatusResults.DeliverySentDate}");
                    AddOutput($"DeliveryReceived: {details.StatusResults.DeliveryReceived}");
                    AddOutput($"DeliveryReceivedDate: {details.StatusResults.DeliveryReceivedDate}");
                    AddOutput($"AutoDelivery: {details.StatusResults.AutoDelivery}");
                    AddOutput($"AutoDeliveryError: {details.StatusResults.AutoDeliveryError}");
                    AddOutput($"HasFile: {details.StatusResults.HasFile}");
                    AddOutput($"AutoDeliveryErrorMessage: {details.StatusResults.AutoDeliveryErrorMessage}");
                    AddOutput($"AuthorizedBy: {details.StatusResults.AuthorizedBy}");
                    AddOutput($"AuthorizedDate {details.StatusResults.AuthorizedDate}");
                    AddOutput($"UploadProcessed: {details.StatusResults.UploadProcessed}");
                    AddOutput($"Upload Status: {details.StatusResults.UploadStatus}");
                    AddOutput($"UploadStatusDescription: {details.StatusResults.UploadStatusDescription}");
                    AddOutput($"UploadFlightCheckReportUrl: {details.StatusResults.UploadFlightCheckReportUrl}");
                    AddOutput($"FixProcessed: {details.StatusResults.FixProcessed}");
                    AddOutput($"FixStatus: {details.StatusResults.FixStatus}");
                    AddOutput($"FixStatusDescription: {details.StatusResults.FixStatusDescription}");
                    AddOutput($"FixFlightCheckReportUrl: {details.StatusResults.FixFlightCheckReportUrl}");
                    AddOutput($"ColorCorrectionProcessed: {details.StatusResults.ColorCorrectionProcessed}");
                    AddOutput($"ColorCorrectionStatus: {details.StatusResults.ColorCorrectionStatus}");
                    AddOutput($"ColorCorrectionStatusDescription: {details.StatusResults.ColorCorrectionStatusDescription}");
                    AddOutput($"UploadedPdfUrl: {details.StatusResults.UploadedPdfUrl}");
                    AddOutput($"UploadFlightCheckReportUrl: {details.StatusResults.UploadFlightCheckReportUrl}");
                    AddOutput($"UploadConvertedSoftProofUrl: {details.StatusResults.UploadConvertedSoftProofUrl}");
                    AddOutput($"FixFlightCheckReportUrl: {details.StatusResults.FixFlightCheckReportUrl}");
                    AddOutput($"FixConvertedSoftProofUrl: {details.StatusResults.FixConvertedSoftProofUrl}");
                    AddOutput($"FixPdfUrl: {details.StatusResults.FixPdfUrl}");
                    AddOutput($"ColorCorrectedFlightCheckReportUrl: {details.StatusResults.ColorCorrectedFlightCheckReportUrl}");
                    AddOutput($"ColorCorrectedSoftProofUrl: {details.StatusResults.ColorCorrectedSoftProofUrl}");
                    AddOutput($"ColorCorrectedPdfUrl: {details.StatusResults.ColorCorrectedPdfUrl}");
                    AddOutput($"DeliveredPdfUrl: {details.StatusResults.DeliveredPdfUrl}");
                    AddOutput($"CanDeliverOriginalFile: {details.StatusResults.CanDeliverOriginalFile}");
                    AddOutput($"CanDeliverFixedFile: {details.StatusResults.CanDeliverFixedFile}");
                    AddOutput($"CanDeliverOptimisedFile: {details.StatusResults.CanDeliverOptimisedFile}");
                    AddOutput("");
                    AddOutput("-Next Item----------------------");
                    AddOutput("");
                }
            }
            catch (Exception exception)
            {
                OutputTextBox.Text = exception.Message;
            }
        }

        private async void PublisherDeliveryDownload_Click(object sender, EventArgs e)
        {
            ClearOutput();

            if (Int32.TryParse(IdTestBox.Text, out int id))
            {
                try
                {
                    AddOutput("Calling PublisherDeliveryDownload...");

                    var downloadResult = await Client.V1.PublisherDeliveryDownload(id);

                    var filePath = downloadResult.Filename;

                    saveFileDialog1.FileName = filePath;
                    saveFileDialog1.CheckPathExists = true;
                    saveFileDialog1.OverwritePrompt = true;

                    if (saveFileDialog1.ShowDialog(this) == DialogResult.OK)
                    {
                        filePath = saveFileDialog1.FileName;

                        File.WriteAllBytes(filePath, downloadResult.FileContent);

                        System.Diagnostics.Process.Start(filePath);
                    }

                    if (File.Exists(filePath))
                    {
                        AddOutput($"File Downloaded to {filePath}");
                    }
                    else
                    {
                        AddOutput("File not saved");
                    }
                }
                catch (Exception exception)
                {
                    OutputTextBox.Text = exception.Message;
                }
            }
            else
            {
                MessageBox.Show(this, "Please enter a valid Id");
            }
        }

        private async void GetPublisherDownloadList_Click(object sender, EventArgs e)
        {
            ClearOutput();

            try
            {
                AddOutput($"Calling GetPublisherDeliveriesForDownload...");

                var deliveries = await Client.V1.GetPublisherDeliveriesForDownload();

                AddOutput("");

                AddOutput($"Id's (Tracking Numbers) - {deliveries.Count} items");

                foreach (var id in deliveries)
                {
                    AddOutput($"Id: {id}");
                }
            }
            catch (Exception exception)
            {
                OutputTextBox.Text = exception.Message;
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private async void SetDeliveryReceivedButton_Click(object sender, EventArgs e)
        {
            ClearOutput();

            if (Int32.TryParse(IdTestBox.Text, out int id))
            {
                try
                {
                    AddOutput("Calling SetPublisherFileReceivedStatus...");

                    var result = await Client.V1.SetPublisherFileReceivedStatus(id);

                    AddOutput("");

                    AddOutput(result == HttpStatusCode.OK ? "HttpStatusCode = OK (status updated)" : "HttpStatusCode = NotModified (status already set as received)");
                }
                catch (Exception exception)
                {
                    OutputTextBox.Text = exception.Message;
                }
            }
            else
            {
                MessageBox.Show(this, "Please enter a valid Id");
            }
        }

        private async void Button3_Click_1(object sender, EventArgs e)
        {
            ClearOutput();

            if (Int32.TryParse(IdTestBox.Text, out int id))
            {
                try
                {
                    AddOutput("Calling SetDeliveryAsPadn...");

                    await Client.V1.SetDeliveryAsPadn(id);

                    AddOutput("");

                    AddOutput("SetDeliveryAsPadn called successfully.");
                }
                catch (Exception exception)
                {
                    OutputTextBox.Text = exception.Message;
                }
            }
            else
            {
                MessageBox.Show(this, "Please enter a valid Id");
            }
        }
    }
}
