﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Adfast.Api.Demo")]
[assembly: AssemblyDescription("Adfast API Demo App for Testing")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Qmuli Limited")]
[assembly: AssemblyProduct("Adfast.Api.Demo")]
[assembly: AssemblyCopyright("Copyright © 2025")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("3ebcae6f-4b6b-4ed0-b754-3521e26d39e3")]
[assembly: AssemblyVersion("3.2.0")]
[assembly: AssemblyFileVersion("3.2.0")]
[assembly: AssemblyInformationalVersion("3.2.0")]
