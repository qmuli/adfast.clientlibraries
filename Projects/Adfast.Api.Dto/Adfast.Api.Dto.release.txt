--------------
VERSION 3.2.0
--------------

UPDATED: .Net Framework to 4.8
UPDATED: Referenced Nuget Packages to the latest versions

--------------
VERSION 3.1.0
--------------

ADDED: Timeout Settings for Http Client

--------------
VERSION 3.0.3
--------------

UPDATED: Updated version to match other related libraries.

--------------
VERSION 3.0.2
--------------

UPDATED: Updated version to match other related libraries.

--------------
VERSION 3.0.1
--------------

ADDED: AdSync Detetion API Endpoint DTO parameters

--------------
VERSION 3.0.0
--------------

ADDED: AdSync API Endpoints

--------------
VERSION 2.14.0
--------------

UPDATED: Updated version to match other related libraries.

--------------
VERSION 2.13.0
--------------

ADDED: PadnAdData Properties: CreatedDateAsDateTime, CreatedDateAsString

--------------
VERSION 2.12.0
--------------

ADDED: PadnSetPadnStatusParameters DTO object

--------------
VERSION 2.11.0
--------------

ADDED: Additional properties added to DeliveryCreateParameters; PadnId, PadnCreatedDate
ADDED: Additional parameters/overload on SetDeliveryAsPadn; padnId, padnCreatedDate

--------------
VERSION 2.10.0
--------------

ADDED: Additional property returned for GetDeliveryDetails: MetadataSource

-------------
VERSION 2.9.0
-------------

ADDED: Additional properties returned for GetDeliveryDetails; PublicationName, PublicationSectionName, Campaign, PartVersion, TargetedAudience

-------------
VERSION 2.8.0
-------------

UPDATED: Updated version to match other related libraries.

-------------
VERSION 2.7.0
-------------

ADDED: PressCentreId and PressCentreName to delivery details

-------------
VERSION 2.6.0
-------------

UPDATED: Updated version to match other related libraries.

-------------
VERSION 2.5.0
-------------

ADDED: Delivery CI Url to delivery details
ADDED: Delivery FileSizeOnDisk Url to delivery details

-------------
VERSION 2.4.0
-------------

UPDATED: Updated version to match other related libraries.

-------------
VERSION 2.3.0
-------------

UPDATED: Version to match other libraries

-------------
VERSION 2.2.0
-------------

ADDED: SetDeliveryAsPadn API call
ADDED: IsPadn property to Delivery data

-------------
VERSION 2.1.6
-------------

ADDED: CroppedFileUrl and UncroppedFileUrl to status results for File Cropping feature

-------------
VERSION 2.1.5
-------------

ADDED: GetPadnAdsByDetectionFilters and related DTO parameters

-------------
VERSION 2.1.4
-------------

FIXED: ListResult.

-------------
VERSION 2.1.3
-------------

UPDATED: PadnAdData DTO, added IsOption property.

-------------
VERSION 2.1.2
-------------

UPDATED: PadnAdData DTO.

-------------
VERSION 2.1.1
-------------

ADDED: ListResult Constructors

-------------
VERSION 2.1.0
-------------

ADDED: Parameters and Result DTO Objects for PADN Related API calls

--------------
VERSION 2.0.11
--------------

ADDED: IsPadn property to FileUploadParameters, DeliveryCreateParameters and MailBoxDeliveryCreateParameters 

--------------
VERSION 2.0.10
--------------

ADDED: Updated to match related libraries

-------------
VERSION 2.0.9
-------------

UPDATED: MailBox delivery details; AdfastDeliveryId and callback info about Adfast delivery creation after mailbox upload.

-------------
VERSION 2.0.8
-------------

UPDATED: Mapped publisher codes for MailBox workflow

-------------
VERSION 2.0.7
-------------

FIXED: Support for mapped publisher codes

-------------
VERSION 2.0.6
-------------

UPDATED: Support for mapped publisher codes
UPDATED: Mailbox worflow to prevent email and return upload page url for external emails

-------------
VERSION 2.0.5
-------------

UPDATED: Serialization Attributes.

-------------
VERSION 2.0.4
-------------

UPDATED: To suport Adfast Client and Dto changes, see Adfast.Api.Client Release notes.