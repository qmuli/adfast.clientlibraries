﻿namespace Adfast.Api.Dto
{
    public enum AdSyncCopyChasingStatus
    {
        /// <summary>
        /// Unknown
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Pending - AdSync record created from feed, without email.
        /// </summary>
        Pending = 1,

        /// <summary>
        /// Predicted - AdSync record created from feed where email has been saved (matched or by user).
        /// </summary>
        Predicted = 2,

        /// <summary>
        /// Confirmed - Confirmed by user or API
        /// </summary>
        Confirmed = 3,

        /// <summary>
        /// Rejected - Rejected by user or API (Denied status via PADN)
        /// </summary>
        Rejected = 4,

        /// <summary>
        /// Confirmed Ads (PADN Related)
        /// </summary>
        ConfirmedExternal = 5,

        /// <summary>
        /// Cancelled
        /// </summary>
        Cancelled = 6,
    }
}
