﻿using System;
using System.Runtime.Serialization;

namespace Adfast.Api.Dto
{
    [Serializable, DataContract]
    public class DeliveryCosts
    {
        [DataMember]
        public bool PublisherMandatoryCostsApplied { get; set; }

        [DataMember]
        public decimal CombinedProcessingCost { get; set; }

        [DataMember]
        public decimal CsvImportCost { get; set; }

        [DataMember]
        public decimal FixCost { get; set; }

        [DataMember]
        public decimal ColorCorrectionCost { get; set; }

        [DataMember]
        public decimal DeliveryCost { get; set; }

        [DataMember]
        public decimal TotalCost { get; set; }
    }
}
