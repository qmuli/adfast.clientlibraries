﻿using System;

namespace Adfast.Api.Dto
{
    [Serializable]
    public enum UploadSubmissionType
    {
        UploadWithChecks = 0,
        UploadWithChecksAndDeliver = 1,
        UploadWithoutChecksAndDeliver = 2
    }
}
