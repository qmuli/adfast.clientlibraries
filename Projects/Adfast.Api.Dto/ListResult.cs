﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Adfast.Api.Dto
{
    [Serializable, DataContract]
    public class ListResult<T>
    {
        [DataMember]
        public int TotalCount { get; set; }

        [DataMember]
        public int? Page { get; set; }

        [DataMember]
        public int? PageSize { get; set; }

        [DataMember]
        public IEnumerable<T> List  { get; set; }

        public ListResult()
        {
            TotalCount = 0;
        }
        public ListResult(int totalCount, IEnumerable<T> list)
        {
            TotalCount = totalCount;
            List = list;
        }

        public ListResult(int totalCount, int page, int pageSize, IEnumerable<T> list)
        {
            TotalCount = totalCount;
            List = list;
        }
    }
}