﻿
namespace Adfast.Api.Dto
{
    public class DownloadResult
    {
        public byte[] FileContent { get; set; }

        public string Filename { get; set; }
    }
}