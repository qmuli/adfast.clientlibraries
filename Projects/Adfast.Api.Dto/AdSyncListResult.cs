﻿using Adfast.Api.Dto.V1;
using System.Collections.Generic;

namespace Adfast.Api.Dto
{
    public class AdSyncListResult
    {
        public AdSyncListResult()
        {
            List = new List<AdSyncAd>();
            TotalCount = 0;
            Page = 0;
            PageSize = 0;
        }

        public List<AdSyncAd> List { get; set; }
        public int TotalCount { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
    }
}