﻿using System;

namespace Adfast.Api.Dto.V1
{
    public class AdSyncAd
    {
        public int Id { get; set; } = 0;

        public AdNetwork AdNetFeedType { get; set; } = AdNetwork.AdSync;

        public string SenderStatus { get; set; } = "";

        public AdSyncCopyChasingStatus AdSenderStatus { get; set; } = AdSyncCopyChasingStatus.Unknown;

        public string Urn { get; set; } = "";

        public int AdvertiserId { get; set; } = 0;

        public string AdvertiserName { get; set; } = "";

        public string Publisher { get; set; } = "";

        public string PublisherName { get; set; } = "";

        public string Publication { get; set; } = "";

        public string PublicationName { get; set; } = "";

        public string Section { get; set; } = "";

        public string SectionName { get; set; } = "";

        public string SectionType { get; set; } = "";

        public DateTime? EditionAsDateTime { get; set; } = null;

        public string EditionAsString { get; set; } = "";

        public string Deadline { get; set; } = "";

        public DateTime? DeadlineDateTime { get; set; } = null;

        public string DeadlineAsString { get; set; } = "";

        /// <summary>
        /// Height of Ad (mapped to Publication Size or As Supplied for column sizes) [AdNet/PADN]
        /// </summary>
        public decimal Depth { get; set; } = 0;

        public decimal Width { get; set; } = 0;

        public string Sender { get; set; } = "";

        public string ConfirmedStatus { get; set; } = "";

        public string Description { get; set; } = "";

        public string Brand { get; set; } = "";

        public string DescriptionBrand { get; set; } = "";

        public int DeliveryId { get; set; } = 0;

        public Guid DeliveryGuid { get; set; } = Guid.Empty;

        public bool DeliveryInitiated { get; set; } = false;

        public bool DeliverySent { get; set; } = false;

        public bool DeliveryReceived { get; set; } = false;

        public bool DeliveryHasFile { get; set; } = false;

        public int MailBoxDeliveryId { get; set; } = 0;

        public Guid MailBoxDeliveryGuid { get; set; }

        public string MailBoxDeliveryStatus { get; set; } = "";

        public string MailBoxDeliverySendDate { get; set; } = "";

        public int? AdfastPublicationId { get; set; } = 0;

        public string AdfastPublicationName { get; set; } = "";

        public object AdfastPublicationSize { get; set; } = null;

        public int AdfastPublicationSizeId { get; set; } = 0;

        public string AdfastPublicationSizeName { get; set; } = "";

        public bool IsOption { get; set; } = false;

        public int? PublisherId { get; set; } = 0;

        public int? Columns { get; set; } = 0;

        public int? PadnAdvertiserId { get; set; } = 0;

        public string PadnAdvertiserName { get; set; }

        public bool IsMono { get; set; } = false;

        public string ProductionStatus { get; set; } = "";

        public PadnAdProductionStatus AdProductionStatus { get; set; } = PadnAdProductionStatus.Unknown;

        public string TPRef { get; set; } = "";

        public DateTime? CreatedDateTime { get; set; } = null;

        public string CreatedDateAsString { get; set; } = "";

        public string SplitCode { get; set; } = "";

        public bool IsSplit { get; set; } = false;

        public bool IsRepeat { get; set; } = false;

        public string Zone { get; set; } = "";

        public string NamedSize { get; set; } = "";

        public string Headline { get; set; } = "";

        public string BrandCode { get; set; } = "";
    }
}
