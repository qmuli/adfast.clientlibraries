﻿
using System;
using System.Runtime.Serialization;

namespace Adfast.Api.Dto.V1
{
    [Serializable, DataContract]
    public class PadnAdRejectParameters
    {
        /// <summary>
        /// Publisher Code, available from the GetPadnAdList API call
        /// </summary>
        [DataMember]
        public string PublisherCode { get; set; }

        /// <summary>
        /// PADN Ad Booking Id, available from the GetPadnAdList API call
        /// </summary>
        [DataMember]
        public int AdId { get; set; }

        /// <summary>
        /// Sender Email Address Override, API User account details are used by default
        /// </summary>
        [DataMember]
        public string SenderEmailAddress { get; set; }
        
        /// <summary>
        /// Sender Name Override, API User account details are used by default
        /// </summary>
        [DataMember]
        public string SenderName { get; set; }

        /// <summary>
        /// Sender Phone Override, API User account details are used by default
        /// </summary>
        [DataMember]
        public string SenderPhone { get; set; }

        /// <summary>
        /// Sender Company Name Override, API User account details are used by default
        /// </summary>
        [DataMember]
        public string SenderCompanyName { get; set; }

        /// <summary>
        /// Note
        /// </summary>
        [DataMember]
        public string Note { get; set; }
    }
}