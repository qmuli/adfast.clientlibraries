﻿using System;
using System.Runtime.Serialization;

namespace Adfast.Api.Dto.V1
{
    [Serializable, DataContract]
    public class MailBoxInsertion
    {
        public MailBoxInsertion()
        {
        }

        public MailBoxInsertion(int publicationSectionId, DateTime insertionDate, DateTime deadlineDate, string publicationSectionCode = "")
        {
            Id = 0;
            PublicationSectionId = publicationSectionId;
            InsertionDate = insertionDate;
            DeadlineDate = deadlineDate;
            PublicationSectionCode = publicationSectionCode;
        }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int PublicationSectionId { get; set; }

        /// <summary>
        /// External Publishers Publication code mapped in Adlib, Used instead of PublicationSectionId, set PublicationSectionId = 0
        /// </summary>
        [DataMember]
        public string PublicationSectionCode { get; set; }

        [DataMember]
        public DateTime InsertionDate { get; set; }

        [DataMember]
        public DateTime DeadlineDate { get; set; }
    }
}
