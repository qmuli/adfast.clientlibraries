using System;
using System.Runtime.Serialization;

namespace Adfast.Api.Dto.V1
{
    [Serializable, DataContract]
    public class PadnAdDetectionParameters
    {
        /// <summary>
        /// Sender Email Address Override (if API override is enabled) Otherwise API User Account Master email address is used.
        /// </summary>
        [DataMember]
        public string SenderEmailAddress { get; set; }

        /// <summary>
        /// PublicationSectionId
        /// </summary>
        [DataMember]
        public int? PublicationSectionId { get; set; }

        /// <summary>
        /// URN
        /// </summary>
        [DataMember]
        public string Urn { get; set; }
       
        /// <summary>
        /// InsertionDate
        /// </summary>
        [DataMember]
        public DateTime? InsertionDate { get; set; }

        /// <summary>
        /// Width
        /// </summary>
        [DataMember]
        public double? Width { get; set; }

        /// <summary>
        /// Height
        /// </summary>
        [DataMember]
        public double? Height { get; set; }
    }
}