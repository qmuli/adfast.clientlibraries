﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Adfast.Api.Dto.V1
{
    [Serializable, DataContract]
    public class MailBoxDeliveryCreateParameters
    {
        public MailBoxDeliveryCreateParameters()
        {
            Height = 0;
            IsMono = false;
            IgnoreSizeCheck = false;
            AutoCreateAdfastDelivery = false;
            SendAutoCreatedAdfastDeliveryReminder = false;
            Insertions = new List<MailBoxInsertion>();
            UrgentDeadline = false;
            EmailSentExternally = false;
            IsPadn = false;
        }

        [DataMember]
        public int PublicationSectionSizeId { get; set; }

        /// <summary>
        /// Publication Section Code (External Publisher Mapping code for Section, used insead of PublicationSectionId)
        /// </summary>
        [DataMember]
        public string PublicationSectionCode { get; set; }

        [DataMember]
        public string Urn { get; set; }

        [DataMember]
        public string AccountNumber { get; set; }

        [DataMember]
        public string Advertiser { get; set; }

        [DataMember]
        public string Brand { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public Decimal Height { get; set; }

        /// <summary>
        /// Width (used with height and PublicationSectionCode to get Publication Section & Size Id, if Section & Size Id not supplied)
        /// </summary>
        [DataMember]
        public Decimal Width { get; set; }

        [DataMember]
        public bool IsMono { get; set; }

        [DataMember]
        public string Comments { get; set; }

        [DataMember]
        public bool IgnoreSizeCheck { get; set; }

        [DataMember]
        public DateTime SendDate { get; set; }

        [DataMember]
        public DateTime DeadlineDate { get; set; }

        /// <summary>
        /// Set to create delivery record without a file automatically instead of sending an email with the link to upload
        /// </summary>
        [DataMember]
        public bool AutoCreateAdfastDelivery { get; set; }

        [DataMember]
        public bool SendAutoCreatedAdfastDeliveryReminder { get; set; }

        [DataMember]
        public bool UrgentDeadline { get; set; }

        /// <summary>
        /// Email muts not be sent, emails must be sent externally
        /// </summary>
        [DataMember]
        public bool EmailSentExternally { get; set; }

        /// <summary>
        /// One or more insertions (PublicationSection Id and Insertion Date)
        /// </summary>
        [DataMember]
        public List<MailBoxInsertion> Insertions { get; set; }

        /// <summary>
        /// Indicates this job has been created via a Papermule PADN workflow
        /// </summary>
        [DataMember]
        public bool IsPadn { get; set; }
    }
}
