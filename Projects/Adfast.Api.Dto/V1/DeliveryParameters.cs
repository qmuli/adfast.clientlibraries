﻿using System;
using System.Runtime.Serialization;

namespace Adfast.Api.Dto.V1
{
    /// <summary>
    /// File Upload Parameters
    /// </summary>
    [Serializable, DataContract]
    public class DeliveryParameters
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string AuthorizedBy { get; set; }
    }
}