﻿using System;
using System.Runtime.Serialization;

namespace Adfast.Api.Dto.V1
{
    [Serializable]
    [DataContract]
    public class SetDeliveryAsAdSyncParameters
    {
        [DataMember]
        public AdNetwork AdNetwork { get; set; }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string BookingId { get; set; }

        [DataMember]
        public string AdSyncCreatedDate { get; set; }
    }
}
