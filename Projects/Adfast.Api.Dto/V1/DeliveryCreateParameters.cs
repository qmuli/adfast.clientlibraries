﻿using System;
using System.Runtime.Serialization;

namespace Adfast.Api.Dto.V1
{
    /// <summary>
    /// Delivery Update Parameters
    /// </summary>
    [Serializable, DataContract]
    public class DeliveryCreateParameters
    {
        public DeliveryCreateParameters()
        {
            PublicationSectionId = 0;
            PublicationSectionSizeId = 0;
            Height = 0;
            Mono = false;
            InsertionDate = DateTime.MinValue;
            Optimize = false;
            AutoDeliver = false;
            AutoDeliverFixedFile = false;
            AutoDeliverOptimisedFile = false;
            AutoFontFix = false;
            AutoUpsampleFix = false;
            IgnoreErrorsAndDeliverOriginalFile = false;
            DeliverWithoutChecks = false;
            IsPadn = false;
            PadnId = "";
            PadnCreatedDate = null;
        }

        /// <summary>
        /// Publication Section Id (From Adlib API)
        /// </summary>
        [DataMember]
        public int PublicationSectionId { get; set; }

        /// <summary>
        /// Publication Section Size Id (From Adlib API) Required
        /// </summary>
        [DataMember]
        public int PublicationSectionSizeId { get; set; }

        /// <summary>
        /// Publication Section Code (External Publisher Mapping code for Section, used insead of PublicationSectionId)
        /// </summary>
        [DataMember]
        public string PublicationSectionCode { get; set; }

        /// <summary>
        /// Height if the size is a column size
        /// </summary>
        [DataMember]
        public decimal Height { get; set; }

        /// <summary>
        /// Width (used with height and PublicationSectionCode to get Publication Section & Size Id, if Section & Size Id not supplied)
        /// </summary>
        [DataMember]
        public decimal Width { get; set; }

        [DataMember]
        public DateTime InsertionDate { get; set; }

        [DataMember]
        public string Advertiser { get; set; }

        [DataMember]
        public string Brand { get; set; }

        /// <summary>
        /// Unique Referenece Number for Publisher - Optional
        /// </summary>
        [DataMember]
        public string Urn { get; set; }

        [DataMember]
        public string Comments { get; set; }

        [DataMember]
        public bool Mono { get; set; }

        [DataMember]
        public bool CropFile { get; set; }

        [DataMember]
        public bool Optimize { get; set; }

        [DataMember]
        public bool AutoDeliver { get; set; }

        [DataMember]
        public bool AutoDeliverFixedFile { get; set; }

        [DataMember]
        public bool AutoDeliverOptimisedFile { get; set; }

        [DataMember]
        public bool AutoFontFix { get; set; }

        [DataMember]
        public bool AutoUpsampleFix { get; set; }

        [DataMember]
        public bool IgnoreErrorsAndDeliverOriginalFile { get; set; }

        [DataMember]
        public bool IgnoreSizeCheck { get; set; }

        [DataMember]
        public bool DeliverWithoutChecks { get; set; }

        public string DeliveryAuthorizedBy { get; set; }

        /// <summary>
        /// This is an override to process the file to a specific standard (See Adlib API for list of Qmuli Standards)
        /// </summary>
        [DataMember]
        public int QmuliStandardWorkflowId { get; set; }

        /// <summary>
        /// This is to reference records that errored via the Createdeliveries API call, this will required by CreateDeliveries and is not saved to Adfast
        /// </summary>
        [DataMember]
        public string ImportReference { get; set; }

        /// <summary>
        /// Indicates this job has been created via a Papermule PADN workflow
        /// </summary>
        [DataMember]
        public bool IsPadn { get; set; }

        /// <summary>
        /// PADN ID, from Papermule/PADN API, set to delivery if job is a PADN job (IsPadn = true)
        /// </summary>
        [DataMember]
        public string PadnId { get; set; }

        /// <summary>
        /// PADN Created Date, from Papermule/PADN API, set to delivery if job is a PADN job (IsPadn = true)
        /// </summary>
        [DataMember]
        public DateTime? PadnCreatedDate { get; set; }
    }
}