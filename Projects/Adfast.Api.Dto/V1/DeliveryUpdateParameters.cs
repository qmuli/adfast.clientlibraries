﻿using System;
using System.Runtime.Serialization;

namespace Adfast.Api.Dto.V1
{
    /// <summary>
    /// Delivery Update Parameters
    /// </summary>
    [Serializable, DataContract]
    public class DeliveryUpdateParameters
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public DeliveryRule DeliveryRule { get; set; }

        [DataMember]
        public DateTime InsertionDate { get; set; }

        [DataMember]
        public string Comments { get; set; }

        /// <summary>
        /// Unique Referenece Number for Publisher - Optional
        /// </summary>
        [DataMember]
        public string Urn { get; set; }

        [DataMember]
        public string Advertiser { get; set; }

        [DataMember]
        public string Brand { get; set; }

        [DataMember]
        public string ContactName { get; set; }

        [DataMember]
        public string ContactCompany { get; set; }

        [DataMember]
        public string ContactPhone { get; set; }

        [DataMember]
        public string ContactEmail { get; set; }

        /// <summary>
        /// DeliverTo Title CustomEmail or CustomFtp DeliveryRule
        /// </summary>
        [DataMember]
        public string DeliveryToTitle { get; set; }

        /// <summary>
        /// Delivery Email Address for CustomEmail DeliveryRule
        /// </summary>
        [DataMember]
        public string DeliveryToEmailAddress { get; set; }

        /// <summary>
        /// DeliverTo FtpHost for CustomFtp DeliveryRule
        /// </summary>
        [DataMember]
        public string DeliveryToFtpHost { get; set; }

        /// <summary>
        /// DeliverTo Ftp Path/Directory for CustomFtp DeliveryRule
        /// </summary>
        [DataMember]
        public string DeliveryToFtpPath{ get; set; }

        /// <summary>
        /// DeliverTo Ftp Port for CustomFtp DeliveryRule
        /// </summary>
        [DataMember]
        public int DeliveryToFtpPort { get; set; }

        /// <summary>
        /// DeliverTo Username for CustomFtp DeliveryRule
        /// </summary>
        [DataMember]
        public string DeliveryToFtpUsername { get; set; }

        /// <summary>
        /// DeliverTo Password for CustomFtp DeliveryRule
        /// </summary>
        [DataMember]
        public string DeliveryToFtpPassword { get; set; }

        [DataMember]
        public bool CropFile { get; set; }

        /// <summary>
        /// This is an override to process the file to a specific standard (See Adlib API for list of Qmuli Standards)
        /// </summary>
        [DataMember]
        public int QmuliStandardWorkflowId { get; set; }

        [DataMember]
        public string ExternalDeliveryReference { get; set; }
    }
}