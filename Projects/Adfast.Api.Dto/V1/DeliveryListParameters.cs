using System;
using System.Runtime.Serialization;

namespace Adfast.Api.Dto.V1
{
    [Serializable, DataContract]
    public class DeliveryListParameters
    {
        [DataMember]
        public int Page { get; set; }

        [DataMember]
        public int PageSize { get; set; }

        [DataMember]
        public string OrderBy { get; set; }

        [DataMember]
        public string OrderDirection { get; set; }

        [DataMember]
        public bool ReturnMailBoxDeliveries { get; set; }
    }
}