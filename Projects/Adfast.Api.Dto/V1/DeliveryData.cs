﻿using System;
using System.Runtime.Serialization;

namespace Adfast.Api.Dto.V1
{
    /// <summary>
    /// File Upload Parameters
    /// </summary>
    [Serializable, DataContract]
    public class DeliveryData
    {
        /// <summary>
        /// Adfast Delivery Id / Tracking Number
        /// </summary>
        [DataMember]
        public int Id { get; set; }

        /// <summary>
        /// Publication Section Id (Maped to Adlib)
        /// </summary>
        [DataMember]
        public int PublicationSectionId { get; set; }

        /// <summary>
        /// Filename
        /// </summary>
        [DataMember]
        public string Filename { get; set; }

        [DataMember]
        public string Advertiser { get; set; }

        [DataMember]
        public string Brand { get; set; }

        /// <summary>
        /// Publication & Section Name
        /// </summary>
        [DataMember]
        public string Publication { get; set; }

        [DataMember]
        public decimal Width { get; set; }

        [DataMember]
        public decimal Height { get; set; }

        [DataMember]
        public DateTime? InsertionDate { get; set; }

        /// <summary>
        /// Unique Referenece Number
        /// </summary>
        [DataMember]
        public string Urn { get; set; }

        /// <summary>
        /// Indicates this is a MailBox Delivery (may not have a file)
        /// </summary>
        [DataMember]
        public bool MailBoxDelivery { get; set; }

        /// <summary>
        /// Thumbnail Url
        /// </summary>
        [DataMember]
        public string ThumbnailUrl { get; set; }

        /// <summary>
        /// Mono if true otherwise 
        /// </summary>
        [DataMember]
        public bool Mono { get; set; }

        [DataMember]
        public DateTime? UploadedDate { get; set; }

        [DataMember]
        public int FileSource { get; set; }

        [DataMember]
        public string FileSourceDescription { get; set; }

        [DataMember]
        public string ContactName { get; set; }

        [DataMember]
        public string ContactPhone { get; set; }

        [DataMember]
        public string ContactCompany { get; set; }

        [DataMember]
        public string ContactEmail { get; set; }

        /// <summary>
        /// Comments (Instructions to Publisher etc)
        /// </summary>
        [DataMember]
        public string Comments { get; set; }

        /// <summary>
        /// Delivery And Processing Costs applied
        /// </summary>
        [DataMember]
        public DeliveryCosts DeliveryCosts { get; set; }

        /// <summary>
        /// Created Date
        /// </summary>
        [DataMember]
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Last Updated Date
        /// </summary>
        [DataMember]
        public DateTime LastUpdatedDate { get; set; }

        /// <summary>
        /// Processing And Delivery Status Results
        /// </summary>
        [DataMember]
        public StatusResults StatusResults { get; set; }

        [DataMember]
        public bool IsSplitDpsDelivery { get; set; }

        [DataMember]
        public int DpsDeliveryLeftHandPageDeliveryId { get; set; }

        [DataMember]
        public int DpsDeliveryRightHandPageDeliveryId { get; set; }

        [DataMember]
        public string ExternalDeliveryReference { get; set; }

        [DataMember]
        public bool IsPadn { get; set; }

        /// <summary>
        /// File size on disk of delivered file
        /// </summary>
        [DataMember]
        public long FileSizeOnDisk { get; set; }

        [DataMember]
        public int PressCentreId { get; set; }

        [DataMember]
        public string PressCentreName { get; set; }

        [DataMember]
        public string PublicationName { get; set; }

        [DataMember]
        public string PublicationSectionName { get; set; }

        [DataMember]
        public string Campaign { get; set; }

        [DataMember]
        public string PartVersion { get; set; }

        [DataMember]
        public string TargetedAudience { get; set; }

        [DataMember]
        public string MetadataSource { get; set; }
    }
}