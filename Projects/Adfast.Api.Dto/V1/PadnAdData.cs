﻿using System;
using System.Runtime.Serialization;

namespace Adfast.Api.Dto.V1
{
    /// <summary>
    /// Padn Ad Data Members
    /// </summary>
    [Serializable, DataContract]
    public class PadnAdData
    {
        /// <summary>
        /// PADN Ad Id
        /// </summary>
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string AdSenderStatus { get; set; }

        [DataMember]
        public string Urn { get; set; }

        [DataMember]
        public int AdvertiserId { get; set; }

        [DataMember]
        public string AdvertiserName { get; set; }
        
        [DataMember]
        public string PublisherCode { get; set; }
        
        [DataMember]
        public string PublisherName { get; set; }
        
        [DataMember]
        public string PublicationCode { get; set; }
        
        [DataMember]
        public string PublicationName { get; set; }
        
        [DataMember]
        public string SectionCode { get; set; }

        [DataMember]
        public string SectionName { get; set; }
        
        [DataMember]
        public string SectionType { get; set; }
        
        [DataMember]
        public DateTime? EditionAsDateTime { get; set; }
        
        [DataMember]
        public string EditionAsString { get; set; }
        
        [DataMember]
        public string Deadline { get; set; }
        
        [DataMember]
        public DateTime? DeadlineDateTime { get; set; }
        
        [DataMember]
        public string DeadlineAsString { get; set; }
        
        [DataMember]
        public decimal Height { get; set; }
        
        [DataMember]
        public decimal Width { get; set; }
        
        [DataMember]
        public string Sender { get; set; }
        
        [DataMember]
        public string ConfirmedStatus { get; set; }
        
        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string Brand { get; set; }
        
        /// <summary>
        /// Adfast Delivery Id (AKA Tracking Number)
        /// </summary>
        [DataMember]
        public int DeliveryId { get; set; }
        
        [DataMember]
        public Guid DeliveryGuid { get; set; }
        
        [DataMember]
        public bool DeliveryInitiated { get; set; }
        
        [DataMember]
        public bool DeliverySent { get; set; }
        
        [DataMember]
        public bool DeliveryReceived { get; set; }
        
        [DataMember]
        public bool DeliveryHasFile { get; set; }
        
        [DataMember]
        public int MailBoxDeliveryId { get; set; }
        
        [DataMember]
        public Guid MailBoxDeliveryGuid { get; set; }
        
        [DataMember]
        public string MailBoxDeliveryStatus { get; set; }
        
        [DataMember]
        public string MailBoxDeliverySendDate { get; set; }
        
        [DataMember]
        public int? AdfastPublicationSectionId { get; set; }
        
        [DataMember]
        public string AdfastPublicationSectionName { get; set; }
        
        [DataMember]
        public int AdfastPublicationSizeId { get; set; }
        
        [DataMember]
        public string AdfastPublicationSizeName { get; set; }

        [DataMember]
        public bool IsOption { get; set; }

        [DataMember]
        public DateTime? CreatedDateAsDateTime { get; set; }

        [DataMember]
        public string CreatedDateAsString { get; set; }
    }
}
