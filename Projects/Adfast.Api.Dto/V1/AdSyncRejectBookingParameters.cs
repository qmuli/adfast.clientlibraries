﻿using System;
using System.Runtime.Serialization;

namespace Adfast.Api.Dto.V1
{
    [Serializable]
    [DataContract]
    public class AdSyncRejectBookingParameters
    {
        [DataMember]
        public AdNetwork? AdNetwork { get; set; }

        [DataMember]
        public string PublisherReference { get; set; }

        [DataMember]
        public int? BookingId { get; set; }

        [DataMember]
        public string SenderEmailAddress { get; set; }

        [DataMember]
        public string SenderName { get; set; }

        [DataMember]
        public string SenderPhone { get; set; }

        [DataMember]
        public string SenderCompanyName { get; set; }

        [DataMember]
        public string Notes { get; set; }
    }
}
