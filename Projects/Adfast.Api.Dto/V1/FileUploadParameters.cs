﻿using System;
using System.Runtime.Serialization;

namespace Adfast.Api.Dto.V1
{
    /// <summary>
    /// File Upload Parameters
    /// </summary>
    [Serializable, DataContract]
    public class FileUploadParameters
    {
        public FileUploadParameters()
        {
            Optimize = false;
            Height = 0;
            Mono = false;
            AutoFontFix = false;
            AutoUpsampleFix = false;
            IgnoreSizeCheck = false;
            IsPadn = false;
        }

        [DataMember]
        public UploadSubmissionType SubmissionType { get; set; }

        [DataMember]
        public DeliveryRule DeliveryRule { get; set; }

        /// <summary>
        /// Publication Section Id (From Adlib API)
        /// </summary>
        [DataMember]
        public int PublicationSectionId { get; set; }

        /// <summary>
        /// Publication Section Size Id (From Adlib API) Optional, Size will be validated against size if supplied
        /// </summary>
        [DataMember]
        public int PublicationSectionSizeId { get; set; }

        /// <summary>
        /// Publisher Code, External publisher mapping code 
        /// </summary>
        [DataMember]
        public string PublisherCode { get; set; }

        /// <summary>
        /// Publication Section Code (External Publisher Mapping code for Section, used insead of PublicationSectionId)
        /// </summary>
        [DataMember]
        public string PublicationSectionCode { get; set; }

        /// <summary>
        /// Height if Section Size is a column size
        /// </summary>
        [DataMember]
        public decimal Height { get; set; }

        /// <summary>
        /// Width (used with height and PublicationSectionCode to get Publication Section & Size Id, if Section & Size Id not supplied)
        /// </summary>
        [DataMember]
        public decimal Width { get; set; }

        [DataMember]
        public bool Mono  { get; set; }

        [DataMember]
        public bool Optimize { get; set; }

        [DataMember]
        public DateTime InsertionDate { get; set; }

        [DataMember]
        public string Comments { get; set; }

        /// <summary>
        /// Unique Referenece Number for Publisher - Optional
        /// </summary>
        [DataMember]
        public string Urn { get; set; }

        [DataMember]
        public string Advertiser { get; set; }

        [DataMember]
        public string Brand { get; set; }

        [DataMember]
        public string ContactName { get; set; }

        [DataMember]
        public string ContactCompany { get; set; }

        [DataMember]
        public string ContactPhone { get; set; }

        [DataMember]
        public string ContactEmail { get; set; }

        /// <summary>
        /// DeliverTo Title CustomEmail or CustomFtp DeliveryRule
        /// </summary>
        [DataMember]
        public string DeliveryToTitle { get; set; }

        /// <summary>
        /// Delivery Email Address for CustomEmail DeliveryRule
        /// </summary>
        [DataMember]
        public string DeliveryToEmailAddress { get; set; }

        /// <summary>
        /// DeliverTo FtpHost for CustomFtp DeliveryRule
        /// </summary>
        [DataMember]
        public string DeliveryToFtpHost { get; set; }

        /// <summary>
        /// DeliverTo Ftp Path/Directory for CustomFtp DeliveryRule
        /// </summary>
        [DataMember]
        public string DeliveryToFtpPath{ get; set; }

        /// <summary>
        /// DeliverTo Ftp Port for CustomFtp DeliveryRule
        /// </summary>
        [DataMember]
        public int DeliveryToFtpPort { get; set; }

        /// <summary>
        /// DeliverTo Username for CustomFtp DeliveryRule
        /// </summary>
        [DataMember]
        public string DeliveryToFtpUsername { get; set; }

        /// <summary>
        /// DeliverTo Password for CustomFtp DeliveryRule
        /// </summary>
        [DataMember]
        public string DeliveryToFtpPassword { get; set; }

        [DataMember]
        public bool AutoFontFix { get; set; }

        [DataMember]
        public bool AutoUpsampleFix { get; set; }

        [DataMember]
        public bool IgnoreSizeCheck { get; set; }

        [DataMember]
        public bool CropFile { get; set; }

        /// <summary>
        /// This is an override to process the file to a specific standard (See Adlib API for list of Qmuli Standards)
        /// </summary>
        [DataMember]
        public int QmuliStandardWorkflowId { get; set; }

        [DataMember]
        public bool SplitDpsDelivery { get; set; }

        [DataMember]
        public string SplitDpsDeliveryLeftHandPageUrn { get; set; }

        [DataMember]
        public string SplitDpsDeliveryRightHandPageUrn { get; set; }

        [DataMember]
        public string ExternalDeliveryReference { get; set; }

        /// <summary>
        /// Indicates this job has been created via a Papermule PADN workflow
        /// </summary>
        [DataMember]
        public bool IsPadn { get; set; }
    }
}