using System;
using System.Runtime.Serialization;

namespace Adfast.Api.Dto.V1
{
    [Serializable, DataContract]
    public class PadnAdListParameters
    {
        /// <summary>
        /// Sender Email Address Override (if API override is enabled) Otherwise API User Account Master email address is used.
        /// </summary>
        [DataMember]
        public string SenderEmailAddress { get; set; }

        /// <summary>
        /// Comma Separated List of Publisher Codes (empty = all)
        /// </summary>
        [DataMember]
        public string PublisherFilter { get; set; }

        /// <summary>
        /// Status Filter: Available Status Codes: Predicted, Confirmed, ConfirmedExternal, Rejected (Denied)
        /// </summary>
        [DataMember]
        public string StatusFilter { get; set; }
        
        /// <summary>
        /// Page Number according to the specified filters
        /// </summary>
        [DataMember]
        public int PageNumber { get; set; }

        /// <summary>
        /// Page Size Returned
        /// </summary>
        [DataMember]
        public int PageSize { get; set; }
        
        /// <summary>
        /// Search Phrase (searches available text)
        /// </summary>
        [DataMember]
        public string SearchPhrase { get; set; }
        
        /// <summary>
        /// SortColumn, the list will be sorted by the specified column, available values: AdSenderStatus, Urn, Publication, Advertiser, Sender, EditionAsDateTime, Deadline (default = Deadline)
        /// </summary>
        [DataMember]
        public string SortColumn { get; set; }
        
        /// <summary>
        /// Sort Direction: (asc or desc) (default = asc)
        /// </summary>
        [DataMember]
        public string SortDirection { get; set; }
        
        /// <summary>
        /// Filters Ads where the deadline is within the specified days
        /// </summary>
        [DataMember]
        public int? DaysFilter { get; set; }   
    }
}