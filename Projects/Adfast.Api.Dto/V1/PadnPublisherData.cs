﻿using System;
using System.Runtime.Serialization;

namespace Adfast.Api.Dto.V1
{
    [Serializable, DataContract]
    public class PadnPublisherData
    {
        [DataMember]
        public string PublisherCode { get; set; }

        [DataMember]
        public string PublisherName { get; set; }
    }
}
