using System;
using System.Runtime.Serialization;

namespace Adfast.Api.Dto.V1
{
    [Serializable]
    [DataContract]
    public class AdSyncApiListFilters
    {
        [DataMember]
        public string SenderEmailAddress { get; set; }

        [DataMember]
        public string PublisherFilter { get; set; }

        [DataMember]
        public string StatusFilter { get; set; }

        [DataMember]
        public int PageNumber { get; set; }

        [DataMember]
        public int PageSize { get; set; }

        [DataMember]
        public string SearchPhrase { get; set; }

        [DataMember]
        public string SortColumn { get; set; }

        [DataMember]
        public string SortDirection { get; set; }

        [DataMember]
        public int? DaysFilter { get; set; }

        [DataMember]
        public string Urn { get; set; }
    }
}