﻿namespace Adfast.Api.Dto.V1
{
    public class AdSyncPublisherListItem
    {
        public string Value { get; set; }

        public string Name { get; set; }
    }
}
