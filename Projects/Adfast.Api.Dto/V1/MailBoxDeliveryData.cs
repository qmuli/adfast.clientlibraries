﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Adfast.Api.Dto.V1
{
    [Serializable, DataContract]
    public class MailBoxDeliveryData
    {
        public MailBoxDeliveryData()
        {
            Height = 0;
            IsMono = false;
            IgnoreSizeCheck = false;
            Sent = false;
            EmailSentExternally = false;
            Insertions = new List<MailBoxInsertion>();
        }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public Guid Guid { get; set; }

        [DataMember]
        public int PublicationSectionSizeId { get; set; }

        [DataMember]
        public string Urn { get; set; }

        [DataMember]
        public string AccountNumber { get; set; }

        [DataMember]
        public string Advertiser { get; set; }

        [DataMember]
        public string Brand { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public Decimal Height { get; set; }

        [DataMember]
        public bool IsMono { get; set; }

        [DataMember]
        public string Comments { get; set; }

        [DataMember]
        public bool IgnoreSizeCheck { get; set; }

        [DataMember]
        public DateTime SendDate { get; set; }

        [DataMember]
        public bool AutoCreateAdfastDelivery { get; set; }

        [DataMember]
        public bool SendAutoCreatedAdfastDeliveryReminder { get; set; }

        [DataMember]
        public bool EmailSentExternally { get; set; }

        [DataMember]
        public bool Sent { get; set; }

        [DataMember]
        public bool Cancelled { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public DateTime LastUpdatedDate { get; set; }

        [DataMember]
        public string EmailLinkCallBackStatus { get; set; }

        [DataMember]
        public bool UrgentDeadline { get; set; }

        [DataMember]
        public string UploadPageUrl { get; set; }

        /// <summary>
        /// One or more insertions (PublicationSection Id and Insertion Date)
        /// </summary>
        [DataMember]
        public List<MailBoxInsertion> Insertions { get; set; }

        [DataMember]
        public int? AdfastDeliveryId { get; set; }

        [DataMember]
        public DateTime UploadDate { get; set; }

        [DataMember]
        public bool CallbackProcessed { get; set; }

        [DataMember]
        public bool CallbackFailed { get; set; }

        [DataMember]
        public int CallbackRetryCount { get; set; }

        [DataMember]
        public string CallbackError { get; set; }
    }
}
