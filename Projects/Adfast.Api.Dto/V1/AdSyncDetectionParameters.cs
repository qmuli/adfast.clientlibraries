﻿using System;
using System.Runtime.Serialization;

namespace Adfast.Api.Dto.V1
{
    [Serializable]
    [DataContract]
    public class AdSyncDetectionParameters
    {
        [DataMember]
        public string SenderEmailAddress { get; set; }
        [DataMember]
        public int? PublicationSectionId { get; set; }
        [DataMember]
        public string Urn { get; set; }
        [DataMember]
        public DateTime? InsertionDate { get; set; }
        [DataMember]
        public double? Width { get; set; }
        [DataMember]
        public double? Height { get; set; }
    }
}
