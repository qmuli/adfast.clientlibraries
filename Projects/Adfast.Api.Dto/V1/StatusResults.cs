﻿using System;
using System.Runtime.Serialization;

namespace Adfast.Api.Dto.V1
{
    [Serializable, DataContract]
    public class StatusResults
    {
        [DataMember]
        public string DeliveryStatusDescription { get; set; }

        [DataMember]
        public string FileToDeliverDescription { get; set; }

        [DataMember]
        public bool DeliveryInitiated { get; set; }

        [DataMember]
        public DateTime? DeliveryInitiatedDate { get; set; }

        [DataMember]
        public bool DeliverySent { get; set; }

        [DataMember]
        public DateTime? DeliverySentDate { get; set; }

        [DataMember]
        public bool DeliveryReceived { get; set; }

        [DataMember]
        public DateTime? DeliveryReceivedDate { get; set; }

        [DataMember]
        public bool AutoDelivery { get; set; }

        [DataMember]
        public bool AutoDeliveryError { get; set; }

        [DataMember]
        public bool HasFile { get; set; }

        [DataMember]
        public string AutoDeliveryErrorMessage { get; set; }

        [DataMember]
        public string AuthorizedBy { get; set; }

        [DataMember]
        public DateTime? AuthorizedDate { get; set; }

        [DataMember]
        public bool UploadProcessed { get; set; }

        [DataMember]
        public int UploadStatus { get; set; }

        [DataMember]
        public string UploadStatusDescription { get; set; }

        [DataMember]
        public bool FixProcessed { get; set; }

        [DataMember]
        public int FixStatus { get; set; }

        [DataMember]
        public string FixStatusDescription { get; set; }

        [DataMember]
        public bool ColorCorrectionProcessed { get; set; }

        [DataMember]
        public int ColorCorrectionStatus { get; set; }

        [DataMember]
        public string ColorCorrectionStatusDescription { get; set; }

        // Files
        [DataMember]
        public string UploadedPdfUrl { get; set; }

        [DataMember]
        public string UploadFlightCheckReportUrl { get; set; }

        [DataMember]
        public string UploadConvertedSoftProofUrl { get; set; }

        [DataMember]
        public string FixFlightCheckReportUrl { get; set; }

        [DataMember]
        public string FixConvertedSoftProofUrl { get; set; }

        [DataMember]
        public string FixPdfUrl { get; set; }

        [DataMember]
        public string ColorCorrectedFlightCheckReportUrl { get; set; }

        [DataMember]
        public string ColorCorrectedSoftProofUrl { get; set; }

        [DataMember]
        public string ColorCorrectedPdfUrl { get; set; }

        [DataMember]
        public string DeliveredPdfUrl { get; set; }

        [DataMember]
        public bool CanDeliverOriginalFile { get; set; }

        [DataMember]
        public bool CanDeliverFixedFile { get; set; }

        [DataMember]
        public bool CanDeliverOptimisedFile { get; set; }

        [DataMember]
        public string UncroppedFileUrl { get; set; }

        [DataMember]
        public string CroppedFileUrl { get; set; }

        [DataMember]
        public string DeliveredCiUrl { get; set; }
    }
}
