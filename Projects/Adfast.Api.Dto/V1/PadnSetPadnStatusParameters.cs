﻿
using System;
using System.Runtime.Serialization;

namespace Adfast.Api.Dto.V1
{
    [Serializable, DataContract]
    public class PadnSetPadnStatusParameters
    {
        [DataMember]
        public int Id { get; set; }

        /// <summary>
        /// PADN Id
        /// </summary>
        [DataMember]
        public string PadnId { get; set; }

        /// <summary>
        /// PADN Id
        /// </summary>
        [DataMember]
        public string PadnCreatedDate { get; set; }
    }
}