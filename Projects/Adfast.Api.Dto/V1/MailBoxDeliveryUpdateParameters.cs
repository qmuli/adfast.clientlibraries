﻿using System;
using System.Runtime.Serialization;

namespace Adfast.Api.Dto.V1
{
    [Serializable, DataContract]
    public class MailBoxDeliveryUpdateParameters
    {
        public MailBoxDeliveryUpdateParameters()
        {
            Height = 0;
            IsMono = false;
            IgnoreSizeCheck = false;
            AutoCreateAdfastDelivery = false;
            SendAutoCreatedAdfastDeliveryReminder = false;
            //Insertions = new List<MailBoxInsertion>();
        }

        [DataMember]
        public int? Id { get; set; }

        [DataMember]
        public int PublicationSectionSizeId { get; set; }


        [DataMember]
        public int PublicationSectionCode { get; set; }

        [DataMember]
        public string UpdateByUrn { get; set; }

        [DataMember]
        public string Urn { get; set; }

        [DataMember]
        public string Advertiser { get; set; }

        [DataMember]
        public string Brand { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public Decimal Height { get; set; }

        /// <summary>
        /// Width (used with height and PublicationSectionCode to get Publication Section & Size Id, if Section & Size Id not supplied)
        /// </summary>
        [DataMember]
        public Decimal Width { get; set; }

        [DataMember]
        public bool IsMono { get; set; }

        [DataMember]
        public string Comments { get; set; }

        [DataMember]
        public bool IgnoreSizeCheck { get; set; }

        [DataMember]
        public DateTime SendDate { get; set; }

        [DataMember]
        public DateTime DeadlineDate { get; set; }

        [DataMember]
        public bool AutoCreateAdfastDelivery { get; set; }

        [DataMember]
        public bool UrgentDeadline { get; set; }

        [DataMember]
        public bool SendAutoCreatedAdfastDeliveryReminder { get; set; }

        ///// <summary>
        ///// One or more insertions (PublicationSection Id and Insertion Date)
        ///// </summary>
        //[DataMember]
        //public List<MailBoxInsertion> Insertions { get; set; }
    }
}
