﻿using System;

namespace Adfast.Api.Dto
{
    [Serializable]
    public enum PadnAdProductionStatus
    {
        /// <summary>
        /// Unknown, or ALL when used as a filter
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// AwaitingCopy
        /// </summary>
        AwaitingCopy = 1,

        /// <summary>
        /// Finished
        /// </summary>
        Finished = 2,

        /// <summary>
        /// Killed Ads
        /// </summary>
        Killed = 3
    }
}
