﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Adfast.Api.Dto")]
[assembly: AssemblyDescription("Adfast API Client/Server Dto/Model Objects")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Qmuli Limited")]
[assembly: AssemblyProduct("Adfast.Api.Dto")]
[assembly: AssemblyCopyright("Copyright © 2025")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("31c80131-cec3-4ee6-8840-220ba478abdf")]
[assembly: AssemblyVersion("3.2.0")]
[assembly: AssemblyFileVersion("3.2.0")]
[assembly: AssemblyInformationalVersion("3.2.0")]