﻿using System;
using System.Runtime.Serialization;

namespace Adfast.Api.Dto
{
    [Serializable, DataContract]
    public class StringResult
    {
        public StringResult()
        {            
            Value = String.Empty;
        }

        public StringResult(string value)
        {            
            Value = value;
        }

        [DataMember]
        public string Value  { get; set; }
    }
}