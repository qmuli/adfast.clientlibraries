﻿
using System.Collections.Generic;

namespace Adfast.Api.Dto
{
    public class DeliveryImportResult
    {
        public DeliveryImportResult()
        {
            NewDeliveryIdList = new List<int>();
            ErrorList = new List<DeliveryImportError>();
        }

        /// <summary>
        /// ID list for all new deliveries created
        /// </summary>
        public List<int> NewDeliveryIdList { get; set; }

        /// <summary>
        /// Error list for all deliveries that could be created
        /// </summary>
        public List<DeliveryImportError> ErrorList { get; set; }
    }
}