﻿using System;
using System.Runtime.Serialization;

namespace Adfast.Api.Dto
{
    [Serializable, DataContract]
    public class IntResult
    {
        public IntResult()
        {            
            Value = 0;
        }

        public IntResult(int value)
        {            
            Value = value;
        }

        [DataMember]
        public int Value  { get; set; }
    }
}
