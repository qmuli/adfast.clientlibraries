﻿using System;
using System.Runtime.Serialization;

namespace Adfast.Api.Dto
{
    [Serializable, DataContract]
    public class IdParameter
    {
        [DataMember]
        public int Id { get; set; }
    }
}
