﻿
namespace Adfast.Api.Dto
{
    public class DeliveryImportError
    {
        public string Reference { get; set; }

        public string Message { get; set; }
    }
}