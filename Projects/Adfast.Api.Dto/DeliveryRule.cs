﻿using System;

namespace Adfast.Api.Dto
{
    [Serializable]
    public enum DeliveryRule
    {
        Default = 0,
        CustomEmail = 1,
        CustomFtp = 2
    }
}
