﻿using System;
using System.Runtime.Serialization;

namespace Adfast.Api.Dto
{
    [Serializable, DataContract]
    public class StringParameter
    {
        [DataMember]
        public string Value { get; set; }
    }
}
