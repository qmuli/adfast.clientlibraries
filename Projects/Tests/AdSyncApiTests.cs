﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class AdSyncApiTests
    {
        /// <summary>
        /// Test PadnAdDetectionParameters
        /// </summary>
        [TestMethod]
        public async Task ListPublishersAndGetAds()
        {
            try
            {
                var adsyncPublisherList = await Adfast.Api.Client.AdSync.V1.GetPublisherList(true);

                Assert.IsTrue(adsyncPublisherList.Count() > 0, "No AdSync publishers returned");
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
