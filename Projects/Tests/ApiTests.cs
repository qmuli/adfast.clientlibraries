﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class ApiTests
    {
        /// <summary>
        /// Test PadnAdDetectionParameters
        /// </summary>
        //[TestMethod]
        //public async Task TestPadnDetection()
        //{
        //    try
        //    {
        //        var parameters = new Adfast.Api.Dto.V1.PadnAdDetectionParameters
        //        {
        //            SenderEmailAddress = "tony.taylor@qmuli.com",
        //            InsertionDate = DateTime.Parse("14/12/2019 17:36:5"),
        //            PublicationSectionId = 25158
        //        };

        //        var padnList = await Adfast.Api.Client.V1.GetPadnAdsByDetectionFilters(parameters);

        //        Assert.IsTrue(padnList.List.ToList().Count() > 0, "No padn jobs returned, try other parameters");
        //    }
        //    catch (Exception exception)
        //    {
        //        throw exception;
        //    }
        //}

        [TestMethod]
        public async Task UploadFile()
        {
            try
            {
                var parameters = new Adfast.Api.Dto.V1.FileUploadParameters
                {
                    SubmissionType = Adfast.Api.Dto.UploadSubmissionType.UploadWithChecks,
                    DeliveryRule = Adfast.Api.Dto.DeliveryRule.Default,
                    PublicationSectionId = 19654,
                    PublicationSectionSizeId = 52401,
                    Height = 0,
                    Mono = false,
                    Optimize = true,
                    InsertionDate = DateTime.Now.AddDays(3),
                    Comments = "This is a test delivery",
                    Urn = "TEST_DPS",
                    Advertiser = "Test Advertiser",
                    Brand = "Test Brand",
                    ContactName = "Tester",
                    ContactCompany = "Qmuli Limited",
                    ContactPhone = "0000",
                    ContactEmail = "support@qmuli.com",
                    DeliveryToTitle = "",
                    DeliveryToEmailAddress = "",
                    DeliveryToFtpHost = "",
                    DeliveryToFtpPath = "",
                    DeliveryToFtpPort = -1,
                    DeliveryToFtpUsername = "",
                    DeliveryToFtpPassword = "",
                    AutoFontFix = false,
                    AutoUpsampleFix = false,
                    IgnoreSizeCheck = false,
                    CropFile = false,
                    QmuliStandardWorkflowId = 0,
                    SplitDpsDelivery = true,
                    SplitDpsDeliveryLeftHandPageUrn = "TEST_DPS_LEFT",
                    SplitDpsDeliveryRightHandPageUrn = "TEST_DPS_RIGHT",
                    ExternalDeliveryReference = "API_TEST"
                };

                var newId = await Adfast.Api.Client.V1.UploadFileAsync("TestFile.pdf", System.IO.File.ReadAllBytes(@"C:\Test.pdf"), parameters);

                Assert.IsTrue(newId > 0, "Error, Adfast ID not generated");
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
