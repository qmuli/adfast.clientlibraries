using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Tests")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Tests")]
[assembly: AssemblyCopyright("Copyright © 2025")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("ae585da5-1e4f-44c7-9978-eedb1e509982")]

// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("5.2.2.0")]
[assembly: AssemblyFileVersion("5.2.2.0")]
