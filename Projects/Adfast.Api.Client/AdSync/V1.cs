﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Adfast.Api.Dto.V1;
using Adfast.Api.Dto;

namespace Adfast.Api.Client.AdSync
{
    /// <summary>
    /// Adfast AdSync API Client Method Library (For Version 1)
    /// </summary>
    public static class V1
    {
        ///  <summary>
        /// GetPublisherList gets the Publishers references (ID and or codes) for supported publishers (includes available PADN Publisher Codes and Qmuli AdSync Publisher IDs)
        ///  </summary>
        ///  <param name="requestSettings">Optional Request setting, uses passed in settings or defaults to application config settings</param>
        /// <param name="includeTestPublishers">eg PADN MediaMule</param>
        /// <returns>Returns a list containing Publisher Code/IDs and PublisherName used in related API calls, based on user API configuration</returns>
        public static async Task<List<AdSyncPublisherListItem>> GetPublisherList(bool includeTestPublishers, RequestSettings requestSettings = null)
        {
            var client = GetClient(requestSettings ?? DefaultRequestSettings);

            var response = await client.GetAsync($"GetPublisherList/?includeTestPublishers={includeTestPublishers.ToString().ToLower()}");

            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsAsync<List<AdSyncPublisherListItem>>();
            }

            throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        /// <summary>
        /// Gets the AdSync Ad Booking List for the specified paging/filter parameters. Includes available PADN and AdSync Bookings from supported publishers
        /// </summary>
        /// <param name="padnAdListParameters"></param>
        /// <param name="requestSettings">Optional Request setting, uses passed in settings or defaults to application config settings</param>
        /// <returns>List of AdSync Ad Booking Data (PADN and/or AdSync data is returned based on API User configuration)</returns>
        public static async Task<AdSyncListResult> GetBookingList(AdSyncApiListFilters adSyncApiListFilters, RequestSettings requestSettings = null)
        {
            var client = GetClient(requestSettings ?? DefaultRequestSettings);

            var response = await client.GetAsync($"GetBookingList/?SenderEmailAddress={HttpUtility.UrlEncode(adSyncApiListFilters.SenderEmailAddress)}&PublisherFilter={HttpUtility.UrlEncode(adSyncApiListFilters.PublisherFilter)}&StatusFilter={HttpUtility.UrlEncode(adSyncApiListFilters.StatusFilter)}&PageNumber={adSyncApiListFilters.PageNumber}&PageSize={adSyncApiListFilters.PageSize}&SearchPhrase={HttpUtility.UrlEncode(adSyncApiListFilters.SearchPhrase)}&SortColumn={HttpUtility.UrlEncode(adSyncApiListFilters.SortColumn)}&SortDirection={HttpUtility.UrlEncode(adSyncApiListFilters.SortDirection)}&DaysFilter={adSyncApiListFilters.DaysFilter}&Urn={HttpUtility.UrlEncode(adSyncApiListFilters.Urn)}");

            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsAsync<AdSyncListResult>();
            }

            throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        /// <summary>
        /// Confirm PADN Job by PADN Publisher Code and PADN Ad Id.
        /// API User account details will be used unless overridden.
        /// </summary>
        /// <param name="publisherCode"></param>
        /// <param name="adId"></param>
        /// <param name="senderName"></param>
        /// <param name="note"></param>
        /// <param name="senderPhone"></param>
        /// <param name="senderCompanyName"></param>
        /// <param name="senderEmailAddress"></param>
        /// <param name="requestSettings">Optional Request setting, uses passed in settings or defaults to application config settings</param>
        /// <returns></returns>
        public static async Task<HttpStatusCode> ConfirmBooking(AdSyncConfirmBookingParameters adSyncConfirmBookingParameters, RequestSettings requestSettings = null)
        {
            var client = GetClient(requestSettings ?? DefaultRequestSettings);

            var response = await client.PostAsJsonAsync($"ConfirmBooking/", adSyncConfirmBookingParameters);

            return response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.NotModified ? response.StatusCode : throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        /// <summary>
        /// Reject PADN Job by PADN Publisher Code and PADN Ad Id.
        /// API User account details will be used unless overridden.
        /// </summary>
        /// <param name="publisherCode"></param>
        /// <param name="adId"></param>
        /// <param name="senderName"></param>
        /// <param name="note"></param>
        /// <param name="senderPhone"></param>
        /// <param name="senderCompanyName"></param>
        /// <param name="senderEmailAddress"></param>
        /// <param name="requestSettings">Optional Request setting, uses passed in settings or defaults to application config settings</param>
        /// <returns></returns>
        public static async Task<HttpStatusCode> RejectBooking(AdSyncRejectBookingParameters adSyncRejectBookingParameters, RequestSettings requestSettings = null)
        {
            var client = GetClient(requestSettings ?? DefaultRequestSettings);

            var response = await client.PostAsJsonAsync($"RejectBooking/", adSyncRejectBookingParameters);

            return response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.NotModified ? response.StatusCode : throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        /// <summary>
        /// Sets an existing adfast delivery as AdSync/PADN (assumes that the relevant match has been confirmed via the call to ConfirmBooking as a match has been identified)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="requestSettings"></param>
        /// <returns></returns>
        public static async Task<HttpStatusCode> SetAdfastDeliveryAsAdSync(SetDeliveryAsAdSyncParameters setDeliveryAsAdSyncParameters, RequestSettings requestSettings = null)
        {
            var client = GetClient(requestSettings ?? DefaultRequestSettings);

            if (setDeliveryAsAdSyncParameters.Id <= 0)
            {
                throw new Exception($"SetAdfastDeliveryAsAdSync: Id must be greater than zero");
            }

            var response = await client.PostAsync($"SetAdfastDeliveryAsAdSync?AdNetwork={(int)setDeliveryAsAdSyncParameters.AdNetwork}&id={setDeliveryAsAdSyncParameters.Id}&bookingId={HttpUtility.UrlEncode(setDeliveryAsAdSyncParameters.BookingId)}&adSyncCreatedDate={HttpUtility.UrlEncode(setDeliveryAsAdSyncParameters.AdSyncCreatedDate ?? "")}", null);

            return response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.NotModified ? response.StatusCode : throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        /// <summary>
        /// Gets the AdSync/PADN Sender Ad Count for all publishers
        /// </summary>
        /// <param name="senderEmailAddress">Sender email override, uses API user email if not specified</param>
        /// <param name="includeTestPublishers"></param>
        /// <param name="requestSettings"></param>
        /// <returns></returns>
        public static int GetSenderAdCount(string senderEmailAddress, bool? includeTestPublishers, RequestSettings requestSettings = null)
        {
            using (var client = GetClient(requestSettings ?? DefaultRequestSettings))
            {
                var response = client.GetAsync($"GetSenderAdCount/?senderEmailAddress={senderEmailAddress ?? ""}&includeTestPublishers={(includeTestPublishers ?? false).ToString().ToLower()}").Result;

                var result = 0;

                if (response.IsSuccessStatusCode)
                {
                    if (!int.TryParse(response.Content.ReadAsStringAsync().Result ?? "0", out result))
                    {
                        result = 0;
                    }
                }

                return result;
            }
        }

        /// <summary>
        /// GetBookingsByDetectionFilters gets the list of ads for the specified filters, used to detect AdSync bookings user selections
        /// </summary>
        /// <param name="adSyncDetectionParameters"></param>
        /// <param name="requestSettings">Optional Request setting, uses passed in settings or defaults to application config settings</param>
        /// <returns>List of AdSync Ad Booking Data (PADN and/or AdSync data is returned based on API User configuration)</returns>
        public static async Task<AdSyncListResult> GetBookingsByDetectionFilters(AdSyncDetectionParameters adSyncDetectionParameters, RequestSettings requestSettings = null)
        {
            var client = GetClient(requestSettings ?? DefaultRequestSettings);

            var response = await client.GetAsync($"GetBookingsByDetectionFilters?apiParameters.SenderEmailAddress={HttpUtility.UrlEncode(adSyncDetectionParameters.SenderEmailAddress)}&apiParameters.PublicationSectionId={(adSyncDetectionParameters.PublicationSectionId.HasValue && adSyncDetectionParameters.PublicationSectionId > 0 ? adSyncDetectionParameters.PublicationSectionId.ToString() : "")}&apiParameters.Urn={HttpUtility.UrlEncode(adSyncDetectionParameters.Urn)}&apiParameters.InsertionDate={(adSyncDetectionParameters.InsertionDate.HasValue ? HttpUtility.UrlEncode(adSyncDetectionParameters.InsertionDate.Value.ToString("dd/MM/yyyy")) : "")}&apiParameters.Width={HttpUtility.UrlEncode((adSyncDetectionParameters.Width.HasValue & adSyncDetectionParameters.Width > 0 ? adSyncDetectionParameters.Width.ToString() : "0").ToString())}&apiParameters.Height={HttpUtility.UrlEncode((adSyncDetectionParameters.Height.HasValue & adSyncDetectionParameters.Height > 0 ? adSyncDetectionParameters.Height.ToString() : "0").ToString())}");

            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsAsync<AdSyncListResult>();
            }

            throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        private static RequestSettings DefaultRequestSettings
        {
            get
            {
                var result = new RequestSettings
                {
                    AdfastApiUrl = Settings.AdfastApiUrl,
                    AdfastApiUsername = Settings.AdfastApiUsername,
                    AdfastApiKey = Settings.AdfastApiKey
                };

                return result;
            }
        }

        private static HttpClient GetClient(RequestSettings requestSettings)
        {
            // Adjust Url For AdSync Url
            var baseUrl = AdjustBaseUrlForAdSync(requestSettings.AdfastApiUrl);

            var client = new HttpClient
            {
                BaseAddress = new Uri(baseUrl)
            };
            client.DefaultRequestHeaders.Add("Username", requestSettings.AdfastApiUsername);
            client.DefaultRequestHeaders.Add("ApiKey", requestSettings.AdfastApiKey);

            return client;
        }

        private static string AdjustBaseUrlForAdSync(string baseUrlSetting)
        {
            baseUrlSetting = baseUrlSetting.ToLower();
            if (baseUrlSetting.Contains("/api/v1"))
            {
                baseUrlSetting = baseUrlSetting.Replace("/api/v1", "/api/adsyncV1");
            }
            return baseUrlSetting;
        }
    }
}