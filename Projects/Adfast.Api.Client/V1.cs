﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using Adfast.Api.Dto.V1;
using Adfast.Api.Dto;

namespace Adfast.Api.Client
{
    /// <summary>
    /// Adfast Api Client Method Library (For Version 1)
    /// </summary>
    public static class V1
    {
        public static async Task<int> UploadFileAsync(string filename, byte[] fileContent, FileUploadParameters fileUploadParameters, RequestSettings requestSettings)
        {
            var client = GetClient(requestSettings);

            using (var formData = new MultipartFormDataContent())
            {
                var fileUploadParametersJson = Newtonsoft.Json.JsonConvert.SerializeObject(fileUploadParameters);
                var stringContent = new StringContent(fileUploadParametersJson);
                stringContent.Headers.Add("Content-Disposition", "form-data; name=\"FileUploadParameters\"");
                formData.Add(stringContent, "FileUploadParameters");

                var filePostData = new ByteArrayContent(fileContent);
                filePostData.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = HttpUtility.UrlEncode(filename)
                };
                formData.Add(filePostData);

                var response = await client.PostAsync(requestSettings.AdfastApiUrl + "UploadFile/", formData);

                if (response.IsSuccessStatusCode)
                {
                    return response.Content.ReadAsAsync<IntResult>().Result.Value;
                }

                throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
            }
        }

        public static async Task<int> UploadFileAsync(string filename, byte[] fileContent, FileUploadParameters fileUploadParameters)
        {
            return await UploadFileAsync(filename, fileContent, fileUploadParameters, DefaultRequestSettings);
        }

        /// <summary>
        /// Gets Processing and Delivery Status for the specified Adfast delivery Id (tracking number)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="requestSettings">settings for the api request</param>
        /// <returns></returns>
        public static async Task<StatusResults> GetDeliveryStatus(int id, RequestSettings requestSettings)
        {
            var client = GetClient(requestSettings);

            var response = await client.GetAsync($"{requestSettings.AdfastApiUrl}GetDeliveryStatus/?Id={id}");

            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsAsync<StatusResults>().Result;
            }

            throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        public static async Task<StatusResults> GetDeliveryStatus(int id)
        {
            return await GetDeliveryStatus(id, DefaultRequestSettings);
        }

        public static async Task<DeliveryData> GetDeliveryDetails(int id, RequestSettings requestSettings)
        {
            var client = GetClient(requestSettings);

            var response = await client.GetAsync($"{requestSettings.AdfastApiUrl}GetDeliveryDetails/?Id={id}");

            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsAsync<DeliveryData>();
            }

            throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        public static async Task<DeliveryData> GetDeliveryDetails(int id)
        {
            return await GetDeliveryDetails(id, DefaultRequestSettings);
        }

        /// <summary>
        /// Gets the delivery list (aka User Delivery Tracking page, Publisher Deliveries)
        /// </summary>
        /// <param name="deliveryListParameters"></param>
        /// <param name="requestSettings">Optional Request setting, uses passed in settings or defaults to application config settings</param>
        /// <returns>A list of DeliveryData objects</returns>
        public static async Task<ListResult<DeliveryData>> GetDeliveryList(DeliveryListParameters deliveryListParameters, RequestSettings requestSettings = null)
        {
            var client = GetClient(requestSettings ?? DefaultRequestSettings);

            var response = await client.GetAsync($"{(requestSettings ?? DefaultRequestSettings).AdfastApiUrl}GetDeliveryList/?Page={deliveryListParameters.Page}&PageSize={deliveryListParameters.PageSize}&OrderBy={deliveryListParameters.OrderBy}&OrderDirection={deliveryListParameters.OrderDirection}");

            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsAsync<ListResult<DeliveryData>>();
            }

            throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        /// <summary>
        /// GetPublisherDeliveriesForDownload gets a list of delivery Id's for deliveries where files have not been downloaded.
        /// Access to this list is for Publisher API users only
        /// </summary>
        /// <param name="fileSourceTypes">File Source Types to return (as comma separated string), empty for all</param>
        /// <param name="requestSettings">Optional</param>
        /// <returns></returns>
        public static async Task<List<int>> GetPublisherDeliveriesForDownload(string fileSourceTypes = null, RequestSettings requestSettings = null)
        {
            var client = GetClient(requestSettings ?? DefaultRequestSettings);

            var response = await client.GetAsync($"{(requestSettings ?? DefaultRequestSettings).AdfastApiUrl}GetPublisherDeliveriesForDownload/?Value={fileSourceTypes ?? ""}");

            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsAsync<List<int>>();
            }

            throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        /// <summary>
        /// Initiates Delivery by Adfast Delivery Id (Tracking Number)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="authorizedBy"></param>
        /// <param name="requestSettings">settings for the api request</param>
        /// <returns></returns>
        public static async Task<bool> InitiateDelivery(int id, string authorizedBy, RequestSettings requestSettings)
        {
            var deliveryParameters = new DeliveryParameters
            {
                Id = id,
                AuthorizedBy = authorizedBy
            };

            return await InitiateDelivery(deliveryParameters, requestSettings);
        }

        /// <summary>
        /// Initiates Delivery by Adfast Delivery Id (Tracking Number)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="authorizedBy"></param>
        /// <returns></returns>
        public static async Task<bool> InitiateDelivery(int id, string authorizedBy)
        {
            return await InitiateDelivery(id, authorizedBy, DefaultRequestSettings);
        }

        /// <summary>
        /// Initiates Delivery using DeliveryParameters
        /// </summary>
        /// <param name="deliveryParameters"></param>
        /// <param name="requestSettings">settings for the api request</param>
        /// <returns></returns>
        public static async Task<bool> InitiateDelivery(DeliveryParameters deliveryParameters, RequestSettings requestSettings)
        {
            var client = GetClient(requestSettings);

            var response = await client.PostAsJsonAsync(requestSettings.AdfastApiUrl + "InitiateDelivery/", deliveryParameters);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        /// <summary>
        /// Initiates Delivery using DeliveryParameters
        /// </summary>
        /// <param name="deliveryParameters"></param>
        /// <returns></returns>
        public static async Task<bool> InitiateDelivery(DeliveryParameters deliveryParameters)
        {
            return await InitiateDelivery(deliveryParameters, DefaultRequestSettings);
        }

        public static async Task<bool> CreateDelivery(DeliveryCreateParameters deliveryCreateParameters, RequestSettings requestSettings)
        {
            var client = GetClient(requestSettings);

            var response = await client.PostAsJsonAsync(requestSettings.AdfastApiUrl + "CreateDelivery/", deliveryCreateParameters);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        public static async Task<bool> CreateDelivery(DeliveryCreateParameters deliveryCreateParameters)
        {
            return await CreateDelivery(deliveryCreateParameters, DefaultRequestSettings);
        }

        public static async Task<bool> UpdateDelivery(DeliveryUpdateParameters deliveryUpdateParameters, RequestSettings requestSettings)
        {
            var client = GetClient(requestSettings);

            var response = await client.PostAsJsonAsync(requestSettings.AdfastApiUrl + "UpdateDeliveryDetails/", deliveryUpdateParameters);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        public static async Task<bool> UpdateDelivery(DeliveryUpdateParameters deliveryUpdateParameters)
        {
            return await UpdateDelivery(deliveryUpdateParameters, DefaultRequestSettings);
        }

        /// <summary>
        /// Deletes the Delivery for the specified Adfast Delivery Id (Tracking Number)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="requestSettings">settings for the api request</param>
        /// <returns></returns>
        public static async Task<bool> DeleteDelivery(int id, RequestSettings requestSettings = null)
        {
            var client = GetClient(requestSettings ?? DefaultRequestSettings);

            var response = await client.DeleteAsync($"{(requestSettings ?? DefaultRequestSettings).AdfastApiUrl}DeleteDelivery/?Id={id}");

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        /// <summary>
        /// Deletes the Delivery for the specified Adfast Delivery Id (Tracking Number)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="requestSettings">settings for the api request</param>
        /// <returns></returns>
        public static async Task<bool> ArchiveDelivery(int id, RequestSettings requestSettings = null)
        {
            var client = GetClient(requestSettings ?? DefaultRequestSettings);

            var response = await client.PostAsJsonAsync($"{(requestSettings ?? DefaultRequestSettings).AdfastApiUrl}ArchiveDelivery/", new IdParameter {Id = id});

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        /// <summary>
        /// Deletes the Delivery for the specified Adfast Delivery Id (Tracking Number)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="requestSettings">settings for the api request</param>
        /// <returns></returns>
        public static async Task<bool> UnArchiveDelivery(int id, RequestSettings requestSettings =  null)
        {
            var client = GetClient(requestSettings ?? DefaultRequestSettings);

            var response = await client.PostAsJsonAsync($"{(requestSettings ?? DefaultRequestSettings).AdfastApiUrl}UnArchiveDelivery/", new IdParameter { Id = id });

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        public static async Task<DownloadResult> PublisherDeliveryDownload(int id, RequestSettings requestSettings = null)
        {
            var client = GetClient(requestSettings ?? DefaultRequestSettings);

            var response = await client.GetAsync($"{(requestSettings ?? DefaultRequestSettings).AdfastApiUrl}PublisherDeliveryDownload/?Id={id}");

            if (response.IsSuccessStatusCode)
            {
                var result = new DownloadResult
                {
                    FileContent = await response.Content.ReadAsByteArrayAsync(),
                    Filename = response.Content.Headers.ContentDisposition.FileName
                };
                return result;
            }

            throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        public static async Task<int> CreateMailBoxDelivery(MailBoxDeliveryCreateParameters mailBoxDeliveryCreateParameters, RequestSettings requestSettings)
        {
            var client = GetClient(requestSettings);

            var response = await client.PostAsJsonAsync($"{requestSettings.AdfastApiUrl}CreateMailBoxDelivery/", mailBoxDeliveryCreateParameters);

            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsAsync<IntResult>().Result.Value;
            }

            throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        public static async Task<int> CreateMailBoxDelivery(MailBoxDeliveryCreateParameters mailBoxDeliveryCreateParameters)
        {
            return await CreateMailBoxDelivery(mailBoxDeliveryCreateParameters, DefaultRequestSettings);
        }

        public static async Task<MailBoxDeliveryData> GetMailBoxDeliveryDetailsById(int id, RequestSettings requestSettings)
        {
            var client = GetClient(requestSettings);

            var response = await client.GetAsync($"{requestSettings.AdfastApiUrl}GetMailBoxDeliveryDetailsById/?Id={id}");

            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsAsync<MailBoxDeliveryData>();
            }

            throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        public static async Task<MailBoxDeliveryData> GetMailBoxDeliveryDetailsById(int id)
        {
            return await GetMailBoxDeliveryDetailsById(id, DefaultRequestSettings);
        }

        public static async Task<MailBoxDeliveryData> GetMailBoxDeliveryDetailsByUrn(string urn, RequestSettings requestSettings)
        {
            var client = GetClient(requestSettings);

            var response = await client.GetAsync($"{requestSettings.AdfastApiUrl}GetMailBoxDeliveryDetailsByUrn/?Value={urn}");

            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsAsync<MailBoxDeliveryData>();
            }

            throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        public static async Task<MailBoxDeliveryData> GetMailBoxDeliveryDetailsByUrn(string urn)
        {
            return await GetMailBoxDeliveryDetailsByUrn(urn, DefaultRequestSettings);
        }

        public static async Task<bool> UpdateMailBoxDelivery(MailBoxDeliveryUpdateParameters mailBoxDeliveryUpdateParameters, RequestSettings requestSettings)
        {
            var client = GetClient(requestSettings);

            var response = await client.PostAsJsonAsync(requestSettings.AdfastApiUrl + "UpdateMailBoxDelivery/", mailBoxDeliveryUpdateParameters);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        public static async Task<bool> UpdateMailBoxDelivery(MailBoxDeliveryUpdateParameters mailBoxDeliveryUpdateParameters)
        {
            return await UpdateMailBoxDelivery(mailBoxDeliveryUpdateParameters, DefaultRequestSettings);
        }

        /// <summary>
        /// Deletes the Delivery for the specified MailBox Delivery Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="requestSettings">settings for the api request</param>
        /// <returns></returns>
        public static async Task<bool> CancelMailBoxDeliveryById(int id, RequestSettings requestSettings)
        {
            var client = GetClient(requestSettings);

            var response = await client.PostAsJsonAsync($"{requestSettings.AdfastApiUrl}CancelMailBoxDeliveryById/", new IdParameter { Id = id });

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        /// <summary>
        /// Deletes the Delivery for the specified MailBox Delivery Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static async Task<bool> CancelMailBoxDeliveryById(int id)
        {
            return await CancelMailBoxDeliveryById(id, DefaultRequestSettings);
        }

        /// <summary>
        /// Deletes the Delivery for the specified MailBox Delivery Id
        /// </summary>
        /// <param name="urn"></param>
        /// <param name="requestSettings">settings for the api request</param>
        /// <returns></returns>
        public static async Task<bool> CancelMailBoxDeliveryByUrn(string urn, RequestSettings requestSettings)
        {
            var client = GetClient(requestSettings);

            var response = await client.PostAsJsonAsync($"{requestSettings.AdfastApiUrl}CancelMailBoxDeliveryByUrn/", new StringParameter { Value = urn });

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        /// <summary>
        /// Deletes the Delivery for the specified MailBox Delivery Id
        /// </summary>
        /// <param name="urn"></param>
        /// <returns></returns>
        public static async Task<bool> CancelMailBoxDeliveryByUrn(string urn)
        {
            return await CancelMailBoxDeliveryByUrn(urn, DefaultRequestSettings);
        }

        /// <summary>
        /// Creates/Imports multiple delivery records (without files) for later file upload.
        /// </summary>
        /// <param name="deliveryCreateParameters"></param>
        /// <param name="requestSettings"></param>
        /// <returns></returns>
        public static async Task<DeliveryImportResult> CreateDeliveries(List<DeliveryCreateParameters> deliveryCreateParameters, RequestSettings requestSettings = null)
        {
            var settings = requestSettings ?? DefaultRequestSettings;

            var client = GetClient(settings);

            var response = await client.PostAsJsonAsync(settings.AdfastApiUrl + "CreateDeliveries/", deliveryCreateParameters);

            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsAsync<DeliveryImportResult>();
            }

            throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        /// <summary>
        /// Gets the full api version number in format x.x.x
        /// </summary>
        /// <param name="requestSettings">settings for the api request</param>
        /// <returns></returns>
        public static async Task<string> GetApiVersion(RequestSettings requestSettings)
        {
            var client = GetClient(requestSettings);

            var response = await client.GetAsync(requestSettings.AdfastApiUrl + "GetApiVersion/");

            if (response.IsSuccessStatusCode)
            {
                return (await response.Content.ReadAsAsync<StringResult>()).Value;
            }

            throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        /// <summary>
        /// Sets the Publisher Received Delivery Status for the specified delivery id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="requestSettings">settings for the api request</param>
        /// <returns></returns>
        public static async Task<HttpStatusCode> SetPublisherFileReceivedStatus(int id, RequestSettings requestSettings = null)
        {
            var client = GetClient(requestSettings ?? DefaultRequestSettings);

            var response = await client.PostAsJsonAsync($"{(requestSettings ?? DefaultRequestSettings).AdfastApiUrl}SetPublisherFileReceivedStatus/", new IdParameter { Id = id });

            return response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.NotModified ? response.StatusCode : throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        ///  <summary>
        /// Gets the PADN Publisher List
        ///  </summary>
        ///  <param name="requestSettings">Optional Request setting, uses passed in settings or defaults to application config settings</param>
        /// <param name="includeMediaMule"></param>
        /// <returns>Returns a list containing PublisherCode and PublisherName used in other Padn related API calls</returns>
        public static async Task<ListResult<PadnPublisherData>> GetPadnPublisherList(bool includeMediaMule = false, RequestSettings requestSettings = null)
        {
            var client = GetClient(requestSettings ?? DefaultRequestSettings);

            var response = await client.GetAsync($"{(requestSettings ?? DefaultRequestSettings).AdfastApiUrl}GetPadnPublisherList/?includeMediaMule={includeMediaMule.ToString().ToLower()}");

            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsAsync<ListResult<PadnPublisherData>>();
            }

            throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        /// <summary>
        ///Gets the PADN Ad List for the specified paging/filter parameters
        /// </summary>
        /// <param name="padnAdListParameters"></param>
        /// <param name="requestSettings">Optional Request setting, uses passed in settings or defaults to application config settings</param>
        /// <returns>A ListResult containing the PADN Ads and paging info</returns>
        public static async Task<ListResult<PadnAdData>> GetPadnAdList(PadnAdListParameters padnAdListParameters, RequestSettings requestSettings = null)
        {
            var client = GetClient(requestSettings ?? DefaultRequestSettings);

            var response = await client.GetAsync($"{(requestSettings ?? DefaultRequestSettings).AdfastApiUrl}GetPadnAdList/?SenderEmailAddress={HttpUtility.UrlEncode(padnAdListParameters.SenderEmailAddress)}&PublisherFilter={HttpUtility.UrlEncode(padnAdListParameters.PublisherFilter)}&StatusFilter={HttpUtility.UrlEncode(padnAdListParameters.StatusFilter)}&PageNumber={padnAdListParameters.PageNumber}&PageSize={padnAdListParameters.PageSize}&SearchPhrase={HttpUtility.UrlEncode(padnAdListParameters.SearchPhrase)}&SortColumn={HttpUtility.UrlEncode(padnAdListParameters.SortColumn)}&SortDirection={HttpUtility.UrlEncode(padnAdListParameters.SortDirection)}&DaysFilter={padnAdListParameters.DaysFilter}");

            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsAsync<ListResult<PadnAdData>>();
            }

            throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        /// <summary>
        /// Confirm PADN Job by PADN Publisher Code and PADN Ad Id.
        /// API User account details will be used unless overridden.
        /// </summary>
        /// <param name="publisherCode"></param>
        /// <param name="adId"></param>
        /// <param name="senderName"></param>
        /// <param name="note"></param>
        /// <param name="senderPhone"></param>
        /// <param name="senderCompanyName"></param>
        /// <param name="senderEmailAddress"></param>
        /// <param name="requestSettings">Optional Request setting, uses passed in settings or defaults to application config settings</param>
        /// <returns></returns>
        public static async Task<HttpStatusCode> ConfirmPadnAd(string publisherCode, int adId, string senderName = null, string note = null, string senderPhone = null, string senderCompanyName = null, string senderEmailAddress = null, RequestSettings requestSettings = null)
        {
            var client = GetClient(requestSettings ?? DefaultRequestSettings);

            var parameters = new PadnAdConfirmationParameters
            {
                PublisherCode = publisherCode,
                AdId = adId,
                SenderEmailAddress = senderEmailAddress,
                SenderName =  senderName,
                SenderPhone = senderPhone,
                SenderCompanyName = senderCompanyName,
                Note = note
            };

            var response = await client.PostAsJsonAsync($"{(requestSettings ?? DefaultRequestSettings).AdfastApiUrl}ConfirmPadnAd/", parameters);

            return response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.NotModified ? response.StatusCode : throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        /// <summary>
        /// Reject PADN Job by PADN Publisher Code and PADN Ad Id.
        /// API User account details will be used unless overridden.
        /// </summary>
        /// <param name="publisherCode"></param>
        /// <param name="adId"></param>
        /// <param name="senderName"></param>
        /// <param name="note"></param>
        /// <param name="senderPhone"></param>
        /// <param name="senderCompanyName"></param>
        /// <param name="senderEmailAddress"></param>
        /// <param name="requestSettings">Optional Request setting, uses passed in settings or defaults to application config settings</param>
        /// <returns></returns>
        public static async Task<HttpStatusCode> RejectPadnAd(string publisherCode, int adId, string senderName = null, string note = null, string senderPhone = null, string senderCompanyName = null, string senderEmailAddress = null, RequestSettings requestSettings = null)
        {
            var client = GetClient(requestSettings ?? DefaultRequestSettings);

            var parameters = new PadnAdConfirmationParameters
            {
                PublisherCode = publisherCode,
                AdId = adId,
                SenderEmailAddress = senderEmailAddress,
                SenderName =  senderName,
                SenderPhone = senderPhone,
                SenderCompanyName = senderCompanyName,
                Note = note
            };

            var response = await client.PostAsJsonAsync($"{(requestSettings ?? DefaultRequestSettings).AdfastApiUrl}RejectPadnAd/", parameters);

            return response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.NotModified ? response.StatusCode : throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        /// <summary>
        /// GetPadnAdsByDetectionFilters gets the list of ads for the specified filters, used to detect padn ads on user selection
        /// </summary>
        /// <param name="padnAdDetectionParameters"></param>
        /// <param name="requestSettings">Optional Request setting, uses passed in settings or defaults to application config settings</param>
        /// <returns>A ListResult containing the PADN Ads</returns>
        public static async Task<ListResult<PadnAdData>> GetPadnAdsByDetectionFilters(PadnAdDetectionParameters padnAdDetectionParameters, RequestSettings requestSettings = null)
        {
            var client = GetClient(requestSettings ?? DefaultRequestSettings);

            var response = await client.GetAsync($"{(requestSettings ?? DefaultRequestSettings).AdfastApiUrl}GetPadnAdsByDetectionFilters?apiParameters.SenderEmailAddress={HttpUtility.UrlEncode(padnAdDetectionParameters.SenderEmailAddress)}&apiParameters.PublicationSectionId={(padnAdDetectionParameters.PublicationSectionId.HasValue && padnAdDetectionParameters.PublicationSectionId > 0 ? padnAdDetectionParameters.PublicationSectionId.ToString() : "")}&apiParameters.Urn={HttpUtility.UrlEncode(padnAdDetectionParameters.Urn)}&apiParameters.InsertionDate={(padnAdDetectionParameters.InsertionDate.HasValue ? HttpUtility.UrlEncode(padnAdDetectionParameters.InsertionDate.Value.ToString("dd/MM/yyyy")) : "")}&apiParameters.Width={HttpUtility.UrlEncode((padnAdDetectionParameters.Width.HasValue & padnAdDetectionParameters.Width > 0 ? padnAdDetectionParameters.Width.ToString() : "0").ToString())}&apiParameters.Height={HttpUtility.UrlEncode((padnAdDetectionParameters.Height.HasValue & padnAdDetectionParameters.Height > 0 ? padnAdDetectionParameters.Height.ToString() : "0").ToString())}");

            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsAsync<ListResult<PadnAdData>>();
            }

            throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        /// <summary>
        /// Sets an existing delivery as PADN (assumes that the relevant match has been confirmed via the call to ConfirmPadnAd as a match has been identified)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="padnId"></param>
        /// <param name="padnCreatedDate"></param>
        /// <param name="requestSettings"></param>
        /// <returns></returns>
        public static async Task<HttpStatusCode> SetDeliveryAsPadn(int id, string padnId, DateTime padnCreatedDate, RequestSettings requestSettings = null)
        {
            return await SetDeliveryAsPadn(id, requestSettings, padnId, padnCreatedDate);
        }

        /// <summary>
        /// Sets an existing delivery as PADN (assumes that the relevant match has been confirmed via the call to ConfirmPadnAd as a match has been identified)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="requestSettings"></param>
        /// <returns></returns>
        public static async Task<HttpStatusCode> SetDeliveryAsPadn(int id, RequestSettings requestSettings = null, string padnId = null, DateTime? padnCreatedDate = null)
        {
            var client = GetClient(requestSettings ?? DefaultRequestSettings);

            if (id <= 0)
            {
                throw new Exception($"SetDeliveryAsPadn: Id must be greater than zero");
            }

            var response = await client.PostAsync($"{(requestSettings ?? DefaultRequestSettings).AdfastApiUrl}SetDeliveryAsPadn?id={id}&padnId={HttpUtility.UrlEncode(padnId)}&padnCreatedDate={HttpUtility.UrlEncode(padnCreatedDate.HasValue ? padnCreatedDate.Value.ToString("dd/MM/yyyy HH:mm:ss") : "")}", null);

            return response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.NotModified ? response.StatusCode : throw new Exception($"{response.StatusCode},{response.ReasonPhrase}");
        }

        /// <summary>
        /// Gets the PADN Sender Ad Count for all publishers
        /// </summary>
        /// <param name="senderEmailAddress">Sender email override, uses API user email if not specified</param>
        /// <param name="includeMediaMule"></param>
        /// <param name="requestSettings"></param>
        /// <returns></returns>
        public static int GetPadnSenderAdCount(string senderEmailAddress, bool? includeMediaMule, RequestSettings requestSettings = null)
        {
            using (var client = GetClient(requestSettings ?? DefaultRequestSettings))
            {
                var response = client.GetAsync($"GetPadnSenderAdCount/?senderEmailAddress={senderEmailAddress ?? ""}&includeMediaMule={(includeMediaMule ?? false).ToString().ToLower()}").Result;

                var result = 0;

                if (response.IsSuccessStatusCode)
                {
                    if (!int.TryParse(response.Content.ReadAsStringAsync().Result ?? "0", out result))
                    {
                        result = 0;
                    }
                }

                return result;
            }
        }

        /// <summary>
        /// Gets the full api version number in format x.x.x
        /// </summary>
        /// <returns></returns>
        public static async Task<string> GetApiVersion()
        {
            return await GetApiVersion(DefaultRequestSettings);
        }

        private static RequestSettings DefaultRequestSettings
        {
            get
            {
                var result = new RequestSettings
                {
                    AdfastApiUrl = Settings.AdfastApiUrl,
                    AdfastApiUsername = Settings.AdfastApiUsername,
                    AdfastApiKey = Settings.AdfastApiKey,
                    Timeout = Settings.Timeout
                };

                return result;
            }
        }

        private static HttpClient GetClient(RequestSettings requestSettings)
        {
            var hmacHandler = new HmacHandler {RequestSettings = requestSettings};
            var client = HttpClientFactory.Create(hmacHandler);
            client.Timeout = new TimeSpan(0, 0, requestSettings.Timeout);
            return client;
        }
    }
}