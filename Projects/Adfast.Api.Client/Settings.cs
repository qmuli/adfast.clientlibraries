﻿using System;
using System.Configuration;

namespace Adfast.Api.Client
{
    public static class Settings
    {
        public static string AdfastApiUrl
        {
            get
            {
                var result = ConfigurationManager.AppSettings["Adfast.Api.Url"];
                return String.IsNullOrWhiteSpace(result) ? String.Empty : result;
            }
        }

        public static string AdfastApiUsername
        {
            get
            {
                var result = ConfigurationManager.AppSettings["Adfast.Api.Username"];
                return String.IsNullOrWhiteSpace(result) ? String.Empty : result;
            }
        }

        public static string AdfastApiKey
        {
            get
            {
                var result = ConfigurationManager.AppSettings["Adfast.Api.Key"];
                return String.IsNullOrWhiteSpace(result) ? String.Empty : result;
            }
        }

        public static int Timeout
        {
            get
            {
                var result = ConfigurationManager.AppSettings["Adfast.Api.Timeout"];

                if (int.TryParse(result, out int timeoutInSeconds))
                {
                    return timeoutInSeconds;
                }

                return 100;
            }
        }
    }
}
