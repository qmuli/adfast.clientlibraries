﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Adfast.Api.Client")]
[assembly: AssemblyDescription("Adfast API Client for Adfast API")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Qmuli Limited")]
[assembly: AssemblyProduct("Adfast.Api.Client")]
[assembly: AssemblyCopyright("Copyright © 2025")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("675ab281-5016-4812-a84c-f10d8bd1cd53")]
[assembly: AssemblyVersion("3.2.0")]
[assembly: AssemblyFileVersion("3.2.0")]
[assembly: AssemblyInformationalVersion("3.2.0")]
