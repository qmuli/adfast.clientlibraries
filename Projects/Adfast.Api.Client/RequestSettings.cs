﻿namespace Adfast.Api.Client
{
    public class RequestSettings
    {
        public string AdfastApiUrl { get; set; }

        public string AdfastApiUsername { get; set; }

        public string AdfastApiKey { get; set; }

        public int Timeout { get; set; } = 100;
    }
}
