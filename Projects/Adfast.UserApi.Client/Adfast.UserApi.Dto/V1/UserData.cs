﻿using System;
using System.Runtime.Serialization;

namespace Adfast.UserApi.Dto.V1
{
    [Serializable, DataContract]
    public class UserData
    {
        public UserData()
        {
            IsAdministrator = false;
            IsPublisherAdmin = false;
            IsPublisherUser = false;
        }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public decimal Credits { get; set; }

        [DataMember]
        public bool IsAdministrator { get; set; }

        [DataMember]
        public bool IsPublisherAdmin { get; set; }

        [DataMember]
        public bool IsPublisherUser { get; set; }

        [DataMember]
        public int? PublisherId { get; set; }

        [DataMember]
        public string CompanyName { get; set; }

        [DataMember]
        public string Phone { get; set; }
    }
}
