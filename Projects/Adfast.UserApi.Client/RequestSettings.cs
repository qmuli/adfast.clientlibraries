﻿namespace Adfast.UserApi.Client
{
    public class RequestSettings
    {
        public string ApiUrl { get; set; }

        public string ApiUsername { get; set; }

        public string SessionKey { get; set; }

        public string ApiKey { get; set; }
    }
}
