﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Http;
using System.Net.Http.Headers;
using Adfast.UserApi.Dto.V1;

namespace Adfast.UserApi.Client
{
    /// <summary>
    /// Adfast User Api Client (For Version 1)
    /// </summary>
    public static class UserApiV1
    {
        /// <summary>
        /// Gets the session Key for the specified credentials
        /// </summary>
        /// <param name="password"></param>
        /// <param name="requestSettings"></param>
        /// <returns></returns>
        public static string GetSessionKey(string password, RequestSettings requestSettings = null)
        {
            using (var client = GetHttpClient(requestSettings))
            {
                var content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("ApiKey", Settings.QmuliAdfastUserApiKey),
                    new KeyValuePair<string, string>("Username", Settings.QmuliAdfastUserApiUsername),
                    new KeyValuePair<string, string>("Password", password)
                });

                var response = client.PostAsync("GetSessionKey/", content).Result;

                var result = String.Empty;

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<string>().Result ?? String.Empty;
                }

                return result;
            }
        }

        /// <summary>
        /// Checks is the supplied headers are valid
        /// </summary>
        /// <param name="requestSettings"></param>
        /// <returns></returns>
        public static bool IsSessionKeyValid(RequestSettings requestSettings = null)
        {
            using (var client = GetHttpClient(requestSettings))
            {
                var content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("", "")
                });

                var response = client.PostAsync("IsSessionKeyValid/", content).Result;

                var result = false;

                if (response.IsSuccessStatusCode)
                {
                    result = (response.Content.ReadAsAsync<bool>().Result.ToString().ToLower() == "true");
                }

                return result;
            }
        }

        /// <summary>
        /// Validates the Adfast username and password
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="requestSettings"></param>
        /// <returns></returns>
        public static UserData ValidateUser(string username, string password, RequestSettings requestSettings = null)
        {
            using (var client = GetHttpClient(requestSettings))
            {
                var content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("Username", username),
                    new KeyValuePair<string, string>("Password", password),
                });

                var response = client.PostAsync("ValidateUser/", content).Result;

                var result = new UserData();

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<UserData>().Result;
                }

                return result;
            }
        }

        /// <summary>
        /// GetAdfastApiKey gets the Adfast Api key for the specified username
        /// </summary>
        /// <param name="username"></param>
        /// <param name="requestSettings"></param>
        /// <returns></returns>
        public static Guid GetAdfastApiKey(string username, RequestSettings requestSettings = null)
        {
            using (var client = GetHttpClient(requestSettings))
            {
                var response = client.GetAsync($"GetAdfastApiKey/?Username={username}").Result;

                var result = String.Empty;

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsStringAsync().Result ?? String.Empty;
                }

                return Guid.TryParse(result, out Guid guid) ? guid : Guid.Empty;
            }
        }

        /// <summary>
        /// Gets the User data for the specified username
        /// </summary>
        /// <param name="username"></param>
        /// <param name="requestSettings"></param>
        /// <returns></returns>
        public static UserData GetUserByUsername(string username, RequestSettings requestSettings = null)
        {
            using (var client = GetHttpClient(requestSettings))
            {
                var content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("Username", username)
                });

                var response = client.PostAsync("GetUserByUsername/", content).Result;

                var result = new UserData();

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<UserData>().Result;
                }

                return result;
            }
        }

        /// <summary>
        /// Gets the User credits for the specified user id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="requestSettings"></param>
        /// <returns></returns>
        public static decimal GetUserCredits(int id, RequestSettings requestSettings = null)
        {
            using (var client = GetHttpClient(requestSettings))
            {
                var content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("Id", id.ToString()),
                });

                var response = client.PostAsync("GetUserCredits/", content).Result;

                decimal result = 0;

                if (response.IsSuccessStatusCode)
                {
                    if (!decimal.TryParse(response.Content.ReadAsAsync<bool>().Result.ToString(), out result))
                    {
                        result = 0;
                    }
                }

                return result;
            }
        }

        /// <summary>
        /// Deduct the specified number of credits from the specified user account
        /// </summary>
        /// <param name="id"></param>
        /// <param name="credits"></param>
        /// <param name="requestSettings"></param>
        /// <returns></returns>
        public static decimal DeductUserCredits(int id, decimal credits, RequestSettings requestSettings = null)
        {
            using (var client = GetHttpClient(requestSettings))
            {
                var content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("Id", id.ToString()),
                    new KeyValuePair<string, string>("Credits", credits.ToString(CultureInfo.InvariantCulture))
                });

                var response = client.PostAsync("DeductUserCredits/", content).Result;

                decimal result = 0;

                if (response.IsSuccessStatusCode)
                {
                    if (!decimal.TryParse(response.Content.ReadAsAsync<bool>().Result.ToString(), out result))
                    {
                        result = 0;
                    }
                }

                return result;
            }
        }

        /// <summary>
        /// Gets the User details by Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="requestSettings"></param>
        /// <returns></returns>
        public static UserData GetUser(int id, RequestSettings requestSettings = null)
        {
            using (var client = GetHttpClient(requestSettings))
            {
                var content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("Id", id.ToString(CultureInfo.InvariantCulture))
                });

                var response = client.PostAsync("GetUser/", content).Result;

                var result = new UserData();

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<UserData>().Result;
                }

                return result;
            }
        }

        /// <summary>
        /// Gets the full api version number in format x.x.x
        /// </summary>
        /// <param name="requestSettings"></param>
        /// <returns></returns>
        public static string GetApiVersion(RequestSettings requestSettings = null)
        {
            using (var client = GetHttpClient(requestSettings))
            {   
                var response = client.GetAsync("GetApiVersion/").Result;

                var result = String.Empty;

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsStringAsync().Result ?? String.Empty;
                }

                return result;
            }
        }

        /// <summary>
        /// Gets the HttpClient and sets the required headers, with config values or passed in settings
        /// </summary>
        /// <param name="requestSettings"></param>
        /// <returns></returns>
        private static HttpClient GetHttpClient(RequestSettings requestSettings = null)
        {
            if (requestSettings == null)
            {
                requestSettings = new RequestSettings
                {
                    ApiKey = Settings.QmuliAdfastUserApiKey,
                    SessionKey = Settings.QmuliAdfastUserApiSessionKey,
                    ApiUsername = Settings.QmuliAdfastUserApiUsername,
                    ApiUrl = Settings.QmuliAdfastUserApiUrl
                };
            }

            var client = new HttpClient {BaseAddress = new Uri(requestSettings.ApiUrl) };
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("SessionKey", requestSettings.SessionKey);
            client.DefaultRequestHeaders.Add("ApiKey", requestSettings.ApiKey);
            client.DefaultRequestHeaders.Add("Username", requestSettings.ApiUsername);
            return client;
        }
    }
}
