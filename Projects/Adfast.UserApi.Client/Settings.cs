﻿using System;
using System.Configuration;

namespace Adfast.UserApi.Client
{
    public static class Settings
    {
        public static string QmuliAdfastUserApiUrl
        {
            get
            {
                var result = ConfigurationManager.AppSettings["Qmuli.Adfast.UserApiUrl"];
                return String.IsNullOrWhiteSpace(result) ? String.Empty : result;
            }
        }

        public static string QmuliAdfastUserApiKey
        {
            get
            {
                var result = ConfigurationManager.AppSettings["Qmuli.Adfast.UserApiKey"];
                return String.IsNullOrWhiteSpace(result) ? String.Empty : result;
            }
        }

        public static string QmuliAdfastUserApiUsername
        {
            get
            {
                var result = ConfigurationManager.AppSettings["Qmuli.Adfast.UserApiUsername"];
                return String.IsNullOrWhiteSpace(result) ? String.Empty : result;
            }
        }

        public static string QmuliAdfastUserApiSessionKey
        {
            get
            {
                var result = ConfigurationManager.AppSettings["Qmuli.Adfast.UserApiSessionKey"];
                return String.IsNullOrWhiteSpace(result) ? String.Empty : result;
            }
        }
    }
}
